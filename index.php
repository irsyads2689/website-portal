<?php

date_default_timezone_set('Asia/Jakarta');

 /*
 * -------------------------------------------------------------------
 *  PATH DEFINITIONS
 * -------------------------------------------------------------------
 */
$framework_path = 'protected/system';
$base_folder = 'protected/content/base';
$view_folder = 'themes';

/*
 * -------------------------------------------------------------------
 *  Bootstrap
 * -------------------------------------------------------------------
 */
require_once 'protected/framework.php';
