-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 05 Jul 2018 pada 03.58
-- Versi Server: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `portal_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `advocates`
--

CREATE TABLE IF NOT EXISTS `advocates` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(100) NOT NULL,
  `meta_title` varchar(100) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `advocates`
--

INSERT INTO `advocates` (`id`, `name`, `slug`, `content`, `image`, `meta_title`, `meta_description`, `meta_keywords`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'tes', 'tes', '<p>tes</p>', 'IMG_1530723375.jpg', '', '', '', '1', '2018-07-04 23:56:15', 0, '2018-07-04 16:56:15', 0),
(2, 'sdfsdf', 'sdfsdf', '', 'IMG_1530724222.jpg', '', '', '', '1', '2018-07-05 00:07:48', 0, '2018-07-04 17:10:22', 0),
(3, 'test', 'test', '', '', '', '', '', '1', '2018-07-05 00:12:16', 2025, '2018-07-04 17:13:55', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `answers`
--

CREATE TABLE IF NOT EXISTS `answers` (
`id` int(11) NOT NULL,
  `discuss_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `answer` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_id` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
`id` bigint(20) unsigned NOT NULL,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `author` tinytext NOT NULL,
  `author_email` varchar(100) NOT NULL DEFAULT '',
  `author_url` varchar(200) NOT NULL DEFAULT '',
  `author_ip` varchar(100) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `approved` varchar(20) NOT NULL DEFAULT '1',
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Struktur dari tabel `discussions`
--

CREATE TABLE IF NOT EXISTS `discussions` (
`id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `question` text NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'publish',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `view_count` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `discussions`
--

INSERT INTO `discussions` (`id`, `member_id`, `title`, `slug`, `question`, `status`, `created_at`, `updated_at`, `view_count`) VALUES
(1, 7, 'tes', 'tes', '<p>tes</p>', 'publish', '2018-04-25 14:25:36', '2018-04-25 07:25:36', 0),
(2, 7, 'bintik putih seperti komedo di leher penis', 'bintik-putih-seperti-komedo-di-leher-penis', '<p><span>bintik putih seperti komedo di leher penis. itu apa yah dok ? apakah bisa hilang ?</span></p>', 'publish', '2018-04-25 14:27:08', '2018-04-25 07:27:08', 0),
(3, 7, 'kembali ke komunitas  balas  buat pertanyaan bagaimana cara mengatasi keputihan yang berlebihan???', 'kembali-ke-komunitas-balas-buat-pertanyaan-bagaimana-cara-mengatasi-keputihan-yang-berlebihan', '<p><span>Dok Bagaimana sih cara mengatasi kputihan yg berlebihan tapi saya belum mlakukan papsmir</span></p>', 'publish', '2018-04-25 14:36:42', '2018-04-25 07:36:42', 0),
(4, 7, 'pap smear utk hub sexual pertama kali', 'pap-smear-utk-hub-sexual-pertama-kali', '<p><span>Dok, saya sudah melakukan hub intim (baru 1x) tetapi belum vaksin hpv. Kalau saya ingin vaksin hpv apa perlu pap smear terlebih dulu walau baru 1x hub intim? Terima kasih.</span></p>', 'publish', '2018-04-25 14:37:36', '2018-04-25 07:37:36', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `doctors`
--

CREATE TABLE IF NOT EXISTS `doctors` (
`id` bigint(20) unsigned NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `image` varchar(100) NOT NULL,
  `last_login` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Struktur dari tabel `members`
--

CREATE TABLE IF NOT EXISTS `members` (
`id` bigint(20) unsigned NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `image` varchar(100) NOT NULL,
  `last_login` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `members`
--

INSERT INTO `members` (`id`, `username`, `password`, `email`, `name`, `phone`, `gender`, `image`, `last_login`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(7, '', '$P$BIIOLBLI5Z5tLnq09Vdaai4DGDh2RR1', 'irsyads2689@gmail.com', 'irsyad', '', '', '', '2018-04-25 11:45:38', 0, '2018-04-25 10:42:41', '2018-04-25 04:45:38', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `options`
--

CREATE TABLE IF NOT EXISTS `options` (
`id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` longtext NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `options`
--

INSERT INTO `options` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'site_name', 'jDokter', '2017-04-27 07:35:10', '2018-05-07 13:59:12'),
(2, 'site_description', '', '2017-04-27 07:36:50', '2017-05-11 05:16:14'),
(3, 'meta_title', 'jDokter', '0000-00-00 00:00:00', '2018-05-07 13:59:20'),
(4, 'meta_description', '', '2017-04-27 07:37:46', '2017-04-27 00:37:46'),
(5, 'mwta_keywords', '', '2017-04-27 07:37:46', '2017-04-27 00:37:46'),
(6, 'theme', 'default', '2017-04-27 07:38:00', '2018-03-26 04:05:22'),
(7, 'admin_email', '', '2017-04-27 08:14:08', '2017-04-27 01:14:08'),
(8, 'google_analytics', '', '2017-04-27 08:14:58', '2017-05-11 05:16:35'),
(9, 'social_facebook', 'https://www.facebook.com/MyMilk.id/', '0000-00-00 00:00:00', '2017-05-11 05:17:19'),
(10, 'social_twitter', 'https://twitter.com/mymilk_id', '0000-00-00 00:00:00', '2017-05-11 05:17:02'),
(11, 'social_youtube', 'https://www.youtube.com/channel/UCkv284_ECaipQCSuCYC7CTA?feature=watch', '0000-00-00 00:00:00', '2017-05-11 05:18:03'),
(12, 'social_instagram', 'https://www.instagram.com/mymilk_id/', '0000-00-00 00:00:00', '2017-05-11 05:17:43'),
(13, 'site_description', 'zayana hijab                                                                                                    ', '0000-00-00 00:00:00', '2018-03-23 02:35:01'),
(14, 'google_analytics', '                                                                                                    ', '0000-00-00 00:00:00', '2018-03-23 02:35:01'),
(15, 'whatsapp', '', '0000-00-00 00:00:00', '2018-03-23 02:35:01'),
(16, 'line', '', '0000-00-00 00:00:00', '2018-03-23 02:35:02'),
(17, 'youtube', '', '0000-00-00 00:00:00', '2018-03-23 02:35:02'),
(18, 'twitter', '', '0000-00-00 00:00:00', '2018-03-23 02:35:02'),
(19, 'facebook', '', '0000-00-00 00:00:00', '2018-03-23 02:35:02'),
(20, 'site_description', 'zayana hijab', '0000-00-00 00:00:00', '2018-03-23 02:38:28'),
(21, 'google_analytics', '                                                                                                    ', '0000-00-00 00:00:00', '2018-03-23 02:38:28'),
(22, 'whatsapp', '', '0000-00-00 00:00:00', '2018-03-23 02:38:28'),
(23, 'line', '', '0000-00-00 00:00:00', '2018-03-23 02:38:28'),
(24, 'youtube', '', '0000-00-00 00:00:00', '2018-03-23 02:38:29'),
(25, 'twitter', '', '0000-00-00 00:00:00', '2018-03-23 02:38:29'),
(26, 'facebook', 'https://facebook.com/irsyads89', '0000-00-00 00:00:00', '2018-03-23 02:38:29'),
(27, 'site_description', 'zayana hijab', '0000-00-00 00:00:00', '2018-03-23 02:39:07'),
(28, 'google_analytics', '', '0000-00-00 00:00:00', '2018-03-23 02:39:07'),
(29, 'whatsapp', '', '0000-00-00 00:00:00', '2018-03-23 02:39:08'),
(30, 'line', '', '0000-00-00 00:00:00', '2018-03-23 02:39:08'),
(31, 'youtube', '', '0000-00-00 00:00:00', '2018-03-23 02:39:08'),
(32, 'twitter', '', '0000-00-00 00:00:00', '2018-03-23 02:39:08'),
(33, 'facebook', 'https://facebook.com/irsyads89', '0000-00-00 00:00:00', '2018-03-23 02:39:08'),
(34, 'meta_description', 'zayana hijab', '0000-00-00 00:00:00', '2018-03-23 02:47:02'),
(35, 'meta_keywords', 'jDokter', '0000-00-00 00:00:00', '2018-05-07 13:59:20'),
(36, 'meta_description', 'zayana hijab', '0000-00-00 00:00:00', '2018-03-23 02:47:06'),
(37, 'site_description', 'zayana hijab', '0000-00-00 00:00:00', '2018-03-23 02:48:08'),
(38, 'google_analytics', 'zayana hijab', '0000-00-00 00:00:00', '2018-03-23 02:48:09'),
(39, 'whatsapp', '', '0000-00-00 00:00:00', '2018-03-23 02:48:09'),
(40, 'line', '', '0000-00-00 00:00:00', '2018-03-23 02:48:09'),
(41, 'youtube', '', '0000-00-00 00:00:00', '2018-03-23 02:48:09'),
(42, 'instagram', '', '0000-00-00 00:00:00', '2018-03-23 02:48:09'),
(43, 'twitter', '', '0000-00-00 00:00:00', '2018-03-23 02:48:09'),
(44, 'facebook', 'https://facebook.com/irsyads89', '0000-00-00 00:00:00', '2018-03-23 02:48:09'),
(45, 'site_description', 'zayana hijab', '0000-00-00 00:00:00', '2018-03-23 02:48:22'),
(46, 'google_analytics', 'zayana hijab', '0000-00-00 00:00:00', '2018-03-23 02:48:22'),
(47, 'whatsapp', '083807230141', '0000-00-00 00:00:00', '2018-03-23 02:48:22'),
(48, 'line', '083807230141', '0000-00-00 00:00:00', '2018-03-23 02:48:22'),
(49, 'youtube', 'https://facebook.com/irsyads89', '0000-00-00 00:00:00', '2018-03-23 02:48:22'),
(50, 'instagram', 'https://facebook.com/irsyads89', '0000-00-00 00:00:00', '2018-03-23 02:48:22'),
(51, 'twitter', 'https://facebook.com/irsyads89', '0000-00-00 00:00:00', '2018-03-23 02:48:22'),
(52, 'facebook', 'https://facebook.com/irsyads89', '0000-00-00 00:00:00', '2018-03-23 02:48:22'),
(53, 'site_description', 'alodok', '0000-00-00 00:00:00', '2018-04-24 03:10:36'),
(54, 'google_analytics', 'alodok', '0000-00-00 00:00:00', '2018-04-24 03:10:37'),
(55, 'whatsapp', '083807230141', '0000-00-00 00:00:00', '2018-04-24 03:10:37'),
(56, 'line', '083807230141', '0000-00-00 00:00:00', '2018-04-24 03:10:37'),
(57, 'youtube', 'https://facebook.com/irsyads89', '0000-00-00 00:00:00', '2018-04-24 03:10:37'),
(58, 'instagram', 'https://facebook.com/irsyads89', '0000-00-00 00:00:00', '2018-04-24 03:10:37'),
(59, 'twitter', 'https://facebook.com/irsyads89', '0000-00-00 00:00:00', '2018-04-24 03:10:37'),
(60, 'facebook', 'https://facebook.com/irsyads89', '0000-00-00 00:00:00', '2018-04-24 03:10:37'),
(61, 'site_description', 'jdokter adalah ...', '0000-00-00 00:00:00', '2018-05-07 13:59:12'),
(62, 'google_analytics', 'jDokter', '0000-00-00 00:00:00', '2018-05-07 13:59:12'),
(63, 'whatsapp', '083807230141', '0000-00-00 00:00:00', '2018-05-07 13:59:12'),
(64, 'line', '083807230141', '0000-00-00 00:00:00', '2018-05-07 13:59:12'),
(65, 'youtube', 'https://facebook.com/irsyads89', '0000-00-00 00:00:00', '2018-05-07 13:59:12'),
(66, 'instagram', 'https://facebook.com/irsyads89', '0000-00-00 00:00:00', '2018-05-07 13:59:12'),
(67, 'twitter', 'https://facebook.com/irsyads89', '0000-00-00 00:00:00', '2018-05-07 13:59:12'),
(68, 'facebook', 'https://facebook.com/irsyads89', '0000-00-00 00:00:00', '2018-05-07 13:59:12'),
(69, 'meta_description', 'jDokter', '0000-00-00 00:00:00', '2018-05-07 13:59:20'),
(70, 'site_description', 'jdokter adalah ...', '0000-00-00 00:00:00', '2018-07-04 16:19:42'),
(71, 'google_analytics', 'jDokter', '0000-00-00 00:00:00', '2018-07-04 16:19:42'),
(72, 'whatsapp', '083807230141', '0000-00-00 00:00:00', '2018-07-04 16:19:42'),
(73, 'line', '083807230141', '0000-00-00 00:00:00', '2018-07-04 16:19:42'),
(74, 'youtube', 'https://facebook.com/irsyads89', '0000-00-00 00:00:00', '2018-07-04 16:19:42'),
(75, 'instagram', 'https://facebook.com/irsyads89', '0000-00-00 00:00:00', '2018-07-04 16:19:42'),
(76, 'twitter', 'https://facebook.com/irsyads89', '0000-00-00 00:00:00', '2018-07-04 16:19:42'),
(77, 'facebook', 'https://facebook.com/irsyads89', '0000-00-00 00:00:00', '2018-07-04 16:19:43'),
(78, 'site_description', 'jdokter adalah ...', '0000-00-00 00:00:00', '2018-07-04 16:20:13'),
(79, 'google_analytics', 'jDokter', '0000-00-00 00:00:00', '2018-07-04 16:20:13'),
(80, 'whatsapp', '083807230141', '0000-00-00 00:00:00', '2018-07-04 16:20:13'),
(81, 'line', '083807230141', '0000-00-00 00:00:00', '2018-07-04 16:20:13'),
(82, 'youtube', 'https://facebook.com/irsyads89', '0000-00-00 00:00:00', '2018-07-04 16:20:13'),
(83, 'instagram', 'https://facebook.com/irsyads89', '0000-00-00 00:00:00', '2018-07-04 16:20:13'),
(84, 'twitter', 'https://facebook.com/irsyads89', '0000-00-00 00:00:00', '2018-07-04 16:20:13'),
(85, 'facebook', 'https://facebook.com/irsyads89', '0000-00-00 00:00:00', '2018-07-04 16:20:13');

-- --------------------------------------------------------

--
-- Struktur dari tabel `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
`id` bigint(20) unsigned NOT NULL,
  `title` text NOT NULL,
  `content` longtext NOT NULL,
  `excerpt` text NOT NULL,
  `slug` varchar(200) NOT NULL DEFAULT '',
  `image` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `type` varchar(20) NOT NULL DEFAULT 'post',
  `status` varchar(20) NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) NOT NULL DEFAULT 'close',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  `view_count` bigint(20) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` bigint(20) unsigned NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` bigint(20) unsigned NOT NULL DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` bigint(20) NOT NULL DEFAULT '0',
  `back_to_top` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=21619 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `posts`
--

INSERT INTO `posts` (`id`, `title`, `content`, `excerpt`, `slug`, `image`, `meta_title`, `meta_description`, `meta_keywords`, `parent`, `type`, `status`, `comment_status`, `comment_count`, `view_count`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`, `back_to_top`) VALUES
(21617, 'profil', '<p>title</p>', '', 'profil', '', '', '', '', 0, 'page', '1', 'close', 0, 0, '2018-07-05 00:53:03', 2025, '2018-07-04 17:53:03', 0, NULL, 0, '2018-07-05 00:53:03'),
(21618, 'test', '<p>test</p>', '', 'test', 'IMG_1530729669.png', 'test', '', '', 0, 'news', '1', 'close', 0, 0, '2018-07-05 01:41:09', 2025, '2018-07-04 18:41:09', 0, NULL, 0, '2018-07-05 01:41:09');

-- --------------------------------------------------------

--
-- Struktur dari tabel `service_legals`
--

CREATE TABLE IF NOT EXISTS `service_legals` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(100) NOT NULL,
  `meta_title` varchar(100) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `status` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `service_legals`
--

INSERT INTO `service_legals` (`id`, `name`, `slug`, `content`, `image`, `meta_title`, `meta_description`, `meta_keywords`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'tes', 'tes', '<p>tes</p>', '', '', '', '', '1', '2018-07-05 00:19:26', 2025, '2018-07-04 17:19:26', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `data` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `sessions`
--

INSERT INTO `sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('25rhjqq92ft3c6nm9tbp81pr1vffe5ur', '::1', 1530729634, '__ci_last_regenerate|i:1530729629;logged_in|b:1;login_username|s:5:"admin";login_name|s:13:"Administrator";login_email|s:0:"";login_id|s:4:"2025";login_group|s:1:"0";old_last_login|s:19:"0000-00-00 00:00:00";language|s:10:"indonesian";'),
('2sfqrul821rvdqmg0sn9pqj66r132316', '::1', 1530737064, '__ci_last_regenerate|i:1530737059;logged_in|b:1;login_username|s:5:"admin";login_name|s:13:"Administrator";login_email|s:0:"";login_id|s:4:"2025";login_group|s:1:"0";old_last_login|s:19:"0000-00-00 00:00:00";language|s:10:"indonesian";'),
('3fi54lqtn6aomd9a4jbqserm33kuoun9', '::1', 1530733105, '__ci_last_regenerate|i:1530733105;'),
('418s6mc7l05f2i7g95ngdnqh0sc2topd', '::1', 1530728920, '__ci_last_regenerate|i:1530728920;logged_in|b:1;login_username|s:5:"admin";login_name|s:13:"Administrator";login_email|s:0:"";login_id|s:4:"2025";login_group|s:1:"0";old_last_login|s:19:"0000-00-00 00:00:00";language|s:10:"indonesian";'),
('626sinci2vinipac25gp5mufep9rg4c4', '::1', 1530736857, '__ci_last_regenerate|i:1530736857;'),
('9qj63kthkvn84fhpiflcbm20t5okfc4q', '::1', 1530731311, '__ci_last_regenerate|i:1530731311;logged_in|b:1;login_username|s:5:"admin";login_name|s:13:"Administrator";login_email|s:0:"";login_id|s:4:"2025";login_group|s:1:"0";old_last_login|s:19:"0000-00-00 00:00:00";language|s:10:"indonesian";'),
('9qv57c3ldtmnjunkm31tgmihjtn92e39', '::1', 1530732453, '__ci_last_regenerate|i:1530732453;'),
('bvriaalatvvf5lvnohvercmqp40apq52', '::1', 1530733883, '__ci_last_regenerate|i:1530733883;'),
('caluq13394ngp7psk9sdh2i839hedu56', '::1', 1530733472, '__ci_last_regenerate|i:1530733472;'),
('elfkvu3ufbr12tkmf1epo528q04rcivq', '::1', 1530735478, '__ci_last_regenerate|i:1530735478;'),
('fg0n4d8jb8cavju4gtogjps09nqt3udv', '::1', 1530737325, '__ci_last_regenerate|i:1530737325;'),
('gdof2um5t8njhfg69gusmq4o9o2ck70c', '::1', 1530734870, '__ci_last_regenerate|i:1530734870;'),
('m80p1i7g0pv4hrj7e7h4rqpogqqj5k2n', '::1', 1530734194, '__ci_last_regenerate|i:1530734194;'),
('mr3i0est8f5no1u0epc5d2bmieq1ec8e', '::1', 1530737648, '__ci_last_regenerate|i:1530737648;'),
('o4rpdsoat4d090mcdbra7pa3tqr0gbav', '::1', 1530734499, '__ci_last_regenerate|i:1530734499;'),
('p1gj8rhkunoguma9f7363f12ntnh6up5', '::1', 1530737020, '__ci_last_regenerate|i:1530737020;'),
('p9g5l39486q4117ccrefkqu5hb43qb0n', '::1', 1530735829, '__ci_last_regenerate|i:1530735829;'),
('pj94li809pkiiuabl9sqsv5sbkmsei25', '::1', 1530732759, '__ci_last_regenerate|i:1530732759;'),
('pmgrttbnv7kbg5l442pv280s3gn7jajc', '::1', 1530735173, '__ci_last_regenerate|i:1530735173;'),
('tr7c0em7bgmhluueajf1tpo60hp3p1kf', '::1', 1530732670, '__ci_last_regenerate|i:1530732670;logged_in|b:1;login_username|s:5:"admin";login_name|s:13:"Administrator";login_email|s:0:"";login_id|s:4:"2025";login_group|s:1:"0";old_last_login|s:19:"0000-00-00 00:00:00";language|s:10:"indonesian";'),
('ud2fppl8fkf8vgnspshmp646pc808bag', '::1', 1530737841, '__ci_last_regenerate|i:1530737835;logged_in|b:1;login_username|s:5:"admin";login_name|s:13:"Administrator";login_email|s:0:"";login_id|s:4:"2025";login_group|s:1:"0";old_last_login|s:19:"0000-00-00 00:00:00";language|s:10:"indonesian";');

-- --------------------------------------------------------

--
-- Struktur dari tabel `terms`
--

CREATE TABLE IF NOT EXISTS `terms` (
`id` bigint(20) unsigned NOT NULL,
  `type` varchar(32) NOT NULL DEFAULT '',
  `name` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `description` longtext NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  `status` varchar(10) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `terms`
--

INSERT INTO `terms` (`id`, `type`, `name`, `slug`, `description`, `parent`, `count`, `status`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted_at`) VALUES
(33, '', 'test', 'test', '', 0, 0, '1', '2018-07-05 01:40:55', '2018-07-04 18:40:55', 2025, 0, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `term_relations`
--

CREATE TABLE IF NOT EXISTS `term_relations` (
`id` bigint(20) NOT NULL,
  `post_id` bigint(20) NOT NULL,
  `term_id` bigint(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `term_relations`
--

INSERT INTO `term_relations` (`id`, `post_id`, `term_id`) VALUES
(10, 21618, 33);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` bigint(20) unsigned NOT NULL,
  `group_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `image` varchar(100) NOT NULL,
  `last_login` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2026 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `group_id`, `username`, `password`, `email`, `name`, `phone`, `gender`, `image`, `last_login`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2025, 0, 'admin', '$P$BtECRw/blO7J1GrFCX5PM8DWtOq4TX0', '', 'Administrator', '', '', '', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', '2018-05-25 07:48:54', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_groups`
--

CREATE TABLE IF NOT EXISTS `user_groups` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advocates`
--
ALTER TABLE `advocates`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
 ADD PRIMARY KEY (`id`), ADD KEY `comment_post_ID` (`post_id`), ADD KEY `comment_approved_date_gmt` (`approved`), ADD KEY `comment_parent` (`parent`), ADD KEY `comment_author_email` (`author_email`(10));

--
-- Indexes for table `discussions`
--
ALTER TABLE `discussions`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
 ADD PRIMARY KEY (`id`), ADD KEY `username` (`username`), ADD KEY `email` (`email`), ADD KEY `deleted_at` (`deleted_at`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
 ADD PRIMARY KEY (`id`), ADD KEY `username` (`username`), ADD KEY `email` (`email`), ADD KEY `deleted_at` (`deleted_at`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
 ADD PRIMARY KEY (`id`), ADD KEY `name` (`name`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
 ADD PRIMARY KEY (`id`), ADD KEY `parent` (`parent`) USING BTREE, ADD KEY `slug` (`slug`) USING BTREE, ADD KEY `created_at` (`created_at`), ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `service_legals`
--
ALTER TABLE `service_legals`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
 ADD PRIMARY KEY (`id`), ADD KEY `timestamp` (`timestamp`);

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
 ADD PRIMARY KEY (`id`), ADD KEY `slug` (`slug`), ADD KEY `type` (`type`) USING BTREE;

--
-- Indexes for table `term_relations`
--
ALTER TABLE `term_relations`
 ADD PRIMARY KEY (`id`), ADD KEY `post_id` (`post_id`), ADD KEY `id` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD KEY `group_id` (`group_id`), ADD KEY `username` (`username`), ADD KEY `email` (`email`), ADD KEY `deleted_at` (`deleted_at`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advocates`
--
ALTER TABLE `advocates`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `discussions`
--
ALTER TABLE `discussions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21619;
--
-- AUTO_INCREMENT for table `service_legals`
--
ALTER TABLE `service_legals`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `term_relations`
--
ALTER TABLE `term_relations`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2026;
--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
