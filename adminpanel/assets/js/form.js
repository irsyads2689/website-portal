$(document).ready(function () {
    $('#form').on('submit', function(e) {
        e.preventDefault();

        $('#loader').show();
        $(this).find('#infoMessage').html('');
        $(this).ajaxSubmit({
            target: "#infoMessage",
            success: function() {
            	$('#loader').hide();
            },
            error: function() {
                alert('Internal server error.');
            }
        })
    })
	
    $('.editor').tinymce({
        selector: '.editor',
        // skin: 'omeoocms',
        height: 400,
        menubar: false,
        image_advtab: true,
        relative_urls: false,
        remove_script_host: false,
        verify_html: false,
        inline_styles : true,
        plugins: [
            'advlist autolink lists link image imagetools charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code'
        ],
        toolbar: 'styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview',
        content_css: 'http://www.tinymce.com/css/codepen.min.css',
        external_filemanager_path: base_url + "filemanager/",
        filemanager_title:"File Manager",
        external_plugins: { "filemanager" : base_url + "filemanager/plugin.min.js" },
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
            editor.on('keyup', function () {
                editor.save();
            });
        }
    });
});
