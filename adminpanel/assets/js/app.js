$(function() {

    // ========================================
    // Ajax Content Loader
    // ========================================
    $('body').on('click', '.ajaxan', function(event) {
        event.preventDefault();
        var url = $(this).attr('href');
        var title = $(this).attr('title');
        $('#loader').show();
        $('#content').load(url, function() {
            $('#loader').hide();
        });
        document.title = title + ' | Insan Indonesia';
        window.history.pushState("", title, url);
        $(this).parent().siblings().removeClass('active');
        $(this).parent().siblings().removeClass('open');
        $(this).parent().siblings().find('ul').css({display:'hidden'});
        $(this).parent().siblings().find('li').removeClass('active');
        $(this).parent().addClass('active');
        $(this).parent().parent().parent().siblings().removeClass('active');
        $(this).parent().parent().parent().siblings().removeClass('open');
        $(this).parent().parent().parent().siblings().find('li').removeClass('active');
        $(this).parent().parent().parent().addClass('open active');
    });

    // $(document).ajaxStart( function() {
    //     $('#loader').show();
    // });
    // $(document).ajaxStop( function() {
    //     $('#loader').hide();
    // });

    // $('.datetimes').datetimepicker({
    //     format: 'DD-MM-YYYY',
    //     ignoreReadonly: true,
    //     allowInputToggle: true
    // })
});

function onlyNumber(evt) {
    evt = (evt) ? evt : window.event;
    alert(evt);
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function isCurrency(number) {
    if(number != undefined) {
        var number = number.toString().replace(/,/g, ""),
            value =  number=='' ? parseFloat(0, 10) : parseFloat(number, 10),
            x = value.toString(),
            a = x.length % 3,
            b = x.substr(0, a),
            c = x.substr(a).match(/\d{3}/g),
            d = 0;
        if (c) {
            d = a ? ',' : '';
            b += d + c.join(',');
        }
        if(symbol != undefined) {
            b = symbol + b;
        }
        return b;
    }
}