$.extend($.fn.dataTable.defaults, {
    //"processing": true,
    serverSide: true,
    lengthMenu: [
        [10, 20, 50, 100, 150],
        [10, 20, 50, 100, 150]
    ],
    autoWidth: false,
    pageLength: 10,
    //"bStateSave": true,
    dom: '<"datatable-header"Bl>r<"datatable-body"t><"datatable-footer"ip>',
    language: {
        search: '<span>Filter:</span> _INPUT_',
        lengthMenu: '<span>Show:</span> _MENU_',
        emptyTable: 'No data available',
        zeroRecords: 'No matching records found',
        paginate: { 'first': '&laquo;', 'last': '&raquo;', 'next': '&rsaquo;', 'previous': '&lsaquo;' }
    },
    pagingType: "full_numbers",
    drawCallback: function () {
        // $('select').select2({
            // minimumResultsForSearch: Infinity
        // });
        // $('select.with_search').select2();
        // $('.dataTables_length select').select2({
            // minimumResultsForSearch: Infinity,
            // width: 'auto'
        // });
        // $('.date').datepicker({
            // format: "yyyy-mm-dd",
            // orientation: "right",
            // autoclose: true
        // });
        // $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        // $("input[type=checkbox]").iCheck({
            // checkboxClass: "icheckbox_flat-green"
        // })
        // $('[data-popup=tooltip]').tooltip();
    },
    preDrawCallback: function() {
        $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
    }
});

$(document).ready(function(){

    // $("input[type=checkbox]").iCheck({
    //     checkboxClass: "icheckbox_flat-green"
    // })
    // $('body').on('ifToggled', 'input[name="checkAll"]', function(){
    //     if($(this).is(':checked')) {
    //         $('input:checkbox').not(this).iCheck('check');
    //     } else {
    //         $('input:checkbox').not(this).iCheck('uncheck');
    //     }
    //     console.log($('input[name=data]').val());
    // });
    //$('select').selectpicker();

    // Bulk action
    // $('body').on('click', '#bulk-action-apply', function(e){
    //     e.preventDefault();
    //     var param = '';
    //     var bulk_district_id = $('#bulk_district_id').val();
    //     var bulk_city_id     = $('#bulk_city_id').val();
    //     var bulk_province_id = $('#bulk_province_id').val();
    //     if (bulk_district_id > 0) {
    //         param = '?district=' + bulk_district_id;
    //     } else if (bulk_city_id > 0) {
    //         param = '?city=' + bulk_city_id;
    //     } else if (bulk_province_id > 0) {
    //         param = '?province=' + bulk_province_id;
    //     console.log(param);
    //     }
    //     var action = $('#bulk-action-select').val();
    //     if (action == 'trash') {
    //         confirm_bulk_action(alert_remove_title, alert_remove_text, action+param, alert_success, alert_remove_success);
    //     } else if (action == 'delete') {
    //         confirm_bulk_action(alert_delete_title, alert_delete_text, action+param, alert_success, alert_delete_success);
    //     } else if (action == 'restore') {
    //         ajax_bulk_action(action+param, alert_success, alert_restore_success);
    //     }
    // });

    // function confirm_bulk_action(confirm_title, confirm_message, action, success_title, success_message) {
    //     swal({
    //         title: confirm_title,
    //         text: confirm_message,
    //         type: 'warning',
    //         showCancelButton: true,
    //         confirmButtonColor: '#f44336',
    //         confirmButtonText: 'Confirm'
    //     }, function (isConfirm) {
    //         if (isConfirm) {
    //             ajax_bulk_action(action, success_title, success_message);
    //         }
    //     });
    // }

    // function ajax_bulk_action(action, success_title, success_message) {
    //     $.ajax({
    //         url: module_url+'/'+action,
    //         data: $('input[name="data[]"]:checked').serialize(),
    //         method: 'post',
    //         success: function(response){
    //             if (response == "success") {
    //                 swal( success_title, success_message, 'success');
    //             }
    //             table.ajax.reload(null, false);
    //         }
    //     });
    // }

    // Delete button
    // $('body').on('click', '.btn-delete', function(e){
    //     e.preventDefault();
    //     var url = $(this).attr('href');
    //     swal({
    //         title: 'Are you sure that you want to delete this record',
    //         text: 'Data will be deleted permanently and cannot be restored.?',
    //         type: "warning",
    //         showCancelButton: true,
    //         confirmButtonColor: '#f44336',
    //         confirmButtonText: 'Confirm'
    //     }, function (isConfirm) {
    //         if (isConfirm) {
    //             $.ajax({
    //                 url: url,
    //                 method: 'post',
    //                 success: function(response){
    //                     if (response == "success") {
    //                         swal('Success!', 'Your data has been successfully deleted.', 'success');
    //                     }
    //                     table.ajax.reload(null, false);
    //                 }
    //             });
    //         }
    //     });
    // });

    // // Restore button
    // $('body').on('click', '.btn-restore', function(e){
    //     e.preventDefault();
    //     var url = $(this).attr('href');
    //     $.ajax({
    //         url: url,
    //         method: 'post',
    //         success: function(response){
    //             if (response == "success") {
    //                 swal(alert_success, alert_restore_success, 'success');
    //             }
    //             table.ajax.reload(null, false);
    //         }
    //     });
    // });

    // // delete button
    // $('body').on('click', '.btn-delete-permanent', function(e){
    //     e.preventDefault();
    //     var url = $(this).attr('href');
    //     swal({
    //         title: alert_delete_title,
    //         text: alert_delete_text,
    //         type: "warning",
    //         showCancelButton: true,
    //         confirmButtonColor: "#f44336",
    //         confirmButtonText: "Confirm"
    //     }, function (isConfirm) {
    //         if (isConfirm) {
    //             $.ajax({
    //                 url: url,
    //                 method: 'post',
    //                 success: function(response){
    //                     if (response == "success") {
    //                         swal(alert_success, alert_delete_success, 'success');
    //                     }
    //                     table.ajax.reload(null, false);
    //                 }
    //             });
    //         }
    //     });
    // });

    // // delete selected
    // $('body').on('click', '#btn_empty_trash', function(e){
    //     e.preventDefault();
    //     swal({
    //         title: alert_delete_title,
    //         text: alert_delete_text,
    //         type: "warning",
    //         showCancelButton: true,
    //         confirmButtonColor: "#f44336",
    //         confirmButtonText: "Confirm"
    //     }, function (isConfirm) {
    //         if (isConfirm) {
    //             $.ajax({
    //                 url: module_url+'/empty_trash',
    //                 method: 'post',
    //                 success: function(response){
    //                     if (response == "success") {
    //                         swal(alert_success, alert_delete_success, 'success');
    //                     }
    //                     table.ajax.reload(null, false);
    //                 }
    //             });
    //         }
    //     });
    // });

    // // ajax button
    // $('body').on('click', '.btn-ajax', function(e){
    //     e.preventDefault();
    //     var url = $(this).attr('href');
    //     swal({
    //         title: 'Are you sure?',
    //         type: "warning",
    //         showCancelButton: true,
    //         confirmButtonColor: "#f44336",
    //         confirmButtonText: "Confirm"
    //     }, function (isConfirm) {
    //         if (isConfirm) {
    //             $.ajax({
    //                 url: url,
    //                 method: 'post',
    //                 success: function(response) {
    //                     var data = $.parseJSON(response);
    //                     noty({
    //                         text: data.message,
    //                         type: data.status,
    //                         layout: "topCenter",
    //                         dismissQueue: true,
    //                         timeout: 4000,
    //                         animation: {
    //                             open: "animated bounceIn",
    //                             close: "animated bounceOut",
    //                         }
    //                     });
    //                     table.ajax.reload(null, false);
    //                 }
    //             });
    //         }
    //     });
    // });
});
