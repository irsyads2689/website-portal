<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class Handle_data {

    public function __construct() {
        $this->app =& get_instance();
    }

    private function get_data($name_file, $table, $method, $param)
    {
        $app = $this->app;
        $app->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));

        $data = $app->cache->get($name_file);

        if(!$data) {
            $app->cache->delete($name_file);
            
            $data = $app->{$table}->{$method}($param);

            $result = json_encode($data);

            $app->cache->save($name_file, $result, 15);
        } else {
            $data = json_decode($data);
        }

        return $data;
    }

    public function header($name_file, $table, $method, $param = array()) {
        $data = $this->get_data($name_file, $table, $method, $param);
        return $data;
    }

    public function sidebar($name_file, $table, $method, $param = array()) {
        $data = $this->get_data($name_file, $table, $method, $param);
        return $data;
    }

    public function page_home($name_file, $table, $method, $param = array()) {
        $data = $this->get_data($name_file, $table, $method, $param);
        return $data;
    }

    public function page_about_us($name_file, $table, $method, $param = array()) {
        $data = $this->get_data($name_file, $table, $method, $param);
        return $data;
    }

    public function page_program($name_file, $table, $method, $param = array()) {
        $data = $this->get_data($name_file, $table, $method, $param);
        return $data;
    }
    public function page_gallery($name_file, $table, $method, $param = array()) {
        $data = $this->get_data($name_file, $table, $method, $param);
        return $data;
    }
    public function page_post($name_file, $table, $method, $param = array()) {
        $data = $this->get_data($name_file, $table, $method, $param);
        return $data;
    }
}
