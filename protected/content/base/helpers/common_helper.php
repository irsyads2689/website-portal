<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Omeoo Framework
 * A framework for PHP development
 *
 * @package     Omeoo Framework
 * @author      Omeoo Dev Team
 * @copyright   Copyright (c) 2016, Omeoo Media (http://www.omeoo.com)
 */

if ( ! function_exists('get_option')) {
    function get_option($name = null)
    {
        $app =& get_instance();
        if (is_null($name))
        {
            $setting = $app->option_model->get_all();
            if ($setting) {
                foreach($setting as $set) {
                    $settings[$set->name] = $set->value;
                }
            }
            return (isset($settings)) ? $settings : '';
        }
        else
        {
            $setting = $app->option_model->fields('id,value')->get(['name' => $name]);
            return ($setting) ? $setting->value : '';
        }
    }
}

 if ( ! function_exists('str_first_upper')) {
    function str_first_upper($string = null)
    {
        return ucwords(strtolower($string));
    }
}

if ( ! function_exists('share_social')) 
{
    function share_social($social=null) 
    {
        $url = current_url();
        switch ($social) 
        {
            case 'google':
                $share_url = 'https://plus.google.com/share?url='.$url; break;
            case 'twitter':
                $share_url = 'http://twitter.com/share?url='.$url; break;
            case 'facebook':
                $share_url = 'http://www.facebook.com/sharer/sharer.php?u='.$url; break;
            case 'linkedin':
                $share_url = 'http://www.linkedin.com/shareArticle?mini=true&url='.$url.'&summary='; break;
            case 'email':
                $share_url = 'mailto:?subject=I wanted you to see this site&amp;body=Check out this site '.$url.'&summary='; break;
            default:
                $share_url = ''; break;
        }

        return $share_url;
    }
}

if (!function_exists('get_meta')) {
    function get_meta($type = 'home', $data = null) {
        $meta = array();
        $app =& get_instance();
        $web = get_option();

        switch($type) {
            case "category":
                $title          = $data->name;
                $keywords       = $data->name;
                $description    = $data->description;
                $site_name      = $web['site_name'];
                $url            = current_url();
                $image          = '';
                $twitter_id     = (isset($web['social_twitter'])) ? str_replace('https://twitter.com/', '', $web['social_twitter']) : '';

                break;
            case "posts":
                $title          = $data->title;
                $keywords       = $data->meta_keywords;
                $description    = $data->meta_description;
                $site_name      = $web['site_name'];
                $url            = current_url();
                $image          = get_image('posts', $data->image);
                $twitter_id     = (isset($web['social_twitter'])) ? str_replace('https://twitter.com/', '', $web['social_twitter']) : '';

                break;                
            default: 
                $title          = $web['meta_title'];
                $keywords       = $web['meta_keywords'];
                $description    = $web['meta_description'];
                $site_name      = $web['site_name'];
                $url            = current_url();
                $image          = '';
                $twitter_id     = (isset($web['social_twitter'])) ? str_replace('https://twitter.com/', '', $web['social_twitter']) : '';

                break;
        }
       
        $app->output->set_meta('title', $title);
        $app->output->set_meta('description', $description);
        $app->output->set_meta('keywords', $keywords);
        // meta fb
        $app->output->set_meta('og:type', 'article');
        $app->output->set_meta('og:site_name', $site_name);
        $app->output->set_meta('og:title', $title);
        $app->output->set_meta('og:description', $description);
        $app->output->set_meta('og:url', $url);
        $app->output->set_meta('og:image', $image);
        // meta twitter
        $app->output->set_meta('twitter:card', 'article');
        $app->output->set_meta('twitter:site', $twitter_id);
        $app->output->set_meta('twitter:creator', '@'.$twitter_id);
        $app->output->set_meta('twitter:title', $title);
        $app->output->set_meta('twitter:description', $description);
        $app->output->set_meta('twitter:image', $image);
    }
}

if (!function_exists('get_theme_url')) 
{
    function get_theme_url()
    {
        $theme = get_option('theme');
        return base_url().'themes/'.$theme;
    }
}

if ( ! function_exists('get_sidebar')) 
{
    function get_sidebar($data=null)
    {
        $app =& get_instance();
        $theme = get_option('theme');

        return $app->load->view($theme.'/sidebar', $data);
    }
}

if (!function_exists('get_assets_url')) {
    function get_assets_url()
    {
        $theme = get_option('theme');
        return base_url().'themes/'.$theme.'/assets';
    }
}

if (!function_exists('get_footer')) {
    function get_footer()
    {
        $app =& get_instance();
        $theme = get_option('theme');
        return $app->load->view($theme.'/footer');
    }
}

if (!function_exists('get_header')) {
    function get_header()
    {
        $app =& get_instance();
        $theme = get_option('theme');
        return $app->load->view($theme.'/header');
    }
}

if (!function_exists('get_image')) {
    function get_image($folder = '', $image) {
        $path = 'uploads/'.$folder.$image;
        $path = base_url($path);
        return $path;
    }
}

if (!function_exists('get_image_thumb')) {
    function get_image_thumb($fullname) {
        $parsed_url = parse_url($fullname, PHP_URL_PATH);
        $dir        = pathinfo(str_replace('/jurnaldokter','',$parsed_url), PATHINFO_DIRNAME);
        // $dir        = pathinfo($parsed_url, PATHINFO_DIRNAME);
        $extension  = pathinfo($fullname, PATHINFO_EXTENSION);
        $filename   = pathinfo($fullname, PATHINFO_FILENAME);
        $image_dir   = '..' . $dir . "/" . $filename . "_thumb." . $extension;

        if(!file_exists($image_dir)) {
            image_resize($parsed_url);
        }

        $image_thumb = base_url($dir . "/" . $filename . "_thumb." . $extension);
        return $image_thumb;
    }
}

if ( ! function_exists('image_resize')) {
    function image_resize($path_image, $width = 75)
    {
        $app =& get_instance();

        $config['image_library']    = 'gd2';
        $config['source_image']     = $path_image;
        $config['create_thumb']     = TRUE;
        $config['maintain_ratio']   = TRUE;
        $config['width']            = $width;
        // $config['height']           = 0;

        $app->load->library('image_lib', $config);

        $app->image_lib->resize();
    }
}

 if (!function_exists('youtube_thumb')) {
     function youtube_thumb($youtube_id) {
        return 'http://img.youtube.com/vi/'. $youtube_id .'/mqdefault.jpg';
     }
 }

 if (!function_exists('youtube_embed')) {
     function youtube_embed($youtube_id) {
        return 'https://www.youtube.com/embed/'. $youtube_id;
     }
 }

if (!function_exists('nl2p')) {
    function nl2p($string, $line_breaks = true, $xml = true) {
        $string = str_replace(array('<p>', '</p>', '<br>', '<br />'), '', $string);

        if ($line_breaks == true)
            return '<p>'.preg_replace(array("/([\n]{2,})/i", "/([^>])\n([^<])/i"), array("</p>\n<p>", '$1<br'.($xml == true ? ' /' : '').'>$2'), trim($string)).'</p>';
        else
            return '<p>'.preg_replace(
            array("/([\n]{2,})/i", "/([\r\n]{3,})/i","/([^>])\n([^<])/i"),
            array("</p>\n<p>", "</p>\n<p>", '$1<br'.($xml == true ? ' /' : '').'>$2'),
            trim($string)).'</p>';
    }
}

if (!function_exists('encode')) {
    function encode($string) {
        return encrypt_decrypt('encrypt', $string);
    }
}

if (!function_exists('decode')) {
    function decode($string) {
        return encrypt_decrypt('decrypt', $string);
    }
}

if (!function_exists('encrypt_decrypt')) {
    function encrypt_decrypt($action, $string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'OmeooFramework';
        $secret_iv = 'Omeoo Media';

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }
}

if (!function_exists('to_array')) {
    function to_array($json) {
        $array = json_decode($json, TRUE);
        return (is_array($array)) ? $array : FALSE;
    }
}

if (!function_exists('to_json')) {
    function to_json($array) {
        return (is_array($array)) ? json_encode($array) : FALSE;
    }
}

if (!function_exists('age')) {
    function age($birthdate) {
        $from = new DateTime($birthdate);
        $to   = new DateTime('today');
        return $from->diff($to)->y;
    }
}

if (!function_exists('currency')) {
    function currency($val) {
        $value = number_format($val, 0, ',', '.');
        return $value;
    }
}

/*==-- Success message --==*/
if (!function_exists('successMessage')) {
    function successMessage($message, $redirect = NULL) {
        $msg = '<div class="alert alert-success alert-styled-left alert-bordered animated bounceIn">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                ' . $message . '</div>';
        if ($redirect != NULL) {
            $msg .= '<script>setTimeout("window.location=\'' . site_url($redirect) . '\'",3000)</script>';
        } else {
            $msg .= '<script>$("html, body").animate({ scrollTop: 0 }, 600);</script>';
        }
        return $msg;
    }
}

/*==-- Error message --==*/
if (!function_exists('errorMessage')) {
    function errorMessage($message) {
        $msg = '<div class="alert alert-danger alert-styled-left alert-bordered animated bounceIn">
				<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
				' . $message . '</div>';
        $msg .= '<script>$("html, body").animate({ scrollTop: 0 }, 600);</script>';
        return $msg;
    }
}

if (!function_exists('clearForm')) {
    function clearForm() {
        return '<script>$("input,textarea,select").val("");</script>';
    }
}

if (!function_exists('ajaxRedirect')) {
    function ajaxRedirect($redirect = '', $timer = 10000) {
        if ($timer == 0) {
            if (substr($redirect, 0, 4) == 'http') {
                return '<script>window.location.href="' . $redirect . '";</script>';
            } else {
                return '<script>window.location.href="' . site_url($redirect) . '";</script>';
            }
        } else {
            if (substr($redirect, 0, 4) == 'http') {
                return '<script>setTimeout("window.location.href=\'' . $redirect . '\'",' . $timer . ');</script>';
            } else {
                return '<script>setTimeout("window.location.href=\'' . site_url($redirect) . '\'",' . $timer . ');</script>';
            }
        }
    }
}

if (!function_exists('excerpt')) {
    function excerpt($content, $length = 200, $striptags = FALSE, $delimiter = ''){
		$excerpt = explode('<div style="page-break-after: always;"><span style="display:none">&nbsp;</span></div>', $content, 2);
		if(count($excerpt) > 1){
			$excerpt_fix = ($striptags!=FALSE) ? strip_tags(htmlspecialchars_decode($excerpt[0])) : closetags(htmlspecialchars_decode($excerpt[0]));
		} else {
			$excerpt_fix = ($striptags!=FALSE) ? strip_tags(substr(htmlspecialchars_decode($content), 0, $length)) : closetags(substr(htmlspecialchars_decode($content), 0, $length));
		}
		return ($length < strlen($content)) ? $excerpt_fix.$delimiter : $excerpt_fix;
	}
}

if (!function_exists('closetags')) {
    function closetags($html){
		preg_match_all('#<([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
		$openedtags = $result[1];
		preg_match_all('#</([a-z]+)>#iU', $html, $result);
		$closedtags = $result[1];

		$len_opened = count($openedtags);

		if (count($closedtags) == $len_opened) {
			return $html;
		}

		$openedtags = array_reverse($openedtags);

		for ($i = 0; $i < $len_opened; $i++) {
			if (!in_array($openedtags[$i], $closedtags)) {
				$html .= '</' . $openedtags[$i] . '>';
			} else {
				unset($closedtags[array_search($openedtags[$i], $closedtags)]);
			}
		}
		return $html;
	}
}

if (!function_exists('date_indo')) {
    function date_indo($fulldate) {
        $date = substr($fulldate, 8, 2);
        $month = get_month(substr($fulldate, 5, 2));
        $year = substr($fulldate, 0, 4);
        return $date . ' ' . $month . ' ' . $year;
    }
}

if (!function_exists('date_simple')) {
    function date_simple($fulldate) {
        $date = substr($fulldate, 8, 2);
        $month = substr($fulldate, 5, 2);
        $year = substr($fulldate, 0, 4);
        return $date . '/' . $month . '/' . $year;
    }
}

if (!function_exists('normal_date')) {
    function normal_date($fulldate) {
        $date = substr($fulldate, 8, 2);
        $month = get_month3(substr($fulldate, 5, 2));
        $year = substr($fulldate, 0, 4);
        return $date . '/' . $month . '/' . $year;
    }
}

if (!function_exists('date_time')) {
    function date_time($fulldate) {
        $date = substr($fulldate, 0, 2);
        $month = get_month2(substr($fulldate, 3, 3));
        $year = substr($fulldate, 7, 4);
        $time = substr($fulldate, 12, 5);
        return $year . '-' . $month . '-' . $date . ' ' . $time;
    }
}

if (!function_exists('mysql_date')) {
    function mysql_date($fulldate) {
        $date = substr($fulldate, 0, 2);
        $month = get_month2(substr($fulldate, 3, 3));
        $year = substr($fulldate, 7, 4);
        return $year . '-' . $month . '-' . $date;
    }
}

if (!function_exists('get_month')) {
    function get_month($month) {
        switch ($month) {
            case 1: return "Januari";
            case 2: return "Februari";
            case 3: return "Maret";
            case 4: return "April";
            case 5: return "Mei";
            case 6: return "Juni";
            case 7: return "Juli";
            case 8: return "Agustus";
            case 9: return "September";
            case 10: return "Oktober";
            case 11: return "November";
            case 12: return "Desember";
        }
    }
}

if (!function_exists('get_month2')) {
    function get_month2($month) {
        switch ($month) {
            case "Jan": return "01";
            case "Feb": return "02";
            case "Mar": return "03";
            case "Apr": return "04";
            case "May": return "05";
            case "Jun": return "06";
            case "Jul": return "07";
            case "Aug": return "08";
            case "Sep": return "09";
            case "Oct": return "10";
            case "Nov": return "11";
            case "Dec": return "12";
        }
    }
}

if (!function_exists('get_month3')) {
    function get_month3($month) {
        switch ($month) {
            case "01": return "Jan";
            case "02": return "Feb";
            case "03": return "Mar";
            case "04": return "Apr";
            case "05": return "May";
            case "06": return "Jun";
            case "07": return "Jul";
            case "08": return "Aug";
            case "09": return "Sep";
            case "10": return "Oct";
            case "11": return "Nov";
            case "12": return "Dec";
        }
    }
}

if (!function_exists('get_day')) {
    function get_day($day) {
        switch ($day) {
            case "Mon": return "Senin";
            case "Tue": return "Selasa";
            case "Wed": return "Rabu";
            case "Thu": return "Kamis";
            case "Fri": return "Jum'at";
            case "Sat": return "Sabtu";
            case "Sun": return "Minggu";
        }
    }
}

if (!function_exists('get_day2')) {
    function get_day2($day) {
        switch ($day) {
            case 1: return "Mon";
            case 2: return "Tue";
            case 3: return "Wed";
            case 4: return "Thu";
            case 5: return "Fri";
            case 6: return "Sat";
            case 7: return "Sun";
        }
    }
}

if (!function_exists('get_day3')) {
    function get_day3($day) {
        switch ($day) {
            case 1: return "Senin";
            case 2: return "Selasa";
            case 3: return "Rabu";
            case 4: return "Kamis";
            case 5: return "Jum'at";
            case 6: return "Sabtu";
            case 7: return "Minggu";
        }
    }
}

if (!function_exists('get_day4')) {
    function get_day4($day) {
        switch ($day) {
            case "Mon": return 1;
            case "Tue": return 2;
            case "Wed": return 3;
            case "Thu": return 4;
            case "Fri": return 5;
            case "Sat": return 6;
            case "Sun": return 7;
        }
    }
}

/* End of file common_helper.php */
/* Location: ./system/helpers/common_helper.php */
