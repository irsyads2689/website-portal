<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller']    = 'home';
$route['home']               	= 'home';

$route['tentang-kami/(:any)']   = 'about_us/index/$1';
$route['program/(:any)']   		= 'program/index/$1';
$route['galeri']   				= 'gallery/index';
$route['galeri/(:any)']			= 'gallery/detail/$1';
$route['kontak-kami']			= 'contact_us/index';
$route['(:any)']      			= 'post/index/$1';
$route['(:any)/(:any)'] 		= 'post/index/$2';

$route['404_override']          = 'notfound';
$route['translate_uri_dashes']  = FALSE;
