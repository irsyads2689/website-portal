<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Program_model extends X_Model {

    public $table = 'programs';
    public $primary_key = 'id';

    public function all_program($param) {
    	$get_all_program = $this->order_by('sort_order', 'asc')->get_all(['parent'=>0, 'is_active'=>1]);
    	return $get_all_program;
    }

    public function get_program($param) {
    	$result = array();
    	$program = $this->get(['slug'=>$param['slug'], 'is_active'=>1]);
    	if($program) {
    		$result['program'] = $program;
    		$result['list_program'] = $this->order_by('created_at', 'asc')->get_all(['parent'=>$program->id]);
    	}
    	return $result;
    }
}
