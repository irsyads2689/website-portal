<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Post_model extends X_Model {

    public $table = 'posts';
    public $primary_key = 'id';

    public function home_profile($param) {
        $home_profile = $this->fields('content, slug, type')->get(['type'=>'tentang-kami', 'slug'=>'profil-yayasan', 'is_active'=>1]);
        return $home_profile;
    }

    public function home_news($param) {
        $home_news = $this->fields('id, title, slug, type, image, image_folder, created_at')->order_by('created_at', 'desc')->limit(3)->get_all(['type'=>'berita', 'is_active'=>1]);
        return $home_news;
    }

    public function latest_news($param) {
        $latest_news = $this->fields('id, title, slug, type, image, image_folder, created_at')->order_by('created_at', 'desc')->limit(5)->get_all(['type'=>'berita', 'is_active'=>1]);
        return $latest_news;
    }

    public function all_about($param) {
        $all_about = $this->order_by('sort_order', 'asc')->get_all(['type'=>'tentang-kami', 'is_active'=>1]);
        return $all_about;
    }

    public function about_us($param) {
        $about_us = $this->fields('id, title, content, image, image_folder')->get(['type'=>'tentang-kami', 'slug'=>$param['slug'], 'is_active'=>1]);
        return $about_us;
    }

    public function all_posts($param) {
        $all_posts = $this->fields('id, title, slug, type, image, image_folder, created_at')->order_by('created_at', 'desc')->limit(6)->get_all(['type'=>$param['slug'], 'is_active'=>1]);
        return $all_posts;
    }
    public function get_post($param) {
        $get_post = $this->fields('id, title, slug, type, content, image, image_folder, created_at')->get(['type'=>$param['type'], 'slug'=>$param['slug'], 'is_active'=>1]);
        return $get_post;
    }

}
