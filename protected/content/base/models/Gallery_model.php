<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery_model extends X_Model {

    public $table = 'galleries';
    public $primary_key = 'id';

    public function all_gallery($param) {
        $image = ', (SELECT ig.image FROM galleries ig WHERE ig.parent=galleries.id ORDER BY ig.sort_order ASC LIMIT 1) as image';
        $image_folder = ', (SELECT ifg.image_folder FROM galleries ifg WHERE ifg.parent=galleries.id ORDER BY ifg.sort_order asc LIMIT 1) as image_folder';
    	$all_gallery = $this->fields('id, title, slug, created_at'.$image.$image_folder)->order_by('created_at', 'desc')->get_all(['parent'=>0, 'is_active'=>1]);
    	return $all_gallery;
    }

    public function get_gallery($param) {
    	$result = array();
    	$gallery = $this->get(['slug'=>$param['slug'], 'is_active'=>1]);
    	if($gallery) {
    		$result['gallery'] = $gallery;
    		$result['list_gallery'] = $this->order_by('sort_order', 'asc')->get_all(['parent'=>$gallery->id]);
    	}
    	return $result;
    }
}
