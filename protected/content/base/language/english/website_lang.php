<?php

// header
$lang['menu_home']  			= 'Home';
$lang['menu_profile']  			= 'Profile';
$lang['menu_advocate']  		= 'Team Advocate';
$lang['menu_service_legal']  	= 'Legal Service';
$lang['menu_consult']  			= 'Consult';
$lang['menu_contact']  			= 'Contact Us';
$lang['menu_news']  			= 'News';
$lang['search']  				= 'Search News';

// body
$lang['our_legal_service']  	= 'Our Legal Services';
$lang['lalets_news']  			= 'Latest News';
$lang['top_news']  				= 'Top News';
$lang['more']  				= 'More';

// footer
$lang['have_issue']  			= 'Do you have a <span class="color">legal</span> issue ?';
$lang['do_not_hesitate']  		= 'Do not hesitate to contact us';
$lang['our_location']  			= 'Our Location';
$lang['follow_us']  			= 'Follow Us';
$lang['about_us']  				= 'About Us';
$lang['call_center']  			= 'Call Center';
$lang['language']  				= 'Language';
$lang['indonesian']  			= 'Indonesian';
$lang['english']  				= 'Inggris';


