<?php

// header
$lang['menu_home']  			= 'Home';
$lang['menu_profile']  			= 'Profil';
$lang['menu_advocate']  		= 'Tim Advokat';
$lang['menu_service_legal']  	= 'Jasa Bantuan Hukum';
$lang['menu_consult']  			= 'Konsultasi';
$lang['menu_contact']  			= 'Kontak Kami';
$lang['menu_news']  			= 'Berita';
$lang['search']  				= 'Cari Berita';

// body
$lang['our_legal_service']  	= 'Layanan Jasa Hukum Kami';
$lang['latest_news']  			= 'Berita Terbaru';
$lang['top_news']  				= 'Berita Teratas';
$lang['more']  				= 'Lainnya';

// footer
$lang['have_issue']  			= 'Anda mempunyai masalah <span class="color">hukum</span> ?';
$lang['do_not_hesitate']  		= 'Jangan ragu menghubungi kami';
$lang['our_location']  			= 'Lokasi Kami';
$lang['follow_us']  			= 'Ikuti Kami';
$lang['about_us']  				= 'Tentang Kami';
$lang['call_center']  			= 'Call Center';
$lang['language']  				= 'Bahasa';
$lang['indonesian']  			= 'Indonesia';
$lang['english']  				= 'Inggris';

