<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends App_Controller {

    public function __construct()
    {
        parent::__construct();
    }

	public function set_language() {
		$this->load->helper('cookie');
		$lang = $this->input->post('lang');
		print_r($lang);
		set_cookie('__lang', $lang, 86400);
	}


}
