<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notfound extends App_Controller {

    public function __construct()
    {
        parent::__construct();
    }

	public function index() {
        $setting    = get_option();
        $theme      = isset($setting['theme']) ? $setting['theme']:'';
        $title      = 'Halaman tidak ditemukan - '.$setting['site_name'];

        $this->output->set_template($theme);
        $this->output->set_title($title);
        // get_meta($partner);
		$this->load->view($theme.'/404.php');
	}


}
