<?php
//defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Omeoo Framework
 * A framework for PHP development
 *
 * @package     Omeoo Framework
 * @author      Omeoo Dev Team
 * @copyright   Copyright (c) 2016, Omeoo Media (http://www.omeoo.com)
 */

/*
 * Database configuration
 */

define('DB_HOST', 'localhost');
define('DB_NAME', 'attack');
define('DB_USER', 'root');
define('DB_PASS', '');
