<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Program extends App_Controller {

    public function __construct() {
        parent::__construct();

        $this->data = array();
    }

    public function index($slug) {
    	$data = $this->handle_data->page_program($slug, 'program_model', 'get_program', array('slug'=>$slug));
    	if(!$data) {
    		$this->notfound();
    	} else {
    		$program = is_object($data) ? $data->program : $data['program'];
    		$list_program = is_object($data) ? $data->list_program : $data['list_program'];
    		
			$setting    = get_option();
			$theme      = isset($setting['theme']) ? $setting['theme']:'';
            $title      = isset($setting['site_name']) ? $program->title .' | '. $setting['site_name'] : $program->title;

			$this->output->set_template($theme);
			$this->output->set_title($title);
			get_meta('home', $program->title);

	        $this->data['program'] = $program;
	        $this->data['list_program'] = $list_program;

			$this->load->view($theme.'/program', $this->data);
		}
    }

    private function notfound()
    {
        $theme = get_option('theme');
        $this->output->set_template($theme);
        $this->output->set_title('Halaman Tidak Ditemukan');
        $this->data['is_page'] = 'pagenotfound';
        
        $this->load->view($theme.'/404', $this->data);
    }
}
