<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends App_Controller {

    public function __construct() {
        parent::__construct();

        $this->data = array();
    }

    public function index($slug = '') {
		if($slug=='') {
			$this->all_gallery();
		} else {
			$this->detail($slug);
		}
    }

    public function all_gallery() {
		$setting    = get_option();
		$theme      = isset($setting['theme']) ? $setting['theme']:'';
		$title      = isset($setting['site_name']) ? 'Galeri | '. $setting['site_name'] : 'Galeri';

		$this->output->set_template($theme);
		$this->output->set_title($title);
		get_meta('home', 'Galeri');

        $this->data['galleries']  = $this->handle_data->page_gallery('all_gallery', 'gallery_model', 'all_gallery');

		$this->load->view($theme.'/gallery', $this->data);
    }

    public function detail($slug) {
    	$data = $this->handle_data->page_program($slug, 'program_model', 'get_program', array('slug'=>$slug));
    	if(!$data) {
    		$this->notfound();
    	} else {
    		$gallery = is_object($data) ? $data->gallery : $data['gallery'];
    		$list_gallery = is_object($data) ? $data->list_gallery : $data['list_gallery'];

			$setting    = get_option();
			$theme      = isset($setting['theme']) ? $setting['theme']:'';
            $title      = isset($setting['site_name']) ? $gallery->title .' | '. $setting['site_name'] : $gallery->title;

			$this->output->set_template($theme);
			$this->output->set_title($title);
			get_meta('home', $gallery->title);

	        $this->data['gallery'] = $gallery;
	        $this->data['list_gallery'] = $list_gallery;

			$this->load->view($theme.'/gallery-single', $this->data);
		}
    }

    private function notfound()
    {
        $theme = get_option('theme');
        $this->output->set_template($theme);
        $this->output->set_title('Halaman Tidak Ditemukan');
        $this->data['is_page'] = 'pagenotfound';
        
        $this->load->view($theme.'/404', $this->data);
    }
}
