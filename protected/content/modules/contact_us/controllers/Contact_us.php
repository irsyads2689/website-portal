<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us extends App_Controller {

    public function __construct() {
        parent::__construct();

        $this->data = array();
    }

    public function index() {
		$setting    = get_option();
		$theme      = isset($setting['theme']) ? $setting['theme']:'';
            $title      = isset($setting['site_name']) ? 'Kontak Kami | '. $setting['site_name'] : 'Kontak Kami';

		$this->output->set_template($theme);
		$this->output->set_title($title);
		get_meta('home', 'Kontak Kami');


		$this->load->view($theme.'/contact_us', $this->data);
    }
}
