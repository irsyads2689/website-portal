<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Post extends App_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->data = array();
    }

    public function index($slug)
    {
        $url_segments   = $this->uri->segment_array();
        $count_segments = count($url_segments);
        
        $post  = '';
        $all_posts = '';

        if($count_segments==1) {
            $all_posts = $this->handle_data->page_post($slug, 'post_model', 'all_posts', ['slug'=>$slug]);
        } elseif($count_segments==2) {
            $post = $this->handle_data->page_post($slug, 'post_model', 'get_post', ['type'=>$url_segments[1], 'slug'=>$slug]);
        }

        if($post) {
            $this->page_post($post);            
        }
        elseif($all_posts) {
            $this->page_all_posts($all_posts, $slug);            
        } 
        else {
            $this->notfound();
        }
    }

    public function page_post($post)
    {
        $setting    = get_option();
        $theme      = isset($setting['theme']) ? $setting['theme']:'default';
        $title      = isset($setting['site_name']) ? $post->title.' | '.$setting['site_name'] : $post->title;

        $this->output->set_template($theme);
        $this->output->set_title($title);
        get_meta('post', $post);
        
		$this->view_count($post->id);
        $this->data['post'] = $post;

        $this->load->view($theme.'/post-single', $this->data);
    }
	
    private function page_all_posts($posts, $slug)
    {
		if(!$this->input->is_ajax_request()) {
			$setting    = get_option();
			$theme      = isset($setting['theme']) ? $setting['theme']:'';
			$title      = isset($setting['site_name']) ? str_first_upper($slug).' | '.$setting['site_name'] : str_first_upper($slug);

			$this->output->set_template($theme);
			$this->output->set_title($title);
			get_meta('home', $slug);
			
			
			// $limit 			= 12;
			// $total_article 	= $this->post_model->get_article_by_category($category->id, 'count');
			// $total_page 	= ceil($total_article / $limit);

			// $this->data['category'] 	 = $category;
			// $this->data['total_page'] 	 = $total_page;
            $this->data['title'] = str_first_upper($slug);
			$this->data['posts'] = $posts;

            // $next_page = $total_page > 1 ? $total_page : 1;
            // $this->data['next_page']  = $next_page;
            // $this->data['try']        = $total_page > 1 ? 0:3;

			$this->load->view($theme.'/post-all', $this->data);
		} else {
			$this->load_category($category);
		}
    }
	private function load_category($category) {
		$param = $this->input->get();
		$result['status'] = false;
		if($param) {
			$limit = 12;
			$start = $limit * ($param['next_page'] - 1);
			$total_article 	= $this->post_model->get_article_by_category($category->id, 'count');
			
			$articles = $this->post_model->limit($limit, $start)->get_article_by_category($category->id);
			$data = '';
  			if($articles) {
  				foreach($articles as $row) {
  					$post_url 		= site_url($row->category_slug.'/'.$row->slug);
  					$post_title 	= $row->title;
  					$post_image 	= get_image('posts', $row->image);
  					// $category_url 	= site_url($row->category_slug);
  					// $category_title = $row->category;
  					$data .= '
			            <div class="col-md-6">
			              	<article class="entry card">
				                <div class="entry__img-holder card__img-holder">
				                  	<a href="'.$post_url.'">
				                    	<div class="thumb-container thumb-70">
				                      		<img data-src="'.$post_image.'" src="'.$post_image.'" class="entry__img lazyload" alt="'.$post_title.'" />
				                    	</div>
				                  	</a>
				                  	<!-- <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet">world</a> -->
				                </div>

			                	<div class="entry__body card__body">
			                  		<div class="entry__header">
					                    <h2 class="entry__title">
					                      	<a href="'.$post_url.'">'.$post_title.'</a>
					                    </h2>
			                  		</div>
			                	</div>
			              	</article>
			            </div>
  					';
  				}

				$result['status'] 	 = true;
                $result['data']      = $data;

                $next_page = intval($param['next_page'])+1;
                $try = $next_page > $param['total_page'] ? 3 : 0;

                $result['next_page']  = $next_page;
                $result['try']        = $try;
			} 
		} else {
			$result['err_msg'] = '<p>Terjadi kesalaha pada server, silahkan scroll kembali.</p>';
		}
			
		echo json_encode($result);
	}

    private function notfound()
    {
        $theme = get_option('theme');
        $this->output->set_template($theme);
        $this->output->set_title('Halaman Tidak Ditemukan');
        $this->data['is_page'] = 'pagenotfound';
        
        $this->load->view($theme.'/404', $this->data);
    }

	private function view_count($post_id='') {
		$post = $this->post_model->get($post_id);
		if($post) {
			$view_count = intval($post->view_count) + 1;
			$this->post_model->update(['view_count' => $view_count], $post_id);
		}
	}
}
