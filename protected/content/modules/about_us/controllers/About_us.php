<?php defined('BASEPATH') OR exit('No direct script access allowed');

class About_us extends App_Controller {

    public function __construct() {
        parent::__construct();

        $this->data = array();
    }

    public function index($slug) {
    	$profile = $this->handle_data->page_about_us($slug, 'post_model', 'about_us', array('slug'=>$slug));
    	if(!$profile) {
    		$this->notfound();
    	} else {
    		$setting    = get_option();
    		$theme      = isset($setting['theme']) ? $setting['theme']:'';
            $title      = isset($setting['site_name']) ? $profile->title .' | '. $setting['site_name'] : $profile->title;

    		$this->output->set_template($theme);
    		$this->output->set_title($title);
    		get_meta('home', $profile->title);

            $this->data['profile']  = $profile;

    		$this->load->view($theme.'/about_us', $this->data);
        }
    }

    private function notfound()
    {
        $theme = get_option('theme');
        $this->output->set_template($theme);
        $this->output->set_title('Halaman Tidak Ditemukan');
        $this->data['is_page'] = 'pagenotfound';
        
        $this->load->view($theme.'/404', $this->data);
    }
}
