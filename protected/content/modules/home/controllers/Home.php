<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends App_Controller {

    public function __construct() {
        parent::__construct();

        $this->data = array();
    }

    public function index() {
		$setting    = get_option();
		$theme      = isset($setting['theme']) ? $setting['theme']:'';
		$title      = (isset($setting['meta_title']) AND $setting['meta_title']!='') ? $setting['meta_title'].' | '.$setting['site_name'] : $setting['site_name'];

		$this->output->set_template($theme);
		$this->output->set_title($title);
		get_meta('home');

        $this->data['profile']  = $this->handle_data->page_home('home_profile', 'post_model', 'home_profile');
        $this->data['programs'] = $this->handle_data->page_home('all_program', 'program_model', 'all_program');
        $this->data['news']     = $this->handle_data->page_home('home_news', 'post_model', 'home_news');

		$this->load->view($theme.'/home', $this->data);
    }

}
