<?php defined('BASEPATH') or exit('No direct script access allowed!');

class Sqlserver extends App_Controller
{

    public function __construct()
    {
        parent::__construct();

        // $this->load->library('administration');
        // $this->administration->logged();
        // $this->load->model('post/post_model');

        $this->data = [
            'module_title'    => 'Halaman',
            'module_icon'     => 'icon-magazine',
            'module_url'      => site_url('page'),
            'menu'            => ['menu' => 'page', 'submenu' => ''],
            'error_message'   => $this->session->flashdata('error_message'),
            'success_message' => $this->session->flashdata('success_message'),
            'need_editor'     => true,
            'has_trash'       => true,

        ];

    }

    /*
    List of post
    */
    public function index()
    {
        if (!$this->input->get('get_list')) {
            if (!$this->input->is_ajax_request()) {
                $this->template->_default();
                $this->output->set_title($this->data['module_title']);
            }

            $this->data['table'] = [
                'columns' => [
                    'id' => ['name'  => 'id', 'title' => 'ID' , 'width' => '50px', 'filter' => ['type' => 'text']],
                    'claim_no' => ['name'  => 'claim_no', 'title' => 'Claim No' , 'width' => 'auto', 'filter' => ['type' => 'text']],
                    'kd_item' => ['name'  => 'kd_item', 'title' => 'Kode Item' , 'width' => '75px', 'filter' => ['type' => 'text']],
                    'createddate' => ['name'  => 'createddate', 'title' => '', 'width' => '150px', 'filter' => ['type' => 'none']]
                ],
                'disable_sorting' => '0,2,3',
                'default_sort_col' => '1',
                'default_sort_order' => 'asc'
            ];

            $this->load->view('list', $this->data);
        } else {
            $query = $this->db->query("SELECT id, claim_no, kd_item, createddate FROM (
    select row_number() OVER (order by claim_no desc) AS ROWINDEX, *  
    from tbl_claim_detail) TB
WHERE TB.ROWINDEX between 400000 and 400100");
            //echo '<pre>';
            print_r($query->result());exit;
            $this->load->library('datatable');


            $this->datatable->select("id, claim_no, kd_item, createddate");
            $this->datatable->from("(
    select row_number() OVER (order by claim_no desc) AS ROWINDEX, id, claim_no, kd_item, createddate 
    from tbl_claim_detail) TB");
            $this->db->where("TB.ROWINDEX >= ", 400000);
            $this->db->where("TB.ROWINDEX <= ", 400100);

            $array = json_decode($this->datatable->generate(), TRUE);
            $number = $this->input->get('start') ? $this->input->get('start') : 0;
            // foreach($array['data'] as $key => $value) {
            //     $number++;
            //     $title = 
            //         '<div class="">
            //             <div>'.$value['title'].'</div>
            //             <div>
            //                 <a href="'.$this->data['module_url'].'/update/'.encode($value['id']).'" class="green ajaxan" title="Edit Halaman">edit</a> | 
            //                 <a href="'.$this->data['module_url'].'/set_active/'.encode($value['id']).'" class="orange btn-status" title="Ubah Status">Ubah Status</a>
            //             </div>
            //         </div>';
            //     switch($value['is_active']) {
            //         case 1: $is_active = '<span class="label label-success">Ya</span>'; break;
            //         default: $is_active = '<span class="label label-danger">Tidak</span>'; break;
            //     }
            //     $created_at = '<div>Dibuat pada:<br>'.date('d/m/Y H:i:s', strtotime($value['created_at'])).'</div>';

            //     $array['data'][$key]['id']          = $number;
            //     $array['data'][$key]['title']       = $title;
            //     $array['data'][$key]['is_active']   = $is_active;
            //     $array['data'][$key]['created_at']  = $created_at;
            // }
            echo json_encode($array);
        }
    }

    /*
    Create post
    */
    public function create()
    {
        $form_title = 'Tambah Halaman';
        if (!$this->input->is_ajax_request()) {
            $this->template->_default();
            $this->output->set_title($form_title);
        }

        $this->data['form'] = [
            'title'  => $form_title,
        ];

        $this->load->view('form', $this->data);
    }

    /*
    Update post
    */
    public function update($id)
    {
        $form_title = 'Edit Halaman';
        if (!$this->input->is_ajax_request()) {
            $this->template->_default();
            $this->output->set_title($form_title);
        }

        $data = $this->get_data($id);
        $this->data['form'] = [
            'title'  => $form_title,
            'data'   => $data,
        ];

        $this->load->view('form', $this->data);
    }

    /*
    Save post
    */
    public function save()
    {
        //only ajax request are allowed
        $this->input->is_ajax_request() or exit('No direct script access allowed!');

        $this->form_validation->set_rules('title', 'Judul Halaman', 'trim|required');

        $this->load->library('upload');
        $this->load->library('user_agent');
        $method = substr($this->agent->referrer(), -6, 6);

        //validate the form
        if ($this->form_validation->run() === true) {
            $data = $this->input->post();
            $data['type']       = 'page';
            $data['title']      = $data['title'];
            $data['slug']       = url_title(strtolower($data['title']), '-');
            $data['is_active']  = isset($data['is_active']) ? 1 : 0;

            // validasi title
            $check_title = $this->post_model->get(['id !=' => $data['id'], 'title' => $data['title']]);
            if($check_title) {
                echo errorMessage('Judul halaman sudah ada di database.');
                return false;
            }

            if ($method == 'create') {
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['created_by'] = $this->session->userdata('login_id');;
                $save = $this->post_model->insert($data);
                if ($save !== false) {
                    echo successMessage('Data telah berhasil disimpan.');
                    echo clearForm();
                } else {
                    echo errorMessage('Terjadi kesalahan ketika menyimpan data.');
                }
            } else {
                $data['updated_by'] = $this->session->userdata('login_id');;
                $save = $this->post_model->update($data, $data['id']);
                if ($save !== false) {
                    echo successMessage('Data telah berhasil diubah');
                } else {
                    echo errorMessage('Terjadi kesalahan ketika mengubah data.');
                }
            }

        } else {
            echo errorMessage(validation_errors());
        }
    }

    public function set_active($id=0) {
        //only ajax request are allowed
        $this->input->is_ajax_request() or exit('No direct script access allowed!');

        $id = decode($id);
        $data = $this->post_model->get($id);
        if($data) {
            $is_active = ($data->is_active == 1) ? 0 : 1; 
            $save = $this->post_model->update(['is_active' => $is_active], $id);
            if($save) {
                echo 1;
            } else {
                echo 0;
            }            
        }
    }


    /*
    Get data by ID
    */
    public function get_data($id)
    {
        $id = decode($id);
        $data = $this->post_model->get(['id'=>$id, 'type'=>'page']);
        if ($data) {
            return $data;
        } else {
            $this->session->set_flashdata('error_message', errorMessage(lang('not_found')));
            redirect($this->data['module_url']);
        }
    }

}
