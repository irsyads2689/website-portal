<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="<?php echo site_url(); ?>">Dashboard</a>
                </li>
                <li class="active"><?php echo $module_title; ?></li>
            </ul>
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    <?php echo $module_title; ?>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <?php
                    echo (!empty($success_message)) ? $success_message : '';
                    echo (!empty($error_message)) ? $error_message : '';
                    ?>
                    <div class="widget-box widget-color-blue">
                        <div class="widget-header widget-header-flat widget-header-large">
                            <h4 class="widget-title">
                                <a href="<?php echo $module_url; ?>/create" class="btn btn-white btn-success ajaxan" title="<?php echo sprintf(lang('form_add'), $module_title); ?>">
                                    <i class="ace-icon fa fa-plus bigger-120 green"></i> Tambah
                                </a>
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div>
                                            <table id="data-grid" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <?php
                                                        if ($table['columns']) {
                                                            foreach ($table['columns'] as $key => $column) {
                                                                echo '<th style="width:'.$column['width'].'">'.$column['title'].'</th>';
                                                            }
                                                        }
                                                        ?>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- PAGE CONTENT ENDS -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->

<?php
$columns = '';
if ($table['columns']) {
    foreach ($table['columns'] as $key => $column) {
        $columns .= '{"data":"'.$key.'", "name":"'.$column['name'].'"},';
    }
}
?>
<script type="text/javascript">
var current_url = '<?php echo current_url(); ?>';
var module_url = '<?php echo $module_url; ?>';

var table = $('#data-grid').DataTable({
    "ajax": current_url + "?get_list=true",
    "columns": [<?php echo $columns; ?>],
    "columnDefs": [ { orderable: false, targets: [<?php echo $table['disable_sorting']; ?>] } ],
    "order": [[ <?php echo $table['default_sort_col'].', "'.$table['default_sort_order'].'"'; ?> ]]
});

$(document).ready(function() {
    $('#data-grid').on('click', '.btn-status',  function(e) {
        e.preventDefault();

        var url = $(this).attr('href');
        $('#loader').show();
        $.get(url, function(result) {
            $('#loader').hide();
            if(result)
                table.ajax.reload(null, false);
        })
    });    
})

</script>