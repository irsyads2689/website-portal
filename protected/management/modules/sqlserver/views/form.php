<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Dashboard</a>
                </li>
                <li class="active"><?php echo $module_title; ?></li>
            </ul>
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    <?php echo $module_title; ?>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">

                    <div class="widget-box widget-color-blue">
                        <div class="widget-header widget-header-flat widget-header-large">
                            <h4 class="widget-title">
                                <?php echo $form['title']; ?>
                            </h4>

                            <div class="widget-toolbar no-border">
                                <div class="inline dropdown-hover">
                                    <a href="<?php echo $module_url; ?>" class="btn btn-white btn-danger ajaxan" title="<?php echo $module_title; ?>">
                                        <i class="ace-icon fa fa-undo bigger-120 red"></i> Kembali
                                    </a>
                                    <button type="button" class="btn btn-white btn-info" onclick="$('#form').submit();">
                                        <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i> Simpan
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main no-padding">
                                <form id="form" class="form-horizontal" method="post" action="<?php echo $module_url.'/save'; ?>" enctype="multipart/form-data">
                                    <div id="infoMessage"></div>
                                    <input type="hidden" name="id" value="<?php echo (isset($form['data'])) ? $form['data']->id : ''; ?>">

                                    <fieldset>
                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Judul Halaman</label>
                                            <div class="col-md-10 col-sm-10 col-xs-12">
                                                <input type="text" name="title" class="form-control" placeholder="Judul Halaman"  value="<?php echo (isset($form['data'])) ? $form['data']->title : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Konten</label>
                                            <div class="col-md-10 col-sm-10 col-xs-12">
                                                <textarea name="content" class="form-control editor" rows="5"><?php echo (isset($form['data'])) ? $form['data']->content : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Meta Title</label>
                                            <div class="col-md-10 col-sm-10 col-xs-12">
                                                <input type="text" name="meta_title" class="form-control" placeholder="Meta Title"  value="<?php echo (isset($form['data'])) ? $form['data']->meta_title : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Meta Description</label>
                                            <div class="col-md-10 col-sm-10 col-xs-12">
                                                <textarea name="meta_description" class="form-control" placeholder="Meta Description" rows="5"><?php echo (isset($form['data'])) ? $form['data']->meta_description : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Meta Keyowrds</label>
                                            <div class="col-md-10 col-sm-10 col-xs-12">
                                                <input type="text" name="meta_keywords" class="form-control" placeholder="Meta Keyowrds"  value="<?php echo (isset($form['data'])) ? $form['data']->meta_keywords : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Aktif</label>
                                            <div class="col-md-10">
                                                <label>
                                                    <input id="is_active" name="is_active" class="ace ace-switch ace-switch-7" type="checkbox" value="1" 
                                                        <?php echo (!isset($form['data'])) ? 'checked' : ($form['data']->is_active==1 ? 'checked' : ''); ?>>
                                                    <span class="lbl"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <div class="form-actions" style="padding-left: 20px">
                                        <a href="<?php echo $module_url; ?>" class="btn btn-white btn-danger  ajaxan" title="<?php echo $module_title; ?>">
                                            <i class="ace-icon fa fa-undo bigger-120 red"></i> Kembali
                                        </a>
                                        <button type="submit" class="btn btn-white btn-info ">
                                            <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i> Simpan
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- PAGE CONTENT ENDS -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->

<div id="dialog-delete">
    
</div>

<script src="<?php echo base_url(); ?>assets/js/form.js" type="text/javascript"></script>

<script type="text/javascript">
    var module_url = "<?php echo $module_url; ?>";
    jQuery(function($) {
        // image primary
        $('#is_active').click(function() {
            var checked = $(this).attr('checked');
            if(checked) {
                $(this).removeAttr('checked');
            } else {
                $(this).attr('checked', true);
            }
        })
    })

</script>
