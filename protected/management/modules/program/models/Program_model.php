<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Program_model extends X_Model {

    public $table = 'programs';
    public $primary_key = 'id';

}
