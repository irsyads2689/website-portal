<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Dashboard</a>
                </li>
                <li class="active"><?php echo $module_title; ?> (<?php echo $program->title; ?>)</li>
            </ul>
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    <?php echo $module_title; ?> (<?php echo $program->title; ?>)
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">

                    <div class="widget-box widget-color-blue">
                        <div class="widget-header widget-header-flat widget-header-large">
                            <h4 class="widget-title">
                                <?php echo $form['title']; ?>
                            </h4>

                            <div class="widget-toolbar no-border">
                                <div class="inline dropdown-hover">
                                    <a href="<?php echo $module_url; ?>" class="btn btn-white btn-danger ajaxan" title="<?php echo $module_title; ?>">
                                        <i class="ace-icon fa fa-undo bigger-120 red"></i> Kembali
                                    </a>
                                    <button type="button" class="btn btn-white btn-info" onclick="$('#form').submit();">
                                        <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i> Simpan
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main no-padding">
                                <form id="form" class="form-horizontal" method="post" action="<?php echo $module_url.'/content/save'; ?>" enctype="multipart/form-data">
                                    <div id="infoMessage"></div>
                                    <input type="hidden" name="id" value="<?php echo (isset($form['data'])) ? $form['data']->id : ''; ?>">
                                    <input type="hidden" name="parent" value="<?php echo (isset($program)) ? $program->id : ''; ?>">

                                    <fieldset>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th style="width: 150px">Gambar</th>
                                                    <th>Konten</th>
                                                    <th style="width: 100px"></th>
                                                </tr>
                                            </thead>
                                            <tbody id="content-program">
                                                <tr class="clone hidden">
                                                    <td style="width: 150px">
                                                        <input multiple class="upload_image" type="file" name="" />
                                                    </td>
                                                    <td>
                                                        <div class="form-group">
                                                            <label class="col-md-2">Judul</label>
                                                            <div class="col-md-10">
                                                                <input type="text" name="" class="form-control content-title">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-2">Deskripsi</label>
                                                            <div class="col-md-10">
                                                                <textarea rows="5" name="" class="form-control content-description"></textarea>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <a href="javascript:;" class="btn btn-xs btn-white btn-warning remove-image">
                                                            <i class="ace-icon fa fa-minus bigger-110 icon-only"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <?php
                                                if(isset($content_program) AND $content_program) {
                                                    foreach($content_program as $key => $row) {
                                                        echo '
                                                        <tr>
                                                            <input type="hidden" name="content['.$key.'][id]" value="'.$row->id.'">
                                                            <input type="hidden" name="content['.$key.'][image_ori]" value="'.get_image($row->image_folder, $row->image).'" data-title="'.$row->image.'">
                                                            <td style="width: 150px">
                                                                <input multiple class="upload_image" type="file" id="upload_image_'.$key.'" name="upload_images['.$key.']" />
                                                            </td>
                                                            <td>
                                                                <div class="form-group">
                                                                    <label class="col-md-2">Judul</label>
                                                                    <div class="col-md-10">
                                                                        <input type="text" name="content['.$key.'][title]" value="'.$row->title.'" class="form-control content-title">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-2">Deskripsi</label>
                                                                    <div class="col-md-10">
                                                                        <textarea rows="5" name="content['.$key.'][description]" class="form-control content-description">'.$row->description.'</textarea>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td style="text-align: center;">
                                                                <a href="javascript:;" class="btn btn-xs btn-white btn-danger remove-image" data-id="'.$row->id.'">
                                                                    <i class="ace-icon fa fa-times bigger-110 icon-only"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        ';
                                                    }
                                                }
                                                ?>
                                                <tr>
                                                    <td colspan="2"></td>
                                                    <td style="text-align: center;">
                                                        <a href="javascript:;" id="add-image" class="btn btn-xs btn-white btn-info" data-key="<?php echo (isset($form['product_images']) AND $form['product_images']) ? COUNT($form['product_images']) : 0; ?>">
                                                            <i class="ace-icon fa fa-plus bigger-110 icon-only"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </fieldset>

                                    <div class="form-actions" style="padding-left: 20px">
                                        <a href="<?php echo $module_url; ?>" class="btn btn-white btn-danger  ajaxan" title="<?php echo $module_title; ?>">
                                            <i class="ace-icon fa fa-undo bigger-120 red"></i> Kembali
                                        </a>
                                        <button type="submit" class="btn btn-white btn-info ">
                                            <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i> Simpan
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- PAGE CONTENT ENDS -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->

<div id="dialog-delete">
    
</div>

<script src="<?php echo base_url(); ?>assets/js/form.js" type="text/javascript"></script>

<script type="text/javascript">
    var module_url = "<?php echo $module_url; ?>";
    jQuery(function($) {
        // list image
        $('.content-title').each(function(k, v) {
            var input_file = $(this).parents('tr').find('input[type=file]');
            var id_input_file = input_file.attr('id');
            file_input($('#'+id_input_file));
            var url_image = $(this).val();
            var title_image = $(this).data('title') || '';
            if(title_image != '') {
                input_file.next().addClass('hide-placeholder selected').removeAttr('data-title');
                input_file.next().find('.ace-file-name').addClass('large').attr('data-title', title_image).prepend('<img id="image_primary" class="middle" style="background-image: url(\''+url_image+'\'); background-size: 100%;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQImWNgYGBgAAAABQABh6FO1AAAAABJRU5ErkJggg==">');
            }
        })

        /* ========= IMAGE =========== */
        // add image
        $('#add-image').on('click', function(e) {
            e.preventDefault();

            var ii = $('input.upload_image'),
            i = ii.eq(ii.length - 1).next().find('img.middle').attr('class');
            if(ii.length > 1 && i==undefined) {
                return false;
            }

            var key     = $(this).attr('data-key');
            var clone   = $('#content-program').find('.clone').clone();

            var id = 'upload_image'+key;
            var name = 'upload_images['+key+']';
            clone.find('input.upload_image').attr('name', name);
            clone.find('input.upload_image').attr('id', id);
            clone.find('input.content-title').attr('name', 'content['+key+'][title]');
            clone.find('input.content-description').attr('name', 'content['+key+'][description]');

            key++;
            $(this).attr('data-key', key);
            clone.removeClass('clone hidden');
            $(this).parent().parent().before(clone);
            file_input($('#'+id));
        })

        // delete image
        $('#content-program').on('click', 'a.remove-image', function(e) {
            e.preventDefault();

            var this_ = $(this),
            id = this_.data('id');
            if(id == undefined) {
                $(this).parent().parent().remove();
            } else {
                dialog_delete
                .html('<div class="alert alert-info bigger-110">Data akan dihapus permanen.</div><p class="bigger-110 bolder center grey">Apa kamu yakin?</p>')
                .dialog('open')
                .dialog('option', 'buttons', [
                    {
                        html: "Tidak",
                        "class": "btn btn-minier",
                        click: function() {
                            $( this ).dialog( "close" );
                        }
                    },
                    {
                        html: "Hapus",
                        "class" : "btn btn-danger btn-minier",
                        click: function() {
                            $.get(module_url+"/delete_image_more/"+id, function(result) {
                                this_.parent().parent().remove();
                                dialog_delete
                                .html(result)
                                .dialog('option', 'buttons', [
                                    { 
                                        html: 'Ok', 
                                        "class": "btn btn-minier btn-info", 
                                        click: function() {
                                            $( this ).dialog( "close" );
                                        }
                                    }
                                ])
                            })
                        }
                    }
                ]);
            }
        })

    })

    function file_input(elm) {
        elm.ace_file_input({
            style:'well',
            btn_choose: 'Drop images here or click to choose',
            no_icon: 'ace-icon fa fa-picture-o',
            allowExt: ["jpeg", "jpg", "png"],
            allowMime: ["image/jpg", "image/jpeg", "image/png"],
            btn_change:null,
            droppable:true,
            thumbnail:'large',
            icon_remove:null,
            before_change:function(files, dropped) {
                return true;
            },
            before_remove : function() {
                return true;
            },
            preview_error : function(filename, error_code) {
                //alert(error_code);
            }

        }).on('change', function(){
        });
    }

</script>
