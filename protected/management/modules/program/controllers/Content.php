<?php defined('BASEPATH') or exit('No direct script access allowed!');

class Content extends App_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('administration');
        $this->administration->logged();
        $this->load->model('program_model');

        $this->data = [
            'module_title'    => 'Program',
            'module_icon'     => 'icon-magazine',
            'module_url'      => site_url('program'),
            'menu'            => ['menu' => 'program', 'submenu' => 'program'],
            'error_message'   => $this->session->flashdata('error_message'),
            'success_message' => $this->session->flashdata('success_message'),
            'need_editor'     => true,
            'has_trash'       => true,

        ];

    }

    public function index()
    {
        echo ajaxRedirect('program', 0);
    }

    public function create($id = '')
    {
        $data = $this->get_data($id);

        $form_title = 'Tambah Konten Program';
        if (!$this->input->is_ajax_request()) {
            $this->template->_default();
            $this->output->set_title($form_title);
        }

        $this->data['form'] = [
            'title'  => $form_title,
            'method'  => 'create'
        ];

        $this->data['program'] = $data;

        $this->load->view('program/content/form', $this->data);
    }

    public function update($id)
    {
        $form_title = 'Edit Konten Program';
        if (!$this->input->is_ajax_request()) {
            $this->template->_default();
            $this->output->set_title($form_title);
        }

        $data = $this->get_data($id);
        $this->data['form'] = [
            'title'  => $form_title,
            'method'  => 'update'
        ];

        $this->data['program'] = $data;
        $this->data['content_program'] = $this->program_model->get_all(['parent'=>$data->id]);

        $this->load->view('program/content/form', $this->data);
    }

    public function save()
    {
        //only ajax request are allowed
        $this->input->is_ajax_request() or exit('No direct script access allowed!');

        $this->form_validation->set_rules('parent', 'Program', 'trim|required');

        $this->load->library('upload');
        // $this->load->library('user_agent');

        //validate the form
        if ($this->form_validation->run() === true) {
            $data = $this->input->post();

            $method     = $data['method'];
            $contents   = isset($data['content']) ? $data['content'] : '';
            unset($data['method']);
            unset($data['content']);

            $created_at = date('Y-m-d H:i:s');
            $created_by = $this->session->userdata('login_id');
            if ($method == 'create') {
                if($contents) {
                    foreach($contents as $key => $value) {
                        if(isset($_FILES['upload_images']['name'][$key])) {
                            // set new file upload
                            $this->set_FILE('upload_image', 'upload_images', $key);

                            // upload image more
                            $image = $this->upload_image('upload_image');

                            if(isset($image['file_name'])) {
                                $contents[$key]['image']        = $image['file_name'];
                                $contents[$key]['image_folder'] = $image['path'];
                                $contents[$key]['parent']       = $data['parent'];
                                $contents[$key]['created_at']   = $created_at;
                                $contents[$key]['created_by']   = $created_by;
                            } else {
                                // if not upload image or upload image failed, then remove row data
                                unset($contents[$key]);
                            }
                        } else {
                            // if not upload image, then remove row data
                            unset($contents[$key]);
                        }
                    }

                    if(!$contents) {
                        echo errorMessage('Pengisian konten program belum lengkap.');
                        exit;
                    }
                    // save content program
                    $save = $this->db->insert_batch('programs', $contents);
                    if ($save !== false) {
                        // echo ajaxRedirect('program', 500);
                        echo successMessage('Data telah berhasil disimpan.');
                        echo loadPage(site_url('program'), 'Program');
                    } else {
                        echo errorMessage('Terjadi kesalahan ketika menyimpan data.');
                    }
                } else {
                    echo errorMessage('Pastikan konten program terisi.');
                }
            } else {
                // update content program
                if($contents) {
                    $insert = false;
                    foreach($contents as $key => $value) {
                        // if image more change / no change / delete 
                        if(isset($value['id'])) {
                            // if image more changed
                            if(isset($_FILES['upload_images']['name'][$key])) {
                                 // set new file upload
                                $this->set_FILE('upload_image', 'upload_images', $key);

                                // upload image more 
                                $image = $this->upload_image('upload_image', $value['image_folder']);
                                if(isset($image['file_name'])) {
                                    // image ori deleted
                                    if(isset($value['image_ori'])) {
                                        image_remove($contents[$key]['image_ori']);
                                        
                                        $contents[$key]['image'] = $image['file_name'];
                                        $contents[$key]['updated_by'] = $this->session->userdata('login_id');
                                    }
                                }
                            }

                            unset($contents[$key]['image_ori']);
                            $save = $this->program_model->update($contents[$key], $value['id']);
                            
                            unset($contents[$key]);
                        } else {
                            if(isset($_FILES['upload_images']['name'][$key])) {
                                // set new file upload
                                $this->set_FILE('upload_image', 'upload_images', $key);

                                // upload new image more
                                $image = $this->upload_image('upload_image');
                                if(isset($image['file_name'])) {
                                    $contents[$key]['image']        = $image['file_name'];
                                    $contents[$key]['image_folder'] = $image['path'];
                                    $contents[$key]['parent']       = $data['parent'];
                                    $contents[$key]['created_at']   = $created_at;
                                    $contents[$key]['created_by']   = $created_by;
                                    $insert = true;
                                } else {
                                    // if not upload image or upload image failed, then remove row data
                                    unset($contents[$key]);
                                }
                            } else {
                                unset($contents[$key]);
                            }
                        }
                    }

                    if($insert) {
                        $save = $this->db->insert_batch('programs', $contents);
                    }

                    if ($save !== false) {
                        // echo ajaxRedirect('program', 1500);
                        echo successMessage('Data telah berhasil diubah');
                        echo loadPage(site_url('program'), 'Program', 700);
                    } else {
                        echo errorMessage('Terjadi kesalahan ketika mengubah data.');
                    }
                } else {
                    echo errorMessage('Pastikan konten program terisi.');
                }
            }

        } else {
            echo errorMessage(validation_errors());
        }
    }

    public function delete($id=0) {
        //only ajax request are allowed
        $this->input->is_ajax_request() or exit('No direct script access allowed!');

        $id = decode($id);
        $data = $this->program_model->get($id);
        if($data) {
            $delete = $this->program_model->delete($id);
            if($delete) {
                $image = get_image($data->image_folder, $data->image);
                image_remove($image);
                echo 1;           
            } else {
                echo 0;
            }            
        } else {
            echo 0;
        }
    }

    public function get_data($id)
    {
        $id = decode($id);
        $data = $this->program_model->get($id);
        if ($data) {
            return $data;
        } else {
            $this->session->set_flashdata('error_message', errorMessage('Data tidak ditemukan.'));
            redirect($this->data['module_url']);
        }
    }

    private function upload_image($filename, $path = '') {
        $image = 'image null';
        if(!empty($_FILES[$filename]['name'])) {
            $path = upload_path($path);
            $config_upload['upload_path']   = $path;
            $config_upload['allowed_types'] = 'jpg|jpeg|png';
            $config_upload['file_name']     = 'IMG_'.time();
            $this->upload->initialize($config_upload);
            if (!$this->upload->do_upload($filename)) {
                $image = 'upload failed';
            } else {
                $upload_data = $this->upload->data();
                $path = str_replace('../uploads/','',$path);
                $image = array('file_name'=>$upload_data['file_name'], 'path'=>$path);

                image_resize($upload_data['full_path'], 1200);
            }
        }
        return $image;
    }

    private function set_FILE($filename, $filename_upload, $key) {
        $_FILES[$filename]['name']       = $_FILES[$filename_upload]['name'][$key];
        $_FILES[$filename]['type']       = $_FILES[$filename_upload]['type'][$key];
        $_FILES[$filename]['tmp_name']   = $_FILES[$filename_upload]['tmp_name'][$key];
        $_FILES[$filename]['error']      = $_FILES[$filename_upload]['error'][$key];
        $_FILES[$filename]['size']       = $_FILES[$filename_upload]['size'][$key];
    }

}
