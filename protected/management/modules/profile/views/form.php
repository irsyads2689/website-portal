<div class="main-content">

    <div class="main-content-inner">

        <div class="breadcrumbs" id="breadcrumbs">

            <ul class="breadcrumb">

                <li>

                    <i class="ace-icon fa fa-home home-icon"></i>

                    <a href="<?php echo site_url(); ?>">Dashboard</a>

                </li>

                <li><a href="<?php echo $module_url; ?>" class="ajaxan" title="<?php echo $module_title; ?>"><?php echo $module_title; ?></a></li>

                <li class="active"><?php echo $form['title']; ?></li>

            </ul>

        </div>



        <div class="page-content">

            <div class="page-header">

                <h1>

                    <?php echo $form['title']; ?>

                    <a href="<?php echo $module_url; ?>" class="btn btn-xs btn-danger ajaxan" title="<?php echo $module_title; ?>">

                        <i class="ace-icon fa fa-undo"></i> Kembali

                    </a>

                </h1>

            </div>



            <div class="row">

                <div class="col-xs-12">

                    <form id="form" class="form-horizontal" method="post" action="<?php echo $module_url.'/save'; ?>" enctype="multipart/form-data">

                        <div id="infoMessage"></div>

                        <input type="hidden" name="id" value="<?php echo (isset($form['data'])) ? $form['data']->id : ''; ?>">



                        <fieldset>

                            <div class="row">

                                <div class="col-md-9">

                                    <div class="form-group">

                                        <label class="col-md-12 col-sm-12 col-xs-12">Judul</label>

                                        <div class="col-md-12 col-sm-12 col-xs-12">

                                            <input type="text" name="title" class="form-control" placeholder="<?php echo lang('title'); ?>"  value="<?php echo (isset($form['data'])) ? $form['data']->title : ''; ?>">

                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="col-md-12 col-sm-12 col-xs-12">Konten</label>

                                        <div class="col-md-12 col-sm-12 col-xs-12">

                                            <textarea name="content" class="form-control editor" rows="5"><?php echo (isset($form['data'])) ? $form['data']->content : ''; ?></textarea>

                                        </div>

                                    </div>

                                    <?php /*
                                    <div class="widget-box">

                                        <div class="widget-header widget-header-flat">

                                            <h4 class="widget-title grey smaller">SEO</h4>

                                        </div>



                                        <div class="widget-body">

                                            <div class="widget-main">

                                                <div class="form-group">

                                                    <label class="col-md-12 col-sm-12 col-xs-12">Meta Title</label>

                                                    <div class="col-md-12 col-sm-12 col-xs-12">

                                                        <input type="text" name="meta_title" class="form-control" placeholder="Meta Title"  value="<?php echo (isset($form['data'])) ? $form['data']->meta_title : ''; ?>">

                                                    </div>

                                                </div>

                                                <div class="form-group">

                                                    <label class="col-md-12 col-sm-12 col-xs-12">Meta Description</label>

                                                    <div class="col-md-12 col-sm-12 col-xs-12">

                                                        <textarea name="meta_description" class="form-control" placeholder="Meta Description"  rows="3"><?php echo (isset($form['data'])) ? $form['data']->meta_description : ''; ?></textarea>

                                                    </div>

                                                </div>

                                                <div class="form-group">

                                                    <label class="col-md-12 col-sm-12 col-xs-12">Meta Keyowrds</label>

                                                    <div class="col-md-12 col-sm-12 col-xs-12">

                                                        <input type="text" name="meta_keywords" class="form-control" placeholder="Meta Keyowrds"  value="<?php echo (isset($form['data'])) ? $form['data']->meta_keywords : ''; ?>">

                                                    </div>

                                                </div>



                                            </div>

                                        </div>

                                    </div>
                                    */ ?>
                                </div>

                                <div class="col-md-3">

                                    <div class="widget-box">

                                        <div class="widget-header widget-header-flat">

                                            <h4 class="widget-title grey smaller">Status</h4>

                                        </div>



                                        <div class="widget-body">

                                            <div class="widget-main">

                                                <div class="form-group">

                                                    <label class="col-md-12">Aktif</label>

                                                    <div class="col-md-12">

                                                        <label>

                                                            <input id="is_active" name="is_active" class="ace ace-switch ace-switch-7" type="checkbox" value="1" 

                                                                <?php echo (!isset($form['data'])) ? 'checked' : ($form['data']->is_active==1 ? 'checked' : ''); ?>>

                                                            <span class="lbl"></span>

                                                        </label>

                                                    </div>

                                                </div>

                                                <div class="form-group">

                                                    <label class="col-md-12">Urutan</label>

                                                    <div class="col-md-12">

                                                            <input name="sort_order" class="form-control" type="number" value="<?php echo (isset($form['data'])) ? $form['data']->sort_order : ''; ?>" >

                                                    </div>

                                                </div>

                                                <div class="form-group no-margin-bottom">

                                                    <div class="col-md-12">

                                                        <button type="submit" class="btn btn-sm btn-info btn-block">

                                                            <i class="ace-icon fa fa-floppy-o bigger-120"></i> Simpan

                                                        </button>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </fieldset>



                    </form>

                </div><!-- /.col -->

            </div><!-- /.row -->

        </div><!-- /.page-content -->

    </div>

</div><!-- /.main-content -->



<div id="dialog-delete">

    

</div>



<script src="<?php echo base_url(); ?>assets/js/form.js" type="text/javascript"></script>



<script type="text/javascript">

    var module_url = "<?php echo $module_url; ?>";

    jQuery(function($) {

        // image primary

        file_input($('#file-image'));

        var image_ori = $('input[name=image_ori]').val();

        if(image_ori) {

            var url_image = "<?php echo (isset($form['has_image']) AND !empty($form['has_image']['image'])) ? get_image('posts', $form['has_image']['image']) : ''; ?>";

            var title_image = "<?php echo (isset($form['has_image']) AND !empty($form['has_image']['image'])) ? $form['has_image']['image'] : ''; ?>";

            $('#file-image').next().addClass('hide-placeholder selected').removeAttr('data-title');

            $('#file-image').next().find('.ace-file-name').addClass('large').attr('data-title', title_image).prepend('<img id="image_primary" class="middle" style="background-image: url(\''+url_image+'\'); background-size: 100%;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQImWNgYGBgAAAABQABh6FO1AAAAABJRU5ErkJggg==">');

        }



        $('#is_active').click(function() {

            var checked = $(this).attr('checked');

            if(checked) {

                $(this).removeAttr('checked');

            } else {

                $(this).attr('checked', true);

            }

        })

    })



    function file_input(elm) {

        elm.ace_file_input({

            style:'well',

            btn_choose: 'Drop images here or click to choose',

            no_icon: 'ace-icon fa fa-picture-o',

            allowExt: ["jpeg", "jpg", "png"],

            allowMime: ["image/jpg", "image/jpeg", "image/png"],

            btn_change:null,

            droppable:true,

            thumbnail:'large',

            icon_remove:null,

            before_change:function(files, dropped) {

                return true;

            },

            before_remove : function() {

                return true;

            },

            preview_error : function(filename, error_code) {

                //alert(error_code);

            }



        }).on('change', function(){

        });

    }



</script>

