<div class="container-fluid container-fixed-lg m-b-20">
    <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>"><i class="pg-home"></i></a></li>
        <li class="breadcrumb-item"><a href="<?php echo $module_url; ?>" class="ajaxcontent" data-title="<?php echo $module_title; ?>"><?php echo $module_title; ?></a></li>
        <li class="breadcrumb-item active"><?php echo $form['title']; ?></li>
    </ul>
    <h3 class="page-title"><?php echo $form['title']; ?></h3>
</div>
<div class="container-fluid container-fixed-lg">
    <div class="row">
        <div class="col-md-9">
            <div class="form-group form-group-default">
                <label>Judul</label>
                <input type="text" class="form-control" name="title" required>
            </div>
            <div class="form-group form-group-default">
                <label>Konten</label>
                <textarea name="content" class="form-control" rows="5"></textarea>
            </div>
            <div class="card">
                <div class="card-block">
                    <p>SEO</p>
                    <div class="form-group-attached">
                        <div class="form-group form-group-default">
                            <label>Meta Title</label>
                            <input type="text" class="form-control" name="meta_title">
                        </div>
                        <div class="form-group form-group-default">
                            <label>Meta Description</label>
                            <textarea name="meta_description" class="form-control" rows="3"></textarea>
                        </div>
                        <div class="form-group form-group-default">
                            <label>Meta Keywords</label>
                            <input type="text" class="form-control" name="meta_keywords">
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-block">
                    <p>Status</p>
                    <div class="form-group-attached">
                        <div class="form-group form-group-default">
                            <label>Status</label>
                            <select  class="cs-select cs-skin-slide cs-transparent form-control" data-init-plugin="cs-select" name="is_active">
                                <option>Ya</option>
                                <option>Tidak</option>
                            </select>
                        </div>
                        <div class="form-group form-group-default">
                            <label>Meta Description</label>
                            <textarea name="meta_description" class="form-control" rows="3"></textarea>
                        </div>
                        <div class="form-group form-group-default">
                            <label>Meta Keywords</label>
                            <input type="text" class="form-control" name="meta_keywords">
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var module_url = "<?php echo $module_url; ?>";
</script>
