<div class="container-fluid container-fixed-lg m-b-20">
    <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>"><i class="pg-home"></i></a></li>
        <li class="breadcrumb-item active"><?php echo $module_title; ?></li>
    </ul>
    <div class="pull-right">
        <a href="<?php echo $module_url; ?>/create" class="btn btn-primary btn-sm ajaxcontent" data-title="<?php echo $module_title; ?>">
            <i class="fa fa-plus"></i> Tambah
        </a>
    </div>
    <h3 class="page-title"><?php echo $module_title; ?></h3>
</div>
<div class="container-fluid container-fixed-lg">
    <div id="alert-container">
        <?php
        echo (!empty($success_message)) ? $success_message : '';
        echo (!empty($error_message)) ? $error_message : '';
        ?>
    </div>
    <div class="card">
        <div class="card-block p-0">
            <div class="table-responsive">
                <table class="table table-condensed table-striped table-hover" id="data-grid">
                    <thead>
                        <tr>
                            <?php
                            if ($table['columns']) {
                                foreach ($table['columns'] as $key => $column) {
                                    echo '<th style="width:'.$column['width'].'">'.$column['title'].'</th>';
                                }
                            }
                            ?>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>


<?php
$columns = '';
if ($table['columns']) {
    foreach ($table['columns'] as $key => $column) {
        $columns .= '{"data":"'.$key.'", "name":"'.$column['name'].'"},';
    }
}
?>
<script type="text/javascript">
var current_url = '<?php echo current_url(); ?>';
var module = {};
module.url = current_url;
$(document).ready(function () {
    module.table = $('#data-grid').on( 'processing.dt', function ( e, settings, processing ) {
        if (processing === true) {
            $('.panel').addClass('is-loading');
        } else {
            $('.panel').removeClass('is-loading');
        }
    }).DataTable({
        "ajax": current_url + "?get_list=true",
        "columns": [<?php echo $columns; ?>],
        "columnDefs": [ { orderable: false, targets: [<?php echo $table['disable_sorting']; ?>] } ],
        "order": [[ <?php echo $table['default_sort_col'].', "'.$table['default_sort_order'].'"'; ?> ]]
    });
    module.table.columns().every( function() {
        var that = this;
        var search = $.fn.dataTable.util.throttle(
            function (val) {
                that.search(val).draw();
            }, 1000
        );
        $( 'input', this.footer() ).on( 'keyup', function (e) {
            if(e.keyCode == 13) {
                search(this.value);
            }
        });
        $( 'input', this.footer() ).on( 'change', function () {
            that.search( this.value ).draw();
        });
        $( 'select', this.footer() ).on( 'change', function () {
            that.search( this.value ).draw();
        });
    });

    $('dataTables_length select').select2();
    $('.select2').select2({minimumResultsForSearch:-1});
// var table = $('#data-grid').dataTable({
//     "ajax": current_url + "?get_list=true",
//     "columns": [<?php echo $columns; ?>],
//     "columnDefs": [ { orderable: false, targets: [<?php echo $table['disable_sorting']; ?>] } ],
//     "order": [[ <?php echo $table['default_sort_col'].', "'.$table['default_sort_order'].'"'; ?> ]]
// });

// $(document).ready(function() {
    $('#data-grid').on('click', '.btn-status',  function(e) {
        e.preventDefault();

        var url = $(this).attr('href');
        $('#loader').show();
        $.get(url, function(result) {
            $('#loader').hide();
            if(result)
                table.ajax.reload(null, false);
        })
    });    
})

</script>