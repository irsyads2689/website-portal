<?php defined('BASEPATH') or exit('No direct script access allowed!');



class News extends App_Controller

{



    public function __construct()

    {

        parent::__construct();



        $this->load->library('administration');

        $this->administration->logged();

        $this->load->model('post_model');



        $this->data = [

            'module_title'    => 'Berita',

            'module_icon'     => 'icon-magazine',

            'module_url'      => site_url('post/news'),

            'menu'            => ['menu' => 'post', 'submenu' => 'news'],

            'error_message'   => $this->session->flashdata('error_message'),

            'success_message' => $this->session->flashdata('success_message'),

            'need_editor'     => true,

            'has_trash'       => true,



        ];



    }



    public function index()

    {

        if (!$this->input->get('get_list')) {

            if (!$this->input->is_ajax_request()) {

                $this->template->_default();

                $this->output->set_title($this->data['module_title']);

            }



            $this->data['table'] = [

                'columns' => [

                    'id' => ['name'  => 'posts.id', 'title' => 'No.' , 'width' => '20px', 'filter' => ['type' => 'text']],

                    // 'image' => ['name'  => 'posts.image', 'title' => 'Gambar' , 'width' => '100px', 'filter' => ['type' => 'text']],

                    'title' => ['name'  => 'posts.title', 'title' => 'Judul' , 'width' => 'auto', 'filter' => ['type' => 'text']],

                    'is_active' => ['name'  => 'posts.is_active', 'title' => 'Aktif' , 'width' => '75px', 'filter' => ['type' => 'text']],

                    'created_at' => ['name'  => 'posts.created_at', 'title' => '', 'width' => '150px', 'filter' => ['type' => 'none']]

                ],

                'disable_sorting' => '0,3',

                'default_sort_col' => '3',

                'default_sort_order' => 'desc'

            ];



            $this->load->view('post/news/list', $this->data);

        } else {

            $this->load->library('datatable');



            $this->datatable->select("posts.id, posts.title, posts.image, posts.image_folder, posts.is_active, posts.created_at");

            $this->datatable->from("posts");

            $this->datatable->where("posts.type", 'berita');



            $array = json_decode($this->datatable->generate(), TRUE);

            $number = $this->input->get('start') ? $this->input->get('start') : 0;

            foreach($array['data'] as $key => $value) {

                $number++;

                // $image = '<div><img src="'.get_image($value['image_folder'], $value['image']).'" class="img img-responsive"></div>';

                if($value['is_active']==1) {

                    $status = '

                    <a href="'.$this->data['module_url'].'/set_active/'.encode($value['id']).'" class="orange btn-status" title="Non Aktif">

                        <i class="ace-icon fa fa-eye-slash"></i> Non Aktif

                    </a>';

                } else {

                    $status = '

                    <a href="'.$this->data['module_url'].'/set_active/'.encode($value['id']).'" class="orange btn-status" title="Aktif">

                        <i class="ace-icon fa fa-eye"></i> Aktif

                    </a>';                    

                }

                $title = 

                    '<div class="">

                        <div>'.$value['title'].'</div>

                        <small>

                            <a href="'.$this->data['module_url'].'/update/'.encode($value['id']).'" class="green ajaxan" title="Edit Profil">

                                <i class="ace-icon fa fa-pencil"></i> Edit

                            </a> | 

                            '.$status.'

                        </small>

                    </div>';

                switch($value['is_active']) {

                    case 1: $is_active = '<span class="label label-success">Ya</span>'; break;

                    default: $is_active = '<span class="label label-danger">Tidak</span>'; break;

                }

                $created_at = '<div>Dibuat pada:<br>'.date('d/m/Y H:i:s', strtotime($value['created_at'])).'</div>';



                $array['data'][$key]['id']          = $number;

                // $array['data'][$key]['image']       = $image;

                $array['data'][$key]['title']       = $title;

                $array['data'][$key]['is_active']   = $is_active;

                $array['data'][$key]['created_at']  = $created_at;

            }

            echo json_encode($array);

        }

    }



    public function create()

    {

        $form_title = 'Tambah Berita';

        if (!$this->input->is_ajax_request()) {

            $this->template->_default();

            $this->output->set_title($form_title);

        }



        $this->data['form'] = [

            'title'  => $form_title

        ];



        $this->load->view('post/news/form', $this->data);

    }



    public function update($id)

    {

        $form_title = 'Edit Berita';

        if (!$this->input->is_ajax_request()) {

            $this->template->_default();

            $this->output->set_title($form_title);

        }



        $data = $this->get_data($id);

        $this->data['form'] = [

            'title'  => $form_title,

            'data'   => $data,

            'has_image' => [

                'image' => $data->image,

                'image_folder' => $data->image_folder

            ]

        ];



        $this->load->view('post/news/form', $this->data);

    }



    public function save()

    {

        //only ajax request are allowed

        $this->input->is_ajax_request() or exit('No direct script access allowed!');



        $this->form_validation->set_rules('title', 'Judul Berita', 'trim|required');



        $this->load->library('upload');

        $this->load->library('user_agent');

        $method = substr($this->agent->referrer(), -6, 6);



        //validate the form

        if ($this->form_validation->run() === true) {

            $data = $this->input->post();

            $data['type']       = 'berita';

            $data['title']      = $data['title'];

            $data['meta_title'] = $data['meta_title'] ? $data['meta_title'] : $data['title'];

            $data['slug']       = url_title(strtolower($data['title']), '-');

            $data['is_active']  = isset($data['is_active']) ? 1 : 0;



            // validasi title

            $check_title = $this->post_model->get(['id !=' => $data['id'], 'title' => $data['title']]);

            if($check_title) {

                echo errorMessage('Judul sudah ada di database.');

                return false;

            }



            // upload image primary

            $image = '';

            if(isset($_FILES['upload_image']['name'])) {

                $image = $this->upload_image('upload_image', $data['image_folder']);

            }

            // if image null

            if($image == 'image null') {

                echo errorMessage('Gambar masih belum ada.');

                return false;

            }

            // if upload image failed

            if($image == 'upload failed') {

                echo errorMessage('Upload gambar gagal, pastikan ukuran gambar tidak melebihi 2MB dan berekstensi jpg, jpeg, png.');

                return false;

            }



            // if upload image / change image

            if($image != '' AND ($image != 'image null' OR $image != 'upload failed')) {

                if(isset($data['image_ori'])) {

                    image_remove($data['image_ori']);

                }

                $data['image'] = $image['file_name'];

                $data['image_folder'] = $image['path'];

            }

            unset($data['upload_image']);

            unset($data['image_ori']);



            if ($method == 'create') {

                $data['created_at'] = date('Y-m-d H:i:s');

                $data['created_by'] = $this->session->userdata('login_id');

                $save = $this->post_model->insert($data);

                if ($save !== false) {

                    echo '

                        <script>

                            var clone = $("#file-image").clone();

                            $(".ace-file-input").parent().append(clone);

                            $(".ace-file-input").remove();

                            file_input($("#file-image"));

                        </script>';

                    echo successMessage('Data telah berhasil disimpan.');

                    echo clearForm();

                } else {

                    echo errorMessage('Terjadi kesalahan ketika menyimpan data.');

                }

            } else {

                $data['updated_by'] = $this->session->userdata('login_id');

                $save = $this->post_model->update($data, $data['id']);

                if ($save !== false) {

                    echo successMessage('Data telah berhasil diubah');

                } else {

                    echo errorMessage('Terjadi kesalahan ketika mengubah data.');

                }

            }



        } else {

            echo errorMessage(validation_errors());

        }

    }



    public function set_active($id=0) {

        //only ajax request are allowed

        $this->input->is_ajax_request() or exit('No direct script access allowed!');



        $id = decode($id);

        $data = $this->post_model->get($id);

        if($data) {

            $is_active = ($data->is_active == 1) ? 0 : 1; 

            $save = $this->post_model->update(['is_active' => $is_active], $id);

            if($save) {

                echo 1;

            } else {

                echo 0;

            }            

        }

    }



    public function get_data($id)

    {

        $id = decode($id);

        $data = $this->post_model->get(['id'=>$id, 'type'=>'berita']);

        if ($data) {

            return $data;

        } else {

            $this->session->set_flashdata('error_message', errorMessage(lang('not_found')));

            redirect($this->data['module_url']);

        }

    }



    private function upload_image($filename, $path = '') {

        $image = 'image null';

        if(!empty($_FILES[$filename]['name'])) {

            $path = upload_path($path);

            $config_upload['upload_path']   = $path;

            $config_upload['allowed_types'] = 'jpg|jpeg|png';

            $config_upload['file_name']     = 'IMG_'.time();

            $this->upload->initialize($config_upload);

            if (!$this->upload->do_upload($filename)) {

                $image = 'upload failed';

            } else {

                $upload_data = $this->upload->data();

                $path = str_replace('../uploads/','',$path);

                $image = array('file_name'=>$upload_data['file_name'], 'path'=>$path);



                image_resize($upload_data['full_path'], 1200);

            }

        }

        return $image;

    }



}

