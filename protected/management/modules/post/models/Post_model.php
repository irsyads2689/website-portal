<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Post_model extends X_Model {

    public $table = 'posts';
    public $primary_key = 'id';

}
