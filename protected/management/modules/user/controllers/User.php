<?php defined('BASEPATH') or exit('No direct script access allowed!');

class User extends App_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('administration');
        $this->administration->logged();
        $this->load->model('user_model');
        $this->load->model('auth/auth_model');

        $this->data = [
            'module_title'    => 'Profil',
            'module_icon'     => 'fa fa-folder-open',
            'module_url'      => site_url('user'),
            'menu'            => ['menu' => '', 'submenu' => ''],
            'error_message'   => $this->session->flashdata('error_message'),
            'success_message' => $this->session->flashdata('success_message'),
        ];

    }

    /*
    profile
    */
    public function profile()
    {
        if(!$this->input->post()) {
            $this->data['module_url'] = site_url('user/profile');
            if (!$this->input->is_ajax_request()) {
                $this->template->_default();
                $this->output->set_title($this->data['module_title']);
            }

            $this->data['data'] = $this->user_model->get($this->session->userdata('login_id'));
            $this->load->view('user', $this->data);
        } else {
            $this->input->is_ajax_request() or exit('No direct script access allowed!');

            $this->form_validation->set_rules('username', 'Username', 'required');

            if($this->form_validation->run() === TRUE) {
                $data = $this->input->post();
                $save = $this->user_model->update($data, $this->session->userdata('login_id'));

                if($save) {
                    echo successMessage("Data anda telah berhasil disimpan.");
                } else {
                    echo errorMessage("Terjadi kesalahan ketika menyimpan data.");
                }
            } else {
                echo errorMessage(validation_errors());
            }

        }

    }

 
    /*
    Change Password
    */
    public function change_password()
    {
        if(!$this->input->post()) {
            $this->data['module_title'] = 'Ubah Kata Sandi';
            $this->data['module_url']   = site_url('user/change_password');
            if (!$this->input->is_ajax_request()) {
                $this->template->_default();
                $this->output->set_title($this->data['module_title']);
            }

            $this->load->view('password', $this->data);
        } else {
            $this->input->is_ajax_request() or exit('No direct script access allowed!');

            $this->form_validation->set_rules('new_password', 'Kata Sandi Baru', 'required');
            $this->form_validation->set_rules('confirm_pass', 'Konfirm Kata Sandi', 'required|matches[new_password]');
            $this->form_validation->set_rules('password', 'Kata Sandi', 'required');

            if($this->form_validation->run() === TRUE) {
                $data = $this->input->post();

                $user = $this->user_model->get($this->session->userdata('login_id'));
                if($user) {
                    $this->load->library('password');
                    $verify = $this->password->check_password($data['password'], $user->password);
                    if ($verify) {
                        $password   = $this->password->hash_password($data['new_password']);
                        $update     = $this->user_model->update(['password' => $password], $user->id);
                        if($update) {
                            echo successMessage("Data anda telah berhasil disimpan.");
                        } else {
                            echo errorMessage("Terjadi kesalahan ketika menyimpan data.");
                        }
                    } else {
                        echo errorMessage("Terjadi kesalahan ketika menyimpan data.");
                    }                    
                } else {
                    redirect('auth/logout');
                }
            } else {
                echo errorMessage(validation_errors());
            }

        }

    }

 

}
