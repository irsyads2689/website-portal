<?php
$lang['module_title']       = 'General Setting';
$lang['site_name']          = 'Website Name';
$lang['site_description']   = 'Website Description';
$lang['google_analytics']   = 'Google Analytics';
$lang['social_facebook']    = 'Facebook';
$lang['social_twitter']     = 'Twitter';
$lang['social_instagram']   = 'Instagram';
$lang['social_youtube']     = 'Youtube';
