<?php
$lang['module_title']       = 'Pengaturan Website';
$lang['site_name']          = 'Name Website';
$lang['site_description']   = 'Deskripsi Website';
$lang['google_analytics']   = 'Google Analytics';
$lang['social_facebook']    = 'Facebook';
$lang['social_twitter']     = 'Twitter';
$lang['social_instagram']   = 'Instagram';
$lang['social_youtube']     = 'Youtube';
