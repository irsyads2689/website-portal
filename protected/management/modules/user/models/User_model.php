<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends X_Model {

    public $table = 'users';
    public $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->timestamps = FALSE;
        $this->soft_deletes = FALSE;
    }
}
