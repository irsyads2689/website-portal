<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Borang_detail_model extends X_Model {

    public $table = 'borang_detail';
    public $primary_key = 'id';

    public function get_total_document($jurusan_id, $type = '', $status = '') {
    	$this->db->select('borang_detail.id');
    	$this->db->from('borang');
    	$this->db->join('borang_detail', 'borang.id=borang_detail.borang_id', 'INNER');
    	$this->db->where('borang.jurusan_id', $jurusan_id);
    	if($type) {
    		$this->db->where('borang_detail.tipe_dokumen', $type);
    	}
    	if($status) {
    		$this->db->where('borang_detail.status', $status);
    	}
    	$query = $this->db->get();
    	return $query->num_rows();
    }
    public function get_new_document($jurusan_id, $limit = 5) {
        $this->db->select("borang_detail.*, borang.butir, pemilik.nama");
        $this->db->from("borang");
        $this->db->join("borang_detail", "borang_detail.borang_id=borang.id", "INNER");
        $this->db->join("pemilik", "pemilik.id=borang_detail.pemilik_id");
        $this->db->where("borang.jurusan_id", $jurusan_id);
        $this->db->where("borang.deleted_at", NULL);
        $this->db->order_by("borang_detail.id", 'DESC');
        $this->db->limit($limit);

    	$query = $this->db->get();
    	return ($query->num_rows() > 0) ? $query->result() : false;
    }


}
