<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Owner_model extends X_Model {

    public $table = 'pemilik';
    public $primary_key = 'id';

}
