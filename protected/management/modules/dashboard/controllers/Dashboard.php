<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends App_Controller {

    function __construct()
    {
        parent::__construct();

        $this->load->library('administration');
        $this->administration->logged();
        $lang = $this->administration->language();
        $this->load->language('dashboard', $lang);

        $this->load->model('borang_detail_model');

        $this->data = [
            // 'lang' => $lang,
            'menu' => ['menu' => 'dashboard', 'submenu' => ''],
            'module_title' => 'Dashboard',
            'module_url' => site_url('dashboard'),
            'is_menu' => true,
        ];
    }

    public function index() {
        if(!$this->input->is_ajax_request()) {
            $this->template->_default();
            $this->output->set_title('Dashboard');            
        }
        $this->load->view('dashboard', $this->data);
    }

}
