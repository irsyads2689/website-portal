<div class="">
    <div class="page-title">
          <div class="title_left">
              <h3>
                  <i class="<?php echo $module_icon; ?>"></i> <?php echo $module_title; ?>
              </h3>
          </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel animated zoomIn">
                <div class="x_title">
                    <h2>Change Password</h2>
                    <div class="pull-right">
                        <button type="button" class="btn btn-primary" onclick="$('#form').submit()"><b><i class="fa fa-save"></i></b> Save</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form id="form" class="form-horizontal form-label-left" action="<?php echo site_url('change_password'); ?>" method="post" data-parsley-validate>
                        <div id="infoMessage"></div>

                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Current Password</label>
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                <input type="password" name="password" class="form-control" placeholder="Current Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">New Password</label>
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                <input type="password" name="new_password" class="form-control" placeholder="New Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Confirm Password</label>
                            <div class="col-md-10 col-sm-10 col-xs-12">
                                <input type="password" name="confirm_pass" class="form-control" placeholder="Confirm Password">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-10 col-md-push-2">
                                <button type="submit" class="btn btn-primary"><b><i class="fa fa-save"></i></b> Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/build/js/form.js'); ?>"></script>
