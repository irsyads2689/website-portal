<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends App_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('auth_model');
    }

    public function login() {
        header("Expires: Thu, 19 Nov 1981 08:52:00 GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");

        if ($this->input->is_ajax_request()) {

            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run()) {
                if ($this->auth_model->login($this->input->post('username'), $this->input->post('password'))) {
                    echo successMessage('Login berhasil, sedang mengalihkan...');
                    echo ajaxRedirect('', 300);
                } else {
                    echo errorMessage('Username dan Password tidak valid.');
                    echo clearForm();
                }
            } else {
                echo errorMessage('Username dan password tidak boleh kosong.');
                echo clearForm();
            }
        } else {
            if ($this->session->userdata('logged_in')) {
                redirect('/', 'refresh');
            }

            $this->template->_login();
            $this->output->set_title('Login');
        }
    }

    //log the user out
    public function logout() {
        header("Expires: Thu, 19 Nov 1981 08:52:00 GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");

        //Destroy the session
        $this->session->sess_destroy();

        //redirect them to the login page
        redirect('login', 'refresh');
    }

    public function show_error($err_message)
    {
        return '<div class="loading"><i class="fa fa-spinner fa-spin"> Loading...</div>';

    }

    public function hash($password)
    {
        $this->load->library('password');
        echo $this->password->hash_password($password);
    }


}
