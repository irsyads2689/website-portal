<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends X_Model {

    public $table = 'users';
    public $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
    }

    public function login($username, $password)
    {
        if (empty($username) || empty($password)) {
            return FALSE;
        }

        $user = $this->get(['username' => $username]);
        if ($user) {
            $this->load->library('password');
            $verify = $this->password->check_password($password, $user->password);

            if ($verify) {
                if ($user->is_active == 0) {
                    return FALSE;
                }

                $this->set_session($user);
                return TRUE;
            }
        }
        return FALSE;
    }

    public function set_session($user) {

        $session_data = array(
            'logged_in' => TRUE,
            'login_id' => $user->id,
            'login_username' => $user->username,
            'login_name' => $user->name,
            'login_group' => $user->group_id,
            'old_last_login' => $user->last_login
        );

        $this->session->set_userdata($session_data);

        return TRUE;
    }

    public function get_password($string, $type='encrypt') {
        $this->load->library('password');
        return $this->password->hash_password($string, $type);
    }

}