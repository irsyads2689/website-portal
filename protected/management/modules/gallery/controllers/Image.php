<?php defined('BASEPATH') or exit('No direct script access allowed!');

class Image extends App_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('administration');
        $this->administration->logged();
        $this->load->model('gallery_model');

        $this->data = [
            'module_title'    => 'Galeri',
            'module_icon'     => 'icon-magazine',
            'module_url'      => site_url('gallery/image'),
            'menu'            => ['menu' => 'gallery', 'submenu' => 'image'],
            'error_message'   => $this->session->flashdata('error_message'),
            'success_message' => $this->session->flashdata('success_message'),
            'need_editor'     => true,
            'has_trash'       => true,

        ];

    }

    public function index()
    {
        if (!$this->input->get('get_list')) {
            if (!$this->input->is_ajax_request()) {
                $this->template->_default();
                $this->output->set_title($this->data['module_title']);
            }

            $this->data['table'] = [
                'columns' => [
                    'id' => ['name'  => 'posts.id', 'title' => 'No.' , 'width' => '20px', 'filter' => ['type' => 'text']],
                    'title' => ['name'  => 'posts.title', 'title' => 'Judul' , 'width' => 'auto', 'filter' => ['type' => 'text']],
                    'is_active' => ['name'  => 'posts.is_active', 'title' => 'Aktif' , 'width' => '75px', 'filter' => ['type' => 'text']],
                    'created_at' => ['name'  => 'posts.created_at', 'title' => '', 'width' => '150px', 'filter' => ['type' => 'none']]
                ],
                'disable_sorting' => '0,2,3',
                'default_sort_col' => '3',
                'default_sort_order' => 'desc'
            ];

            $this->load->view('list', $this->data);
        } else {
            $this->load->library('datatable');

            $this->datatable->select("galleries.id, galleries.title, galleries.image, galleries.image_folder, galleries.is_active, galleries.created_at");
            $this->datatable->from("galleries");
            $this->datatable->where("galleries.type", 'foto');

            $array = json_decode($this->datatable->generate(), TRUE);
            $number = $this->input->get('start') ? $this->input->get('start') : 0;
            foreach($array['data'] as $key => $value) {
                $number++;
                if($value['is_active']==1) {
                    $status = '
                    <a href="'.$this->data['module_url'].'/set_active/'.encode($value['id']).'" class="orange btn-status" title="Non Aktif">
                        <i class="ace-icon fa fa-eye-slash"></i> Non Aktif
                    </a>';
                } else {
                    $status = '
                    <a href="'.$this->data['module_url'].'/set_active/'.encode($value['id']).'" class="orange btn-status" title="Aktif">
                        <i class="ace-icon fa fa-eye"></i> Aktif
                    </a>';                    
                }
                $title = 
                    '<div class="">
                        <div>'.$value['title'].'</div>
                        <small>
                            <a href="'.$this->data['module_url'].'/update/'.encode($value['id']).'" class="green ajaxan" title="Edit Profil">
                                <i class="ace-icon fa fa-pencil"></i> Edit
                            </a> | 
                            '.$status.'
                        </small>
                    </div>';
                switch($value['is_active']) {
                    case 1: $is_active = '<span class="label label-success">Ya</span>'; break;
                    default: $is_active = '<span class="label label-danger">Tidak</span>'; break;
                }
                $created_at = '<div>Dibuat pada:<br>'.date('d/m/Y H:i:s', strtotime($value['created_at'])).'</div>';

                $array['data'][$key]['id']          = $number;
                $array['data'][$key]['title']       = $title;
                $array['data'][$key]['is_active']   = $is_active;
                $array['data'][$key]['created_at']  = $created_at;
            }
            echo json_encode($array);
        }
    }

    public function create()
    {
        $form_title = 'Tambah Galeri';
        if (!$this->input->is_ajax_request()) {
            $this->template->_default();
            $this->output->set_title($form_title);
        }

        $this->data['form'] = [
            'title'  => $form_title
        ];

        $this->load->view('form', $this->data);
    }

    public function update($id)
    {
        $form_title = 'Edit Galeri';
        if (!$this->input->is_ajax_request()) {
            $this->template->_default();
            $this->output->set_title($form_title);
        }

        $data = $this->get_data($id);
        $this->data['form'] = [
            'title'  => $form_title,
            'data'   => $data
        ];

        $this->data['galleries'] = $this->gallery_model->get_all(['parent'=>$data->id]);

        $this->load->view('form', $this->data);
    }

    public function save()
    {
        //only ajax request are allowed
        $this->input->is_ajax_request() or exit('No direct script access allowed!');

        $this->form_validation->set_rules('title', 'Judul Galeri', 'trim|required');

        $this->load->library('upload');
        $this->load->library('user_agent');
        $method = substr($this->agent->referrer(), -6, 6);

        //validate the form
        if ($this->form_validation->run() === true) {
            $data = $this->input->post();
            $data['type']       = 'foto';
            $data['title']      = $data['title'];
            // $data['meta_title'] = $data['meta_title'] ? $data['meta_title'] : $data['title'];
            $data['slug']       = url_title(strtolower($data['title']), '-');
            $data['is_active']  = isset($data['is_active']) ? 1 : 0;

            // validasi title
            $check_title = $this->gallery_model->get(['id !=' => $data['id'], 'title' => $data['title']]);
            if($check_title) {
                echo errorMessage('Judul sudah ada di database.');
                return false;
            }

            $contents   = isset($data['content']) ? $data['content'] : '';
            unset($data['content']);
            if(!$contents) {
                echo errorMessage('Konten galeri masih kosong.');
                exit;
            }

            $created_at = date('Y-m-d H:i:s');
            $created_by = $this->session->userdata('login_id');

            if ($method == 'create') {
                $data['created_at'] = $created_at;
                $data['created_by'] = $created_by;
                $save = $this->gallery_model->insert($data);
                if(!$save) {
                    echo errorMessage('Terjadi kesalahan ketika menyimpan data.');
                    exit;
                }

                foreach($contents as $key => $value) {
                    if(isset($_FILES['upload_images']['name'][$key])) {
                        // set new file upload
                        $this->set_FILE('upload_image', 'upload_images', $key);

                        // upload image more
                        $image = $this->upload_image('upload_image');

                        if(isset($image['file_name'])) {
                            $contents[$key]['image']        = $image['file_name'];
                            $contents[$key]['image_folder'] = $image['path'];
                            $contents[$key]['parent']       = $save;
                            $contents[$key]['created_at']   = $created_at;
                            $contents[$key]['created_by']   = $created_by;
                        } else {
                            // if not upload image or upload image failed, then remove row data
                            unset($contents[$key]);
                        }
                    } else {
                        // if not upload image, then remove row data
                        unset($contents[$key]);
                    }
                }

                if(!$contents) {
                    echo errorMessage('Pengisian konten galeri belum lengkap.');
                    exit;
                }
                // save content program
                $save = $this->db->insert_batch('galleries', $contents);
                if ($save !== false) {
                    // echo ajaxRedirect('program', 500);
                    echo successMessage('Data telah berhasil disimpan.');
                    echo loadPage(site_url('gallery/image'), 'Galeri');
                } else {
                    echo errorMessage('Terjadi kesalahan ketika menyimpan data.');
                }
            } else {
                unset($data['upload_images']);
                $data['updated_by'] = $this->session->userdata('login_id');
                $save = $this->gallery_model->update($data, $data['id']);
                if (!$save) {
                    echo errorMessage('Terjadi kesalahan ketika mengubah data.');
                    exit;
                }

                $insert = false;
                foreach($contents as $key => $value) {
                    // if image more change / no change / delete 
                    if(isset($value['id'])) {
                        // if image more changed
                        if(isset($_FILES['upload_images']['name'][$key])) {
                             // set new file upload
                            $this->set_FILE('upload_image', 'upload_images', $key);

                            // upload image more 
                            $image = $this->upload_image('upload_image', $value['image_folder']);
                            if(isset($image['file_name'])) {
                                // image ori deleted
                                if(isset($value['image_ori'])) {
                                    image_remove($contents[$key]['image_ori']);
                                    
                                    $contents[$key]['image']        = $image['file_name'];
                                    $contents[$key]['image_folder'] = $image['path'];
                                    $contents[$key]['updated_by']   = $created_by;
                                }
                            }
                        }

                        unset($contents[$key]['image_ori']);
                        $save = $this->gallery_model->update($contents[$key], $value['id']);
                        
                        unset($contents[$key]);
                    } else {
                        if(isset($_FILES['upload_images']['name'][$key])) {
                            // set new file upload
                            $this->set_FILE('upload_image', 'upload_images', $key);

                            // upload new image more
                            $image = $this->upload_image('upload_image');
                            if($image != 'upload failed' OR $image != 'image null') {
                                $contents[$key]['image']        = $image['file_name'];
                                $contents[$key]['image_folder'] = $image['path'];
                                $contents[$key]['parent']       = $data['id'];
                                $contents[$key]['created_at']   = $created_at;
                                $contents[$key]['created_by']   = $created_by;
                                $insert = true;
                            } else {
                                // if not upload image or upload image failed, then remove row data
                                unset($contents[$key]);
                            }
                        } else {
                            unset($contents[$key]);
                        }
                    }
                }

                if($insert) {
                    $save = $this->db->insert_batch('galleries', $contents);
                }

                if ($save !== false) {
                    // echo ajaxRedirect('program', 1500);
                    echo successMessage('Data telah berhasil diubah');
                    echo loadPage(site_url('gallery/image'), 'Galeri', 700);
                } else {
                    echo errorMessage('Terjadi kesalahan ketika mengubah data.');
                }

            }

        } else {
            echo errorMessage(validation_errors());
        }
    }

    public function delete($id=0) {
        //only ajax request are allowed
        $this->input->is_ajax_request() or exit('No direct script access allowed!');

        $id = decode($id);
        $data = $this->gallery_model->get($id);
        if($data) {
            $delete = $this->gallery_model->delete($id);
            if($delete) {
                $image = get_image($data->image_folder, $data->image);
                image_remove($image);
                echo 1;           
            } else {
                echo 0;
            }            
        } else {
            echo 0;
        }
    }

    public function set_active($id=0) {
        //only ajax request are allowed
        $this->input->is_ajax_request() or exit('No direct script access allowed!');

        $id = decode($id);
        $data = $this->gallery_model->get($id);
        if($data) {
            $is_active = ($data->is_active == 1) ? 0 : 1; 
            $save = $this->gallery_model->update(['is_active' => $is_active], $id);
            if($save) {
                echo 1;
            } else {
                echo 0;
            }            
        }
    }

    public function get_data($id)
    {
        $id = decode($id);
        $data = $this->gallery_model->get(['id'=>$id, 'type'=>'foto']);
        if ($data) {
            return $data;
        } else {
            $this->session->set_flashdata('error_message', errorMessage(lang('not_found')));
            redirect($this->data['module_url']);
        }
    }

    private function upload_image($filename, $path = '') {
        $image = 'image null';
        if(!empty($_FILES[$filename]['name'])) {
            $path = upload_path($path);
            $config_upload['upload_path']   = $path;
            $config_upload['allowed_types'] = 'jpg|jpeg|png';
            $config_upload['file_name']     = 'IMG_'.time();
            $this->upload->initialize($config_upload);
            if (!$this->upload->do_upload($filename)) {
                $image = 'upload failed';
            } else {
                $upload_data = $this->upload->data();
                $path = str_replace('../uploads/','',$path);
                $image = array('file_name'=>$upload_data['file_name'], 'path'=>$path);

                image_resize($upload_data['full_path'], 1200);
            }
        }
        return $image;
    }

    private function set_FILE($filename, $filename_upload, $key) {
        $_FILES[$filename]['name']       = $_FILES[$filename_upload]['name'][$key];
        $_FILES[$filename]['type']       = $_FILES[$filename_upload]['type'][$key];
        $_FILES[$filename]['tmp_name']   = $_FILES[$filename_upload]['tmp_name'][$key];
        $_FILES[$filename]['error']      = $_FILES[$filename_upload]['error'][$key];
        $_FILES[$filename]['size']       = $_FILES[$filename_upload]['size'][$key];
    }
}
