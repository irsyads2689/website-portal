<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="<?php echo site_url(); ?>">Dashboard</a>
                </li>
                <li><a href="<?php echo $module_url; ?>" class="ajaxan" title="<?php echo $module_title; ?>"><?php echo $module_title; ?></a></li>
                <li class="active"><?php echo $form['title']; ?></li>
            </ul>
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    <?php echo $form['title']; ?>
                    <a href="<?php echo $module_url; ?>" class="btn btn-xs btn-danger ajaxan" title="<?php echo $module_title; ?>">
                        <i class="ace-icon fa fa-undo"></i> Kembali
                    </a>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <form id="form" class="form-horizontal" method="post" action="<?php echo $module_url.'/save'; ?>" enctype="multipart/form-data">
                        <div id="infoMessage"></div>
                        <input type="hidden" name="id" value="<?php echo (isset($form['data'])) ? $form['data']->id : ''; ?>">

                        <fieldset>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <label class="col-md-12 col-sm-12 col-xs-12">Judul</label>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <input type="text" name="title" class="form-control" placeholder="<?php echo lang('title'); ?>"  value="<?php echo (isset($form['data'])) ? $form['data']->title : ''; ?>">
                                        </div>
                                    </div>
                                    <div class="widget-box">
                                        <div class="widget-header widget-header-flat">
                                            <h4 class="widget-title grey smaller">Foto</h4>
                                        </div>
                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <table class="table table-hover">
                                                    <?php /*
                                                    <thead>
                                                        <tr>
                                                            <th>Foto</th>
                                                            <th style="width: 50px">Order</th>
                                                            <th style="width: 50px"></th>
                                                        </tr>
                                                    </thead>
                                                    */ ?>
                                                    <tbody id="galleries">
                                                        <tr class="clone hidden">
                                                            <td>
                                                                <input multiple class="upload" type="file" name="" />
                                                            </td>
                                                            <td style="width: 150px">
                                                                <input type="text" name="" class="form-control sort_order">
                                                            </td>
                                                            <td style="text-align: center; width: 50px">
                                                                <a href="javascript:;" class="btn btn-xs btn-white btn-warning remove-image">
                                                                    <i class="ace-icon fa fa-minus bigger-110 icon-only"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        if(isset($galleries) AND $galleries) {
                                                            foreach($galleries as $key => $row) {
                                                                echo '
                                                                <tr>
                                                                    <input type="hidden" name="content['.$key.'][id]" value="'.$row->id.'">
                                                                    <input type="hidden" name="content['.$key.'][image_ori]" value="'.get_image($row->image_folder, $row->image).'" data-title="'.$row->image.'" class="image_ori">
                                                                    <input type="hidden" name="content['.$key.'][image_folder]" value="'.$row->image_folder.'">
                                                                    <td>
                                                                        <input multiple class="upload_image" type="file" id="upload_image_'.$key.'" name="upload_images['.$key.']" />
                                                                    </td>
                                                                    <td style="width: 150px">
                                                                        <input type="text" name="content['.$key.'][sort_order]" value="'.$row->sort_order.'" class="form-control content-sort_order">
                                                                    </td>
                                                                    <td style="text-align: center; width: 50px">
                                                                        <a href="javascript:;" class="btn btn-xs btn-white btn-danger remove-image" data-id="'.encode($row->id).'">
                                                                            <i class="ace-icon fa fa-times bigger-110 icon-only"></i>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                                ';
                                                            }
                                                        }
                                                        ?>
                                                        <tr>
                                                            <td colspan="3" style="text-align: center;">
                                                                <a href="javascript:;" id="add-image" class="btn btn-sm btn-success" data-key="<?php echo (isset($galleries) AND $galleries) ? COUNT($galleries) : 0; ?>">
                                                                    <i class="ace-icon fa fa-plus bigger-120"></i> Tambah Foto
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="widget-box">
                                        <div class="widget-header widget-header-flat">
                                            <h4 class="widget-title grey smaller">Status</h4>
                                        </div>

                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <div class="form-group">
                                                    <label class="col-md-12">Aktif</label>
                                                    <div class="col-md-12">
                                                        <label>
                                                            <input id="is_active" name="is_active" class="ace ace-switch ace-switch-7" type="checkbox" value="1" 
                                                                <?php echo (!isset($form['data'])) ? 'checked' : ($form['data']->is_active==1 ? 'checked' : ''); ?>>
                                                            <span class="lbl"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group no-margin-bottom">
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-sm btn-info btn-block">
                                                            <i class="ace-icon fa fa-floppy-o bigger-120"></i> Simpan
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                    </form>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->

<div id="dialog-delete">
    
</div>

<script src="<?php echo base_url(); ?>assets/js/form.js" type="text/javascript"></script>

<script type="text/javascript">
    var module_url = "<?php echo $module_url; ?>";
    jQuery(function($) {
        // list image
        $('.content-sort_order').each(function(k, v) {
            var input_file = $(this).parents('tr').find('input.upload_image');
            var id_input_file = input_file.attr('id');
            file_input($('#'+id_input_file));
            var image_ori = $(this).parents('tr').find('input.image_ori');
            var url_image = image_ori.val();
            var title_image = image_ori.data('title') || '';
            if(title_image != '') {
                input_file.next().addClass('hide-placeholder selected').removeAttr('data-title');
                input_file.next().find('.ace-file-name').addClass('large').attr('data-title', title_image).prepend('<img id="image_primary" class="middle" style="background-image: url(\''+url_image+'\'); background-size: 100%;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQImWNgYGBgAAAABQABh6FO1AAAAABJRU5ErkJggg==">');
            }
        })

        /* ========= IMAGE =========== */
        // add image
        $('#add-image').on('click', function(e) {
            e.preventDefault();

            var ii = $('input.upload_image'),
            i = ii.eq(ii.length - 1).next().find('img.middle').attr('class'),
            o = ii.eq(ii.length - 1).parents('tr').find('input.content-sort_order').val();
            if((ii.length > 1 && i==undefined) || o=='') {
                return false;
            }

            var key     = $(this).attr('data-key');
            var clone   = $('#galleries').find('.clone').clone();

            var id = 'upload_image'+key;
            var name = 'upload_images['+key+']';
            clone.find('input.upload').attr('name', name).attr('id', id).toggleClass('upload upload_image');
            clone.find('input.sort_order').attr('name', 'content['+key+'][sort_order]').toggleClass('title content-sort_order');

            key++;
            $(this).attr('data-key', key);
            clone.removeClass('clone hidden');
            $(this).parent().parent().before(clone);
            file_input($('#'+id));
        })

        // delete image
        $('#galleries').on('click', 'a.remove-image', function(e) {
            e.preventDefault();

            var this_ = $(this),
            id = this_.data('id');
            if(id == undefined) {
                this_.parent().parent().remove();
            } else {
                var ask = confirm('Anda yakin mau hapus foto ini?');
                if(ask) {
                    $.ajax({
                        url: module_url + '/delete/' + id,
                        success: function(results) {
                            if(results==1) {
                                this_.parent().parent().remove();
                                alert('Foto berhasil dihapus.');
                            } else {
                                alert('Foto gagal dihapus.');
                            }
                        },
                        error: function() {
                            alert('Server Internal Error.');
                        }
                    })
                }
            }
        })


        $('#is_active').click(function() {
            var checked = $(this).attr('checked');
            if(checked) {
                $(this).removeAttr('checked');
            } else {
                $(this).attr('checked', true);
            }
        })
    })

    function file_input(elm) {
        elm.ace_file_input({
            style:'well',
            btn_choose: 'Drop images here or click to choose',
            no_icon: 'ace-icon fa fa-picture-o',
            allowExt: ["jpeg", "jpg", "png"],
            allowMime: ["image/jpg", "image/jpeg", "image/png"],
            btn_change:null,
            droppable:true,
            thumbnail:'large',
            icon_remove:null,
            before_change:function(files, dropped) {
                return true;
            },
            before_remove : function() {
                return true;
            },
            preview_error : function(filename, error_code) {
                //alert(error_code);
            }

        }).on('change', function(){
        });
    }

</script>
