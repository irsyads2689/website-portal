<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="<?php echo site_url(); ?>">Dashboard</a>
                </li>
                <li class="active"><?php echo $module_title; ?></li>
            </ul>
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    <?php echo $module_title; ?>
                    <a href="<?php echo $module_url; ?>/create" class="btn btn-success btn-xs ajaxan" title="Tambah Artikel">
                        <i class="ace-icon fa fa-plus"></i> Tambah
                    </a>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <?php
                    echo (!empty($success_message)) ? $success_message : '';
                    echo (!empty($error_message)) ? $error_message : '';
                    ?>
                    <div class="widget-box transparent">
                        <div class="widget-body">
                            <div class="widget-main no-padding">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div>
                                            <table id="data-grid" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <?php
                                                        if ($table['columns']) {
                                                            foreach ($table['columns'] as $key => $column) {
                                                                echo '<th style="width:'.$column['width'].'">'.$column['title'].'</th>';
                                                            }
                                                        }
                                                        ?>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- PAGE CONTENT ENDS -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->

<?php
$columns = '';
if ($table['columns']) {
    foreach ($table['columns'] as $key => $column) {
        $columns .= '{"data":"'.$key.'", "name":"'.$column['name'].'"},';
    }
}
?>
<script type="text/javascript">
var current_url = '<?php echo current_url(); ?>';
var module_url = '<?php echo $module_url; ?>';

var table = $('#data-grid').DataTable({
    "ajax": current_url + "?get_list=true",
    "columns": [<?php echo $columns; ?>],
    "columnDefs": [ { orderable: false, targets: [<?php echo $table['disable_sorting']; ?>] } ],
    "order": [[ <?php echo $table['default_sort_col'].', "'.$table['default_sort_order'].'"'; ?> ]]
});

$(document).ready(function() {
    $('#data-grid').on('click', '.btn-status',  function(e) {
        e.preventDefault();

        var url = $(this).attr('href');
        $('#loader').show();
        $.get(url, function(result) {
            $('#loader').hide();
            if(result)
                table.ajax.reload(null, false);
        })
    });    
})

</script>