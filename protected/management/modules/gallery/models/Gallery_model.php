<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery_model extends X_Model {

    public $table = 'galleries';
    public $primary_key = 'id';

}
