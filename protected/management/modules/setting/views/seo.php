<div class="main-content">
    <div class="main-content-inner">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="#">Dashboard</a>
                </li>
                <li class="active"><?php echo $module_title; ?></li>
            </ul>
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    <?php echo $module_title; ?>
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">

                    <div class="widget-box widget-color-blue">
                        <div class="widget-header widget-header-flat widget-header-large">
                            <h4 class="widget-title">
                                <?php echo $module_title; ?>
                            </h4>

                            <div class="widget-toolbar no-border">
                                <div class="inline dropdown-hover">
                                    <button type="button" class="btn btn-white btn-info" onclick="$('#form').submit();">
                                        <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i> Simpan
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main no-padding">
                                <form id="form" class="form-horizontal" method="post" action="<?php echo $module_url; ?>" enctype="multipart/form-data">
                                    <div id="infoMessage"></div>

                                    <fieldset>
                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Meta Title</label>
                                            <div class="col-md-10 col-sm-10 col-xs-12">
                                                <input type="text" name="setting[meta_title]" class="form-control" placeholder="Meta Title"  value="<?php echo (isset($data['meta_title'])) ? $data['meta_title'] : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Meta Deskripsi</label>
                                            <div class="col-md-10 col-sm-10 col-xs-12">
                                                <textarea name="setting[meta_description]" class="form-control" rows="5"><?php echo (isset($data['meta_description'])) ? $data['meta_description'] : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Meta Keywords</label>
                                            <div class="col-md-10 col-sm-10 col-xs-12">
                                                <input type="text" name="setting[meta_keywords]" class="form-control" placeholder="Meta Title"  value="<?php echo (isset($data['meta_keywords'])) ? $data['meta_keywords'] : ''; ?>">
                                            </div>
                                        </div>
                                    </fieldset>

                                    <div class="form-actions" style="padding-left: 20px">
                                        <button type="submit" class="btn btn-white btn-info ">
                                            <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i> Simpan
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- PAGE CONTENT ENDS -->
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->

<div id="dialog-delete">
    
</div>

<script src="<?php echo base_url(); ?>assets/js/form.js" type="text/javascript"></script>

<script type="text/javascript">
    var module_url = "<?php echo $module_url; ?>";
    jQuery(function($) {
        $('#status').click(function() {
            var checked = $(this).attr('checked');
            if(checked) {
                $(this).removeAttr('checked');
            } else {
                $(this).attr('checked', true);
            }
        })
    })

</script>
