<?php defined('BASEPATH') or exit('No direct script access allowed!');



class Setting extends App_Controller

{



    public function __construct()

    {

        parent::__construct();



        $this->load->library('administration');

        $this->administration->logged();

        $this->load->model('option_model');



        $this->data = [

            'module_title'    => 'Pengaturan Umum',

            'module_icon'     => 'icon-magazine',

            'module_url'      => site_url('setting'),

            'menu'            => ['menu' => 'setting', 'submenu' => 'general'],

            'error_message'   => $this->session->flashdata('error_message'),

            'success_message' => $this->session->flashdata('success_message'),

        ];



    }



    /*

    Setting

    */

    public function index()

    {

        if(!$this->input->post()) {

            $this->data['module_title'] = 'Pengaturan Umum';

            if (!$this->input->is_ajax_request()) {

                $this->template->_default();

                $this->output->set_title($this->data['module_title']);

            }



            $this->data['data'] = get_option();

            $this->load->view('setting', $this->data);

        } else {

            $this->input->is_ajax_request() or exit('No direct script access allowed!');



            $this->form_validation->set_rules('setting[site_name]', 'Nama Website', 'required');

            $this->form_validation->set_rules('setting[site_description]', 'Deskripsi Website', 'required');

            $this->form_validation->set_rules('setting[google_analytics]', 'Google Analytics', 'required');
            $this->form_validation->set_rules('setting[site_address]', 'Alamat', 'required');
            $this->form_validation->set_rules('setting[site_email]', 'Email', 'required');
            $this->form_validation->set_rules('setting[site_phone]', 'No. Telp', 'required');



            if($this->form_validation->run() === TRUE) {

                $data = $this->input->post();

                $settings = isset($data['setting']) ? $data['setting'] : '';

                if($settings) {

                    foreach($settings as $key => $value) {

                        $check = get_option($key);

                        if($check) {

                            $save = $this->option_model->update(['value' => $value], ['name' => $key]);

                        } else {

                            $save = $this->option_model->insert(['name' => $key, 'value' => $value]);

                        }

                    }


                    if($save) {

                        echo successMessage('Data telah berhasil disimpan.');

                    } else {

                        echo errorMessage('Terjadi kesalahan ketika menyimpan data.');

                    }

                } else {

                    echo errorMessage('Inputan belum ada.');

                }

            } else {

                echo errorMessage(validation_errors());

            }



        }



    }



 

    /*

    SEO

    */

    public function seo()

    {

        if(!$this->input->post()) {

            $this->data['module_title'] = 'Pengaturan SEO';

            $this->data['module_url']   = site_url('setting/seo');

            $this->data['menu']['submenu']   = 'seo';

            if (!$this->input->is_ajax_request()) {

                $this->template->_default();

                $this->output->set_title($this->data['module_title']);

            }



            $this->data['data'] = get_option();

            $this->load->view('seo', $this->data);

        } else {

            $this->input->is_ajax_request() or exit('No direct script access allowed!');



            $this->form_validation->set_rules('setting[meta_title]', 'Meta Title', 'required');

            $this->form_validation->set_rules('setting[meta_description]', 'Meta Description', 'required');

            $this->form_validation->set_rules('setting[meta_keywords]', 'Meta Keyrowds', 'required');



            if($this->form_validation->run() === TRUE) {

                $data = $this->input->post();

                $settings = isset($data['setting']) ? $data['setting'] : '';

                if($settings) {

                    $save = false;

                    foreach($settings as $key => $value) {

                        $check = get_option($key);

                        if($check) {

                            $save = $this->option_model->update(['value' => $value], ['name' => $key]);

                        } else {

                            $save = $this->option_model->insert(['name' => $key, 'value' => $value]);

                        }

                    }



                    if($save) {

                        echo successMessage('Data telah berhasil disimpan.');

                    } else {

                        echo errorMessage('Terjadi kesalahan ketika menyimpan data.');

                    }

                } else {

                   errorMessage('Inputan belum ada.');

                }

            } else {

                echo errorMessage(validation_errors());

            }



        }



    }



 



}

