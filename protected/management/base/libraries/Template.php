<?php (defined('BASEPATH')) or exit('No direct script access allowed');

/**
* Omeoo Framework
* A framework for PHP development
*
* @package     Omeoo Framework
* @author      Omeoo Dev Team
* @copyright   Copyright (c) 2016, Omeoo Media (http://www.omeoo.com)
*/

class Template
{

    public function __construct()
    {
        $this->app = & get_instance();
    }

    public function _default($default = 'default')
    {
        $this->app->output->set_template($default);
    }

    public function _login($default = 'login')
    {
        $this->app->output->set_template($default);
    }
}
