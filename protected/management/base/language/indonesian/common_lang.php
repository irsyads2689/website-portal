<?php
$lang['profile']			= 'Profil';
$lang['profile_plural']    	= 'Profil';

$lang['partner']			= 'Rekanan';
$lang['partner_plural']    	= 'Rekanan';

$lang['facility']           = 'Fasilitas';
$lang['facility_plural']    = 'Fasilitas';
$lang['facility_medical']           = 'Layanan Medis';
$lang['facility_medical_plural']    = 'Layanan Medis';
$lang['facility_support']           = 'Penunjang';
$lang['facility_support_plural']    = 'Penunjang';

$lang['information']           	= 'Informasi';
$lang['information_plural']    	= 'Informasi';
$lang['health']           	= 'Kesehatan';
$lang['health_plural']    	= 'Kesehatan';
$lang['activities']           	= 'Kegiatan';
$lang['activities_support']    	= 'Kegiatan';
$lang['specialist']           		= 'Spesialis';
$lang['specialist_support']    		= 'Spesialis';
$lang['doctor']           		= 'Dokter';
$lang['doctor_support']    		= 'Dokter';
$lang['schedule']           	= 'Jadwal Praktek Dokter';
$lang['schedule_support']    	= 'Jadwal Praktek Dokter';

$lang['setting']        	= 'Pengaturan';
$lang['setting_plural'] 	= 'Pengaturan';

$lang['setting_general']        	= 'General';
$lang['setting_general_plural'] 	= 'General';

$lang['setting_seo']        	= 'SEO';
$lang['setting_seo_plural'] 	= 'SEO';
