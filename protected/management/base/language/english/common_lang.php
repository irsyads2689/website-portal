<?php
$lang['news']           	= 'News';
$lang['news_plural']    	= 'News';

$lang['category']			= 'Category';
$lang['category_plural']    = 'Category';

$lang['design']				= 'Design';
$lang['design_plural']    	= 'Design';

$lang['slider']				= 'Slider';
$lang['slider_plural']    	= 'Slider';

$lang['page']				= 'Page';
$lang['page_plural']    	= 'Page';

$lang['advocate_team']		 	= 'Team of Advocate';
$lang['advocate_team_plural']   = 'Team of Advocate';

$lang['service_legal']		 	= 'Service of Legal Aid';
$lang['service_legal_plural']   = 'Service of Legal Aid';

$lang['setting']        	= 'Setting';
$lang['setting_plural'] 	= 'Setting';

$lang['setting_general']        	= 'General';
$lang['setting_general_plural'] 	= 'General';

$lang['setting_seo']        	= 'SEO';
$lang['setting_seo_plural'] 	= 'SEO';
