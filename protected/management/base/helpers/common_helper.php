<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

if ( ! function_exists('loadPage')) {
    function loadPage($url, $title="", $timer = 0) {
        return "<script>
        var url = '$url';
        var title = '$title';
        $('#loader').show();
        setTimeout(function() {
            $('#content').load(url, function() {
                $('#loader').hide();
            });
        }, ".$timer.")
        document.title = title + ' | Insan Indonesia';
        window.history.pushState('', title, url);
        $(this).parent().siblings().removeClass('active');
        $(this).parent().siblings().removeClass('open');
        $(this).parent().siblings().find('ul').css({display:'hidden'});
        $(this).parent().siblings().find('li').removeClass('active');
        $(this).parent().addClass('active');
        $(this).parent().parent().parent().siblings().removeClass('active');
        $(this).parent().parent().parent().siblings().removeClass('open');
        $(this).parent().parent().parent().siblings().find('li').removeClass('active');
        $(this).parent().parent().parent().addClass('open active');
        </script>";
    }
}

if ( ! function_exists('image_remove')) {
    function image_remove($path_image)
    {
        $app =& get_instance();
        $parsed_url = parse_url($path_image, PHP_URL_PATH);
        $dir        = pathinfo(str_replace('/yatim_web','',$parsed_url), PATHINFO_DIRNAME);
        $extension  = pathinfo($path_image, PATHINFO_EXTENSION);
        $filename   = pathinfo($path_image, PATHINFO_FILENAME);
        $image_dir  = '..'. $dir .'/'. $filename .'.'. $extension;
        // $image_dir_thumb  = '..'. $dir .'/'. $filename .'_thumb.'. $extension;
        if(file_exists($image_dir)) {
            unlink($image_dir);
            // unlink($image_dir_thumb);
        }
    }
}

if ( ! function_exists('image_resize')) {
    function image_resize($path_image, $width = 1200, $create_thumb = FALSE)
    {
        $app =& get_instance();

        $config['image_library']    = 'gd2';
        $config['source_image']     = $path_image;
        $config['create_thumb']     = $create_thumb;
        $config['maintain_ratio']   = TRUE;
        $config['width']            = $width;
        // $config['height']           = 0;

        $app->load->library('image_lib', $config);

        $app->image_lib->resize();
    }
}

if ( ! function_exists('image_to_base64')) {
    function image_to_base64($path_image)
    { 
        $type   = pathinfo($path_image, PATHINFO_EXTENSION);
        $image  = file_get_contents($path_image);
        $base64Image = 'data:image/png;base64,' . base64_encode($image);
        return $base64Image;
    }
}

if ( ! function_exists('to_number')) {
    function to_number($val = 0)
    {
        return floatval(str_replace(',', '', $val));
    }
}

if ( ! function_exists('str_first_upper')) {
    function str_first_upper($string = null)
    {
        return ucwords(strtolower($string));
    }
}

if ( ! function_exists('get_option')) {
    function get_option($name = null)
    {
        $app =& get_instance();
        if (is_null($name))
        {
            $setting = $app->option_model->get_all();
            if ($setting) {
                foreach($setting as $set) {
                    $settings[$set->name] = $set->value;
                }
            }
            return (isset($settings)) ? $settings : '';
        }
        else
        {
            $setting = $app->option_model->fields('id,value')->get(['name' => $name]);
            return ($setting) ? $setting->value : '';
        }
    }
}

if (!function_exists('get_image')) {
    function get_image($folder = '', $image) {
        $path = 'uploads/'.$folder.$image;
        $path = str_replace('adminpanel/', '', base_url($path));
        return $path;
    }
}

if (!function_exists('upload_path')) {
    function upload_path($path = '') {
        if($path != '') {
            return '../uploads/'.$path;
        }
        
        $year = date('Y/');
        $path = '../uploads/'.$year;

        if(!is_dir($path)) {
            mkdir($path);
            $file_index = fopen($path.'index.html', 'w');
            $html = '<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Directory access is forbidden.</p></body><html>';
            fwrite($file_index, $html);
            fclose($file_index);
        }

        $path .= date('m/');

        if(!is_dir($path)) {
            mkdir($path);
            $file_index = fopen($path.'index.html', 'w');
            $html = '<!DOCTYPE html><html><head><title>403 Forbidden</title></head><body><p>Directory access is forbidden.</p></body><html>';
            fwrite($file_index, $html);
            fclose($file_index);
        }

        return $path;
    }
}

if (!function_exists('encode')) {
    function encode($string) {
        return encrypt_decrypt('encrypt', $string);
    }
}

if (!function_exists('decode')) {
    function decode($string) {
        return encrypt_decrypt('decrypt', $string);
    }
}

if (!function_exists('encrypt_decrypt')) {
    function encrypt_decrypt($action, $string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'eXnumberFramework';
        $secret_iv = 'Brother';

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }
}

/*==-- JSON to Array --==*/
if (!function_exists('to_array')) {
    function to_array($json) {
        $array = json_decode($json, TRUE);
        return (is_array($array)) ? $array : FALSE;
    }
}

/*==-- Array to JSON --==*/
if (!function_exists('to_json')) {
    function to_json($array) {
        return (is_array($array)) ? json_encode($array) : FALSE;
    }
}

/*==-- Rupiah currency --==*/
if (!function_exists('currency')) {
    function currency($val, $currency = '') {
        $value = number_format($val, 0, ',', '.');
        return ($currency) ? $currency .' '. $value : $value;
    }
}

/*==-- Success message --==*/
if (!function_exists('successMessage')) {
    function successMessage($message, $redirect = NULL) {
        $msg = 
        '<div class="alert alert-success">'.$message.'</div>';
        if ($redirect != NULL) {
            $msg .= '<script>
                        $("html, body").animate({ scrollTop: 0 }, 600); 
                        setTimeout("window.location=\'' . site_url($redirect) . '\'",1000);
                    </script>';
        } else {
            $msg .= '<script>
                        $("html, body").animate({ scrollTop: 0 }, 600);
                    </script>';
        }
        return $msg;
    }
}

/*==-- Error message --==*/
if (!function_exists('errorMessage')) {
    function errorMessage($message) {
        $msg = 
        '<div class="alert alert-danger">'.$message.'</div>';
        $msg .= '<script>
                    $("html, body").animate({ scrollTop: 0 }, 600);
                </script>';
        return $msg;
    }
}

if (!function_exists('deleteMessage')) {
    function deleteMessage($status, $title) {
        if ($status == "success") {
            echo successMessage($title.' successfully deleted.');
        } elseif ($status == "error") {
            echo errorMessage('Failed to delete '.$title);
        }
    }
}

if (!function_exists('clearForm')) {
    function clearForm() {
        return '<script>$("input,textarea,select").val("");</script>';
    }
}

/*==-- Ajax redirect --==*/
if (!function_exists('ajaxRedirect')) {
    function ajaxRedirect($redirect = '', $timer = 0) {
        if ($timer == 0) {
            return '<script>window.location.href="' . site_url($redirect) . '";</script>';
        } else {
            return '<script>setTimeout("window.location.href=\'' . site_url($redirect) . '\'",' . $timer . ');</script>';
        }
    }
}

if (!function_exists('excerpt')) {
    function excerpt($content, $length = 200, $striptags = FALSE, $delimiter = ''){
		$excerpt = explode('<div style="page-break-after: always;"><span style="display:none">&nbsp;</span></div>', $content, 2);
		if(count($excerpt) > 1){
			$excerpt_fix = ($striptags!=FALSE) ? strip_tags(htmlspecialchars_decode($excerpt[0])) : closetags(htmlspecialchars_decode($excerpt[0]));
		} else {
			$excerpt_fix = ($striptags!=FALSE) ? strip_tags(substr(htmlspecialchars_decode($content), 0, $length)) : closetags(substr(htmlspecialchars_decode($content), 0, $length));
		}
		return ($length < strlen($content)) ? $excerpt_fix.$delimiter : $excerpt_fix;
	}
}

if (!function_exists('date_indo')) {
    function date_indo($fulldate) {
        $date = substr($fulldate, 8, 2);
        $month = get_month(substr($fulldate, 5, 2));
        $year = substr($fulldate, 0, 4);
        return $date . ' ' . $month . ' ' . $year;
    }
}

if (!function_exists('date_simple')) {
    function date_simple($fulldate) {
        $date = substr($fulldate, 8, 2);
        $month = substr($fulldate, 5, 2);
        $year = substr($fulldate, 0, 4);
        return $date . '/' . $month . '/' . $year;
    }
}

if (!function_exists('date_normal')) {
    function normal_date($fulldate) {
        $date = substr($fulldate, 8, 2);
        $month = get_month3(substr($fulldate, 5, 2));
        $year = substr($fulldate, 0, 4);
        return $date . '/' . $month . '/' . $year;
    }
}

if (!function_exists('date_time')) {
    function date_time($fulldate) {
        $date = substr($fulldate, 0, 2);
        $month = get_month2(substr($fulldate, 3, 3));
        $year = substr($fulldate, 7, 4);
        $time = substr($fulldate, 12, 5);
        return $year . '-' . $month . '-' . $date . ' ' . $time;
    }
}

if (!function_exists('date_full')) {
    function date_full($fulldate) {
        $date = substr($fulldate, 8, 2);
        $month = substr($fulldate, 5, 2);
        $year = substr($fulldate, 0, 4);
        $time = substr($fulldate, 11, 8);
        return $date . '/' . $month . '/' . $year . ' ' . $time;
    }
}

if (!function_exists('mysql_date')) {
    function mysql_date($fulldate) {
        $date = substr($fulldate, 0, 2);
        $month = get_month2(substr($fulldate, 3, 3));
        $year = substr($fulldate, 7, 4);
        return $year . '-' . $month . '-' . $date;
    }
}

if (!function_exists('get_month')) {
    function get_month($month) {
        switch ($month) {
            case 1: return "Januari";
            case 2: return "Februari";
            case 3: return "Maret";
            case 4: return "April";
            case 5: return "Mei";
            case 6: return "Juni";
            case 7: return "Juli";
            case 8: return "Agustus";
            case 9: return "September";
            case 10: return "Oktober";
            case 11: return "November";
            case 12: return "Desember";
        }
    }
}

if (!function_exists('get_month2')) {
    function get_month2($month) {
        switch ($month) {
            case "Jan": return "01";
            case "Feb": return "02";
            case "Mar": return "03";
            case "Apr": return "04";
            case "May": return "05";
            case "Jun": return "06";
            case "Jul": return "07";
            case "Aug": return "08";
            case "Sep": return "09";
            case "Oct": return "10";
            case "Nov": return "11";
            case "Dec": return "12";
        }
    }
}

if (!function_exists('get_month3')) {
    function get_month3($month) {
        switch ($month) {
            case "01": return "Jan";
            case "02": return "Feb";
            case "03": return "Mar";
            case "04": return "Apr";
            case "05": return "May";
            case "06": return "Jun";
            case "07": return "Jul";
            case "08": return "Aug";
            case "09": return "Sep";
            case "10": return "Oct";
            case "11": return "Nov";
            case "12": return "Dec";
        }
    }
}

if (!function_exists('get_month4')) {
    function get_month4($month) {
        switch ($month) {
            case "01": return "I";
            case "02": return "II";
            case "03": return "III";
            case "04": return "IV";
            case "05": return "V";
            case "06": return "VI";
            case "07": return "VII";
            case "08": return "VIII";
            case "09": return "IX";
            case "10": return "X";
            case "11": return "XI";
            case "12": return "XII";
        }
    }
}

if (!function_exists('get_month5')) {
    function get_month5($month) {
        switch ($month) {
            case "01": return "Januari";
            case "02": return "Februari";
            case "03": return "Maret";
            case "04": return "April";
            case "05": return "Mei";
            case "06": return "Juni";
            case "07": return "Juli";
            case "08": return "Agustus";
            case "09": return "September";
            case "10": return "Oktober";
            case "11": return "November";
            case "12": return "Desember";
        }
    }
}

if (!function_exists('get_day')) {
    function get_day($day) {
        switch ($day) {
            case "Mon": return "Senin";
            case "Tue": return "Selasa";
            case "Wed": return "Rabu";
            case "Thu": return "Kamis";
            case "Fri": return "Jum'at";
            case "Sat": return "Sabtu";
            case "Sun": return "Minggu";
        }
    }
}

if (!function_exists('get_day2')) {
    function get_day2($day) {
        switch ($day) {
            case 1: return "Mon";
            case 2: return "Tue";
            case 3: return "Wed";
            case 4: return "Thu";
            case 5: return "Fri";
            case 6: return "Sat";
            case 7: return "Sun";
        }
    }
}

if (!function_exists('get_day3')) {
    function get_day3($day) {
        switch ($day) {
            case 1: return "Senin";
            case 2: return "Selasa";
            case 3: return "Rabu";
            case 4: return "Kamis";
            case 5: return "Jum'at";
            case 6: return "Sabtu";
            case 7: return "Minggu";
        }
    }
}


/* End of file common_helper.php */
/* Location: ./system/helpers/common_helper.php */
