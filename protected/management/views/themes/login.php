<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>Login | Website Portal</title>

    <meta name="description" content="User login page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/4.2.0/css/font-awesome.min.css" />

    <!-- text fonts -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/fonts.googleapis.com.css" />

    <!-- ace styles -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ace.min.css" />

    <style type="text/css">
      .light-login { background: #fff; }
      #login-box, #login-box div {
        background: #fff;
        border: 0 solid #fff;
        box-shadow: none;
      }
      #login-box .widget-header {
        text-align: center;
        letter-spacing: 5px;
      }
      #infoMessage {
        padding: 0 36px;
        visibility: hidden;
        opacity: 0;
        transition: .3s;
      }
      #infoMessage.show {
        visibility: visible;
        opacity: 1;        
      }

    </style>
  </head>

  <body class="login-layout light-login">
    <div class="main-container">
      <div class="main-content">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-1">
            <div class="login-container">

              <div class="space-6"></div>
              <div class="space-6"></div>
              <div class="space-6"></div>
              <div class="space-6"></div>
              <div class="space-6"></div>
              <div class="space-6"></div>

              <div class="position-relative">
                <div id="login-box" class="login-box visible widget-box no-border">
                  <div class="widget-header widget-header-large">
                    <h3 class="widget-title">LOGIN</h3>
                  </div>
                  <div class="widget-body">
                    <div class="widget-main">
                      
                      <div class="space-6"></div>
                      
                      <div class="space-6"></div>

                      <form id="form" action="<?php echo site_url('login'); ?>" method="post" autocomplete='off'>
                        <div id="infoMessage"></div>
                        <fieldset>
                          <label class="block clearfix">
                            <span class="block input-icon input-icon-right">
                              <input type="text" name="username" class="form-control" placeholder="Username" />
                              <i class="ace-icon fa fa-user"></i>
                            </span>
                          </label>

                          <label class="block clearfix">
                            <span class="block input-icon input-icon-right">
                              <input type="password" name="password" class="form-control" placeholder="Password" />
                              <i class="ace-icon fa fa-lock"></i>
                            </span>
                          </label>

                          <div class="space"></div>

                          <div class="clearfix">
                            <button type="submit" class="btn btn-sm btn-success btn-block">
                              <span class="bigger-110">Login</span>
                            </button>
                          </div>

                          <div class="space-4"></div>
                        </fieldset>
                      </form>


                      <div class="space-6"></div>

                    </div>

                    <div class="toolbar clearfix">
                    </div>
                  </div>
                </div>


              </div>

            </div>
          </div>
        </div>
      </div>
    </div>

    <script src="<?php echo base_url(); ?>assets/js/jquery.2.1.1.min.js"></script>
    <script type="text/javascript">
      window.jQuery || document.write("<script src='<?php echo base_url(); ?>assets/js/jquery.min.js'>"+"<"+"/script>");
    </script>


    <script type="text/javascript">
      if('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url(); ?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
    </script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.form.min.js"></script>

    <script type="text/javascript">
      jQuery(function($) {
          $('#form').on('submit', function(e) {
            e.preventDefault();

            var form = $(this);
            $('#infoMessage').addClass('show').html('<i class="fa fa-spinner fa-spin"></i> Loading...');
            form.find('input').attr('readonly', true);
            form.find('button').attr('disabled', true);
            form.ajaxSubmit({
              target: '#infoMessage',
              success: function() {
                setTimeout(function() {
                  $('#infoMessage').removeClass('show').html('');
                }, 1500);
                form.find('button').removeAttr('readonly');
                form.find('input').removeAttr('readonly');
              },
              error: function() {}
            })
          })

          $('input').on('blur keyup focus keypress', function() {
            var x = 0;
            console.log($(this).val());
            $('input').each(function(index, value) {
              if($(this).val() != '') {
                x++;
              }
            })
            if(x < 2) {
              $('button[type=submit]').attr('disabled', true);
            } else {
              $('button[type=submit]').removeAttr('disabled');
            }
          })
      });
    </script>
  </body>
</html>
