<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Login | Insan Indonesia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="<?php echo base_url(); ?>assets/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url(); ?>assets/assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url(); ?>assets/assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url(); ?>assets/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="<?php echo base_url(); ?>assets/pages/css/themes/corporate.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
    var base_url = "<?php echo base_url(); ?>";
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/pages/css/windows.chrome.fix.css" />'
    }
    </script>
  </head>
  <body class="fixed-header menu-pin menu-behind">

    <div class="login-wrapper ">

      <div class="bg-pic">
        <img src="<?php echo base_url(); ?>assets/assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" data-src="<?php echo base_url(); ?>assets/assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" data-src-retina="<?php echo base_url(); ?>assets/assets/img/demo/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" alt="" class="lazy">
      </div>

      <div class="login-container bg-white">
        <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
          <img src="<?php echo base_url(); ?>assets/assets/img/logo.png" alt="logo" data-src="<?php echo base_url(); ?>assets/assets/img/logo.png" data-src-retina="<?php echo base_url(); ?>assets/assets/img/logo_2x.png" width="78" height="22">
          <p class="p-t-35">Sign into your pages account</p>
          <!-- START Login Form -->
          <form id="form-login" class="p-t-15" role="form" action="<?php echo site_url('login'); ?>" method="post">
            <div id="infoMessage"></div>
            <!-- START Form Control-->
            <div class="form-group form-group-default">
              <label>User Name</label>
              <div class="controls">
                <input type="text" name="username" placeholder="User Name" class="form-control" required>
              </div>
            </div>
            <!-- END Form Control-->
            <!-- START Form Control-->
            <div class="form-group form-group-default">
              <label>Password</label>
              <div class="controls">
                <input type="password" class="form-control" name="password" placeholder="Password" required>
              </div>
            </div>
            <!-- END Form Control-->
            <button class="btn btn-primary btn-cons m-t-10" type="submit">Login</button>
          </form>
        </div>
      </div>

    </div>

    <!-- BEGIN VENDOR JS -->
    <!-- <script src="<?php echo base_url(); ?>assets/assets/plugins/pace/pace.min.js" type="text/javascript"></script> -->
    <script src="<?php echo base_url(); ?>assets/assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/tether/js/tether.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets/plugins/select2/js/select2.full.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets/plugins/classie/classie.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/jquery.form.min.js" type="text/javascript"></script>
    <!-- END VENDOR JS -->
    <script src="<?php echo base_url(); ?>assets/pages/js/pages.min.js"></script>
    <script>
    $(function()
    {
      $('#form-login').on('submit', function(e) {
        var form = $(this);
        form.find('input').attr('readonly', true);
        form.find('button').attr('disabled', true);
        $('.pgn-wrapper').remove();

        form.ajaxSubmit({
          target: '#infoMessage',
          success: function() {
            form.find('input').removeAttr('readonly');
            form.find('button').removeAttr('disabled');
          },
          error: function(data) {
            form.find('input').removeAttr('readonly');
            form.find('button').removeAttr('disabled');
            $('.login-container').pgNotification({
                style: 'bar',
                message: data,
                position: 'top',
                timeout: 0,
                type: 'error'
            }).show();
          }
        })

        e.preventDefault();
      })
    })
    </script>
  </body>
</html>