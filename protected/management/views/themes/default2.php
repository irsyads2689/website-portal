<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title><?php echo $title; ?> | Insan Indonesia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="<?php echo base_url(); ?>assets/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url(); ?>assets/assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url(); ?>assets/assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url(); ?>assets/assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/assets/plugins/datatables-responsive/css/datatables.responsive.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url(); ?>assets/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="<?php echo base_url(); ?>assets/pages/css/themes/corporate.css" rel="stylesheet" type="text/css" />

    <style type="text/css">
        #loader-container {
            display: block;
            position: fixed;
            background-color: rgba(0,0,0,.3);
            z-index: 9999;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
        }
        #loader-container > div {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            z-index: 10000;
            width: 75px;
            height: 75px;
        }
    </style>

    <script type="text/javascript">
    var base_url = "<?php echo base_url(); ?>";
    </script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
<body class="fixed-header menu-pin menu-behind">
    <div id="loader-container" class="hidden"><div class="progress-circle-indeterminate"></div></div>

    <?php $this->load->view('themes/default/sidebar'); ?>

    <div class="page-container ">
        <div class="header ">
            <!-- START MOBILE SIDEBAR TOGGLE -->
            <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu" data-toggle="sidebar"></a>
            <!-- END MOBILE SIDEBAR TOGGLE -->
            <div class="">
                <div class="brand inline  m-l-10 ">
                    <img src="<?php echo base_url(); ?>assets/assets/img/logo.png" alt="logo" data-src="<?php echo base_url(); ?>assets/assets/img/logo.png" data-src-retina="<?php echo base_url(); ?>assets/assets/img/logo_2x.png" width="78" height="22">
                </div>
            </div>
            <div class="d-flex align-items-center">
              <!-- START User Info-->
                <div class="pull-left p-r-10 fs-14 font-heading hidden-md-down">
                    <span class="semi-bold">David</span> <span class="text-master">Nest</span>
                </div>
                <div class="dropdown pull-right hidden-md-down">
                    <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="thumbnail-wrapper d32 circular inline">
                        <img src="<?php echo base_url(); ?>assets/assets/img/profiles/avatar.jpg" alt="" data-src="<?php echo base_url(); ?>assets/assets/img/profiles/avatar.jpg" data-src-retina="<?php echo base_url(); ?>assets/assets/img/profiles/avatar_small2x.jpg" width="32" height="32">
                      </span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                        <a href="#" class="dropdown-item"><i class="pg-settings_small"></i> Settings</a>
                        <a href="#" class="dropdown-item"><i class="pg-outdent"></i> Feedback</a>
                        <a href="#" class="dropdown-item"><i class="pg-signals"></i> Help</a>
                        <a href="#" class="clearfix bg-master-lighter dropdown-item">
                            <span class="pull-left">Logout</span>
                            <span class="pull-right"><i class="pg-power"></i></span>
                        </a>
                    </div>
                </div>
                <!-- END User Info-->
                <a href="#" class="header-icon pg pg-alt_menu btn-link m-l-10 sm-no-margin d-inline-block" data-toggle="quickview" data-toggle-element="#quickview"></a>
            </div>                      
        </div>
        <div class="page-content-wrapper ">
            <div class="content " id="content">
                <?php echo $output; ?>
              
            </div>

            <div class=" container-fluid  container-fixed-lg footer">
                <div class="copyright sm-text-center">
                    <p class="small no-margin pull-left sm-pull-reset">
                        <span class="hint-text">Copyright &copy; <?php echo date('Y'); ?> </span>
                        <span class="font-montserrat bold">Yayasan Insan Indonesia</span>.
                        <span class="hint-text">All rights reserved. </span>
                    </p>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- BEGIN VENDOR JS -->
    <!-- <script src="<?php echo base_url(); ?>assets/assets/plugins/pace/pace.min.js" type="text/javascript"></script> -->
    <script src="<?php echo base_url(); ?>assets/assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/tether/js/tether.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets/plugins/select2/js/select2.full.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets/plugins/classie/classie.js"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets/plugins/datatables-responsive/js/datatables.responsive.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets/plugins/datatables-responsive/js/lodash.min.js"></script></head>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/assets/plugins/jquery.form.min.js"></script></head>
    <script src="<?php echo base_url(); ?>assets/assets/js/grid.js" type="text/javascript"></script>

    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="<?php echo base_url(); ?>assets/pages/js/pages.min.js"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="<?php echo base_url(); ?>assets/assets/js/scripts.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS -->
    <script type="text/javascript">
        $(document).ready(function() {
            $('body').on('click', '.ajaxmenu', function(event) {
                event.preventDefault();
                var url = $(this).attr('href');
                var title = $(this).attr('data-title');
                $('#loader-container').removeClass('hidden');
                $('#content').load(url, function() {
                    $('#loader-container').addClass('hidden');
                });
                document.title = title + ' | Insan Indonesia';
                window.history.pushState("", title, url);
                $(this).parent().siblings().removeClass('active');
                $(this).parent().siblings().removeClass('open');
                $(this).parent().find('i.icon-thumbnail').removeClass('bg-success');
                $(this).parent().siblings().find('ul').css({display:'none'});
                $(this).parent().siblings().find('li').removeClass('active');
                $(this).parent().addClass('active');
                $(this).parent().find('i.icon-thumbnail').addClass('bg-success');
                $(this).parent().parent().parent().siblings().removeClass('active');
                $(this).parent().parent().parent().siblings().removeClass('open');
                $(this).parent().parent().parent().find('i.icon-thumbnail').removeClass('bg-success');
                $(this).parent().parent().parent().siblings().find('li').removeClass('active');
                $(this).parent().parent().parent().addClass('open active');
                $(this).parent().parent().parent().find('i.icon-thumbnail').addClass('bg-success');
            });
            $('body').on('click', '.ajaxcontent', function(event) {
                event.preventDefault();
                var url = $(this).attr('href');
                var title = $(this).attr('data-title');
                $('#loader-container').removeClass('hidden');
                $('#content').load(url, function() {
                    $('#loader-container').addClass('hidden');
                });
                document.title = title + ' | Insan Indonesia';
                window.history.pushState("", title, url);
            });
            $('#form').on('submit', function(e) {
                e.preventDefault();

                var form = $(this);
                form.find('input, select, textarea').attr('readonly', true);
                form.find('button').attr('disabled', true);
                form.ajaxSubmit({
                    target: "#infoMessage",
                    success: function() {
                        form.find('input, select, textarea').removeAttr('readonly');
                        form.find('button').removeAttr('disabled');
                    },
                    error: function(data) {
                        form.find('input, select, textarea').removeAttr('readonly');
                        form.find('button').removeAttr('disabled');
                        $('.login-container').pgNotification({
                            style: 'bar',
                            message: data,
                            position: 'top',
                            timeout: 0,
                            type: 'error'
                        }).show();
                    }
                })
            })
        })
    </script>
  </body>
</html>