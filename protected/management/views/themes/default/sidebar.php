<div id="sidebar" class="sidebar responsive sidebar-fixed sidebar-scroll">
    <ul class="nav nav-list" id="menu">
        <li class="<?php echo $menu['menu'] == 'dashboard' ? 'open active' : ''; ?>">
            <a href="<?php echo site_url(); ?>">
                <i class="menu-icon fa fa-tachometer"></i>
                <span class="menu-text"> Dashboard </span>
            </a>
            <b class="arrow"></b>
        </li>
		
		<li class="<?php echo $menu['menu'] == 'profile' ? 'active' : ''; ?>">
            <a href="<?php echo site_url('profile'); ?>" class="ajaxan" title="Profil">
                <i class="menu-icon fa fa-file-o"></i>
                <span class="menu-text"> Profil</span>
            </a>
            <b class="arrow"></b>
        </li>

		<li class="<?php echo $menu['menu'] == 'program' ? 'active' : ''; ?>">
            <a href="<?php echo site_url('program'); ?>" class="ajaxan" title="Program">
                <i class="menu-icon fa fa-file-o"></i>
                <span class="menu-text"> Program</span>
            </a>
            <b class="arrow"></b>
        </li>

		<li class="<?php echo $menu['submenu'] == 'news' ? 'active' : ''; ?>">
            <a href="<?php echo site_url('post/news'); ?>" class="ajaxan" title="Berita">
                <i class="menu-icon fa fa-file-o"></i>
                <span class="menu-text"> Berita</span>
            </a>
            <b class="arrow"></b>
        </li>

		<li class="<?php echo $menu['submenu'] == 'article' ? 'active' : ''; ?>">
            <a href="<?php echo site_url('post/article'); ?>" class="ajaxan" title="Artikel">
                <i class="menu-icon fa fa-file-o"></i>
                <span class="menu-text"> Artikel</span>
            </a>
            <b class="arrow"></b>
        </li>

        <?php /*
		<li class="<?php echo $menu['menu'] == 'post' ? 'active open' : ''; ?>">
			<a href="" class="dropdown-toggle">
				<i class="menu-icon fa fa-tags"></i>
				<span class="menu-text"> Informasi </span>

				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>
			<ul class="submenu">
				<li class="<?php echo $menu['submenu'] == 'news' ? 'active' : ''; ?>">
					<a href="<?php echo site_url('post/news'); ?>" class="ajaxan" title="Berita">
						<i class="menu-icon fa fa-caret-right"></i>
						Berita
					</a>

					<b class="arrow"></b>
				</li>
				<li class="<?php echo $menu['submenu'] == 'article' ? 'active' : ''; ?>">
					<a href="<?php echo site_url('post/article'); ?>" class="ajaxan" title="Artikel">
						<i class="menu-icon fa fa-caret-right"></i>
						Artikel
					</a>

					<b class="arrow"></b>
				</li>
			</ul>
		</li>
		*/ ?>

		<li class="<?php echo $menu['menu'] == 'gallery' ? 'active' : ''; ?>">
            <a href="<?php echo site_url('gallery/image'); ?>" class="ajaxan" title="Galeri">
                <i class="menu-icon fa fa-file-o"></i>
                <span class="menu-text"> Galeri</span>
            </a>
            <b class="arrow"></b>
        </li>

		<li class="<?php echo $menu['menu'] == 'setting' ? 'active open' : ''; ?>">
			<a href="" class="dropdown-toggle">
				<i class="menu-icon fa fa-tags"></i>
				<span class="menu-text"> Pengaturan </span>

				<b class="arrow fa fa-angle-down"></b>
			</a>

			<b class="arrow"></b>

			<ul class="submenu">
				<li class="<?php echo $menu['submenu'] == 'general' ? 'active' : ''; ?>">
					<a href="<?php echo site_url('setting'); ?>" class="ajaxan" title="Pengaturan Umum">
						<i class="menu-icon fa fa-caret-right"></i>
						General
					</a>

					<b class="arrow"></b>
				</li>

				<li class="<?php echo $menu['submenu'] == 'seo' ? 'active' : ''; ?>">
					<a href="<?php echo site_url('setting/seo'); ?>" class="ajaxan" title="Pengaturan SEO">
						<i class="menu-icon fa fa-caret-right"></i>
						Pengaturan SEO
					</a>

					<b class="arrow"></b>
				</li>
			</ul>
		</li>
    </ul>
</div>
