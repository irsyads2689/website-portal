<nav class="page-sidebar" data-pages="sidebar">
	<div class="sidebar-header">
    	<img src="<?php echo base_url(); ?>assets/assets/img/logo_white.png" alt="logo" class="brand" data-src="<?php echo base_url(); ?>assets/assets/img/logo_white.png" data-src-retina="<?php echo base_url(); ?>assets/assets/img/logo_white_2x.png" width="78" height="22">
  	</div>
  	<div class="sidebar-menu">
	    <ul class="menu-items">
	      	<li class="m-t-30 <?php echo $menu['menu'] == 'dashboard' ? 'open active' : ''; ?>">
		        <a href="<?php echo site_url(); ?>" class="ajaxmenu" data-title="Dashboard"><span class="title">Dashboard</span></a>
		        <span class="icon-thumbnail  <?php echo $menu['menu'] == 'dashboard' ? 'bg-success' : ''; ?>"><i class="pg-home"></i></span>
	      	</li>
	      	<li class="<?php echo $menu['menu'] == 'profile' ? 'open active' : ''; ?>">
		        <a href="<?php echo site_url('profile'); ?>" class="ajaxmenu" data-title="Profil"><span class="title">Profil</span></a>
		        <span class="icon-thumbnail  <?php echo $menu['menu'] == 'profile' ? 'bg-success' : ''; ?>"><i class="pg-home"></i></span>
	      	</li>
	      	<li class=" <?php echo $menu['menu'] == 'profile' ? 'open active' : ''; ?>">
		        <a href="javascript:;">
		        	<span class="title">Profil</span>
		        	<span class="arrow <?php echo $menu['menu'] == 'profile' ? 'open active' : ''; ?>"></span>
		        </a>
		        <span class="icon-thumbnail <?php echo $menu['menu'] == 'profile' ? 'bg-success' : ''; ?>"><i class="pg-calender"></i></span>
		        <ul class="sub-menu">
		          	<li class=" <?php echo $menu['submenu'] == 'profile' ? 'active' : ''; ?>">
			            <a href="<?php echo site_url('profile'); ?>" class="ajaxmenu" data-title="Profil">Semua Profil</a>
			            <span class="icon-thumbnail <?php echo $menu['menu'] == 'profile' ? 'bg-success' : ''; ?>">p</span>
		         	</li>
		          	<li class=" <?php echo $menu['submenu'] == 'profile-create' ? 'active' : ''; ?>">
			            <a href="<?php echo site_url('profile/create'); ?>" class="ajaxmenu" data-title="Tambah Profil">Tambah Baru</a>
			            <span class="icon-thumbnail <?php echo $menu['menu'] == 'profile-create' ? 'bg-success' : ''; ?>">t</span>
		          	</li>
		        </ul>
		    </li>
	    </ul>
	    <div class="clearfix"></div>
  	</div>
</nav>
