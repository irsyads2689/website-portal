<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta charset="utf-8" />
        <title><?php echo $title; ?> | Insan Indonesia</title>

        <meta name="description" content="overview &amp; stats" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

        <!-- bootstrap & fontawesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/4.2.0/css/font-awesome.min.css" />

        <!-- page specific plugin styles -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.min.css" />
        <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.custom.min.css" /> -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.gritter.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/datepicker.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/colorpicker.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-fileinput.css" />

        <!-- text fonts -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/fonts.googleapis.com.css" />

        <!-- ace styles -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ace-skins.min.css" class="ace-main-stylesheet" id="ace-skins-stylesheet" />

		<script type="text/javascript">
            var base_url = "<?php echo base_url(); ?>";
        </script>

        <!-- ace settings handler -->
        <script src="<?php echo base_url(); ?>assets/js/ace-extra.min.js"></script>

        <!-- basic scripts -->
        <!--[if !IE]> -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.2.1.1.min.js"></script>

        <!-- <![endif]-->

        <!--[if !IE]> -->
        <script type="text/javascript">
            window.jQuery || document.write("<script src='<?php echo base_url(); ?>assets/js/jquery.min.js'>"+"<"+"/script>");
        </script>

        <!-- <![endif]-->

        <script type="text/javascript">
            if('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url(); ?>assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
        </script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

        <!-- page specific plugin scripts -->
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
        <!-- <script src="<?php echo base_url(); ?>assets/js/jquery-ui.custom.min.js"></script> -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.ui.touch-punch.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
        <!-- <script src="<?php echo base_url(); ?>assets/js/dataTables.tableTools.min.js"></script> -->
        <!-- <script src="<?php echo base_url(); ?>assets/js/dataTables.colVis.min.js"></script> -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.form.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap-fileinput.js"></script>
        <script src="<?php echo base_url(); ?>assets/tinymce/tinymce.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/tinymce/jquery.tinymce.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.maskedinput.min.js"></script>

        <!-- ace scripts -->
        <script src="<?php echo base_url(); ?>assets/js/ace-elements.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/ace.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/app.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/grid.js"></script>

    </head>

    <body class="skin-1">
        <div id="navbar" class="navbar navbar-default navbar-fixed-top">
            <div class="navbar-container" id="navbar-container">
                <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
                    <span class="sr-only">Toggle sidebar</span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>
                </button>

                <div class="navbar-header pull-left">
                    <a href="<?php echo site_url(); ?>" class="navbar-brand">
                        <small>
                            Insan Indonesia
                        </small>
                    </a>
                </div>

                <div class="navbar-buttons navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <li class="green dropdown-modal">
							<a class="dropdown-toggle" href="<?php echo str_replace('/adminpanel','', base_url()); ?>" aria-expanded="false" target="_blank">
							 Lihat Website
							</a>
						</li>
                        <li class="light-blue">
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                <img class="nav-user-photo" src="<?php echo base_url(); ?>assets/avatars/avatar2.png" alt="Jason's Photo" />
                                
                                <i class="ace-icon fa fa-caret-down"></i>
                            </a>

                            <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                <li>
                                    <a href="<?php echo site_url('user/profile'); ?>" class="ajaxan" title="Profil">
                                        <i class="ace-icon fa fa-user"></i>
                                        Profil
                                    </a>
                                </li>

                                <li>
                                    <a href="<?php echo site_url('user/change_password'); ?>" class="ajaxan" title="Ubah Kata Sandi">
                                        <i class="ace-icon fa fa-key"></i>
                                        Ubah Kata Sandi
                                    </a>
                                </li>

                                <li class="divider"></li>

                                <li>
                                    <a href="<?php echo site_url('auth/logout'); ?>">
                                        <i class="ace-icon fa fa-power-off"></i>
                                        Keluar
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="main-container" id="main-container">

            <?php echo $this->load->view('themes/default/sidebar'); ?>

            <div id="content" class="main-content">
            <?php echo $output; ?>
            </div>

            <?php /*
            <div class="footer">
                <div class="footer-inner">
                    <div class="footer-content">
                        <span class="bigger-120">
                            <span class="blue bolder">Ace</span>
                            Application &copy; 2013-2014
                        </span>

                        &nbsp; &nbsp;
                        <span class="action-buttons">
                            <a href="#">
                                <i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
                            </a>

                            <a href="#">
                                <i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
                            </a>

                            <a href="#">
                                <i class="ace-icon fa fa-rss-square orange bigger-150"></i>
                            </a>
                        </span>
                    </div>
                </div>
            </div>
            */ ?>

            <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
            </a>
        </div>

        <div id="loader"><div class="view-loading show-loading"><div class="loading-element--circle"></div></div></div>

        <!-- inline scripts related to this page -->
        <script type="text/javascript">

        </script>
    </body>
</html>
