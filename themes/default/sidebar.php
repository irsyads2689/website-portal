<aside id="aside" class="col-md-3">
    <div class="widget">
        <h3 class="widget-title">Program</h3>
        <div class="widget-category">
            <?php
            $programs = $this->handle_data->header('all_program', 'program_model', 'all_program'); 
            if($programs) {
                echo '<ul>';
                foreach($programs as $row) {
                    echo '<li><a href="'.site_url('program/'.$row->slug).'">'.$row->title.'</a></li>';
                }
                echo '</ul>';
            }
            ?>
        </div>
    </div>

    <div class="widget">
        <h3 class="widget-title">Berita Terbaru</h3>
        <?php 
        $latest_news = $this->handle_data->sidebar('latest_news', 'post_model', 'latest_news');
        if($latest_news) {
            foreach($latest_news as $row) {
                $title  = $row->title;
                $link   = site_url($row->type.'/'.$row->slug);
                $image  = get_image($row->image_folder, $row->image);
                $date   = date_indo($row->created_at);
                echo '
                    <div class="widget-post">
                        <a href="'.$link.'">
                            <div class="widget-img">
                                <img src="'.$image.'" alt="'.$title.'">
                            </div>
                            <div class="widget-content">'.$title.'</div>
                        </a>
                        <ul class="article-meta">
                            <li>'.$date.'</li>
                        </ul>
                    </div>';
            }
        }
        ?>
    </div>

</aside>
