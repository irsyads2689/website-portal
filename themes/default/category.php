<?php 
$theme_url = get_theme_url(); 
?>

<div class="main-container container pt-24" id="main-container">
	<div class="container">
      	<ul class="breadcrumbs">
        	<li class="breadcrumbs__item">
          		<a href="<?php echo site_url(); ?>" class="breadcrumbs__url">Home</a>
        	</li>
        	<li class="breadcrumbs__item breadcrumbs__item--current">
          		<?php echo $category->name; ?>
        	</li>
      	</ul>
    </div>

    <div class="main-container container" id="main-container">
    	<div class="row">
    		<div class="col-lg-8 blog__content mb-16">
          		<h1 class="page-title"><?php echo $category->name; ?></h1>
          		<div class="row card-row" id="article-category" data-next_page="<?php echo $next_page; ?>" data-total_page="<?php echo $total_page; ?>" data-try="<?php echo $try; ?>">
          			<?php 
          			if($articles) {
          				foreach($articles as $row) {
          					$post_url 		= site_url($row->category_slug.'/'.$row->slug);
          					$post_title 	= $row->title;
          					$post_image 	= get_image('posts', $row->image);
          					// $category_url 	= site_url($row->category_slug);
          					// $category_title = $row->category;
          					echo '
					            <div class="col-md-6">
					              	<article class="entry card">
						                <div class="entry__img-holder card__img-holder">
						                  	<a href="'.$post_url.'">
						                    	<div class="thumb-container thumb-70">
						                      		<img data-src="'.$post_image.'" src="'.$post_image.'" class="entry__img lazyload" alt="'.$post_title.'" />
						                    	</div>
						                  	</a>
						                  	<!-- <a href="#" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--violet">world</a> -->
						                </div>

					                	<div class="entry__body card__body">
					                  		<div class="entry__header">
							                    <h2 class="entry__title">
							                      	<a href="'.$post_url.'">'.$post_title.'</a>
							                    </h2>
					                  		</div>
					                	</div>
					              	</article>
					            </div>
          					';
          				} 
      				} 
      				?>
		        </div>
                <div style="position: relative; display: none;" id="loader-scroll" class="pt-40 pb-40 hidden">
                    <div class="loader" style="display: block"><div></div></div>
                </div>

	       	</div>


	        <?php $this->load->view('default/sidebar'); ?>

	    </div>
	</div>
</div>