<?php 
$theme_url = get_theme_url(); 
$web = get_option();
$theme              = isset($web['theme']) ? $web['theme'] : 'default';
$site_description   = isset($web['site_description']) ? $web['site_description'] : '';
$site_address       = isset($web['site_address']) ? $web['site_address'] : '';
$site_email         = isset($web['site_email']) ? $web['site_email'] : '';
$site_phone         = isset($web['site_phone']) ? $web['site_phone'] : '';
$facebook           = isset($web['facebook']) ? $web['facebook'] : '';
$twitter            = isset($web['twitter']) ? $web['twitter'] : '';
$instagram          = isset($web['instagram']) ? $web['instagram'] : '';
$youtube            = isset($web['youtube']) ? $web['youtube'] : '';
?>
    <footer id="footer" class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="footer">
                        <div class="footer-logo">
                            <a class="logo" href="#"><img src="<?php echo base_url('themes/'.$theme.'/assets/img/logo.jpeg'); ?>" alt=""></a>
                        </div>
                        <p><?php echo $site_description; ?></p>
                        <ul class="footer-contact">
                            <li><i class="fa fa-map-marker"></i> <?php echo $site_address; ?></li>
                            <li><i class="fa fa-phone"></i> <?php echo $site_phone; ?></li>
                            <li><i class="fa fa-envelope"></i> <a href="#"><?php echo $site_email; ?></a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="footer">
                        <h3 class="footer-title">Galeri</h3>
                        <ul class="footer-galery">
                            <li><a href="#"><img src="<?php echo $theme_url; ?>/assets/img/gallery1.jpg" alt=""></a></li>
                            <li><a href="#"><img src="<?php echo $theme_url; ?>/assets/img/gallery2.jpg" alt=""></a></li>
                            <li><a href="#"><img src="<?php echo $theme_url; ?>/assets/img/gallery3.jpg" alt=""></a></li>
                            <li><a href="#"><img src="<?php echo $theme_url; ?>/assets/img/gallery4.jpg" alt=""></a></li>
                            <li><a href="#"><img src="<?php echo $theme_url; ?>/assets/img/gallery5.jpg" alt=""></a></li>
                            <li><a href="#"><img src="<?php echo $theme_url; ?>/assets/img/gallery6.jpg" alt=""></a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="footer">
                        <h3 class="footer-title">Media Sosial</h3>
                        <ul class="footer-social">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div id="footer-bottom" class="row">
                <div class="col-md-12">
                    <div class="footer-copyright">
                        <span>Copyright &copy; <?php echo date('Y').' '.get_option('site_name');?>. All rights reserved</span>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery Plugins -->
    <script src="<?php echo $theme_url; ?>/assets/js/jquery.min.js"></script>
    <script src="<?php echo $theme_url; ?>/assets/js/bootstrap.min.js"></script>
    <script src="<?php echo $theme_url; ?>/assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo $theme_url; ?>/assets/js/jquery.stellar.min.js"></script>
    <script src="<?php echo $theme_url; ?>/assets/js/main.js"></script>

</body>

</html>
