    <div id="page-header">
    </div>
</header>

<div class="section">
    <div class="container">
        <div class="row">
            <main id="main" class="col-md-9">
                <div class="article">
                    <?php 
                    if($post->image!='') {
                        echo '
                            <div class="article-img">
                                <img src="'.get_image($post->image_folder, $post->image).'" alt="">
                            </div>';
                    }
                    ?>

                    <div class="article-content">
                         <h2 class="article-title"> <?php echo $post->title; ?></h2>
                        <!-- /article title -->

                        <!-- article meta -->
                        <ul class="article-meta">
                            <li> <?php echo date_indo($post->created_at); ?></li>
                        </ul>
                       <?php echo $post->content; ?>
                    </div>
                </div>
            </main>

            <?php $this->load->view('default/sidebar'); ?>
        </div>
    </div>
</div>


