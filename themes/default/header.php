<?php 
$theme_url = get_theme_url(); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<title><?php echo $title; ?></title>

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400%7CSource+Sans+Pro:700" rel="stylesheet">

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="<?php echo $theme_url; ?>/assets/css/bootstrap.min.css" />

	<!-- Owl Carousel -->
	<link type="text/css" rel="stylesheet" href="<?php echo $theme_url; ?>/assets/css/owl.carousel.css" />
	<link type="text/css" rel="stylesheet" href="<?php echo $theme_url; ?>/assets/css/owl.theme.default.css" />

	<!-- Font Awesome Icon -->
	<link rel="stylesheet" href="<?php echo $theme_url; ?>/assets/css/font-awesome.min.css">

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="<?php echo $theme_url; ?>/assets/css/style.css" />

</head>

<body>
	<header>
		<nav id="main-navbar">
		  	<div class="container">
			    <div class="navbar-header">
			      	<div class="navbar-brand">
				        <a class="logo" href="<?php echo site_url(); ?>"><img src="<?php echo $theme_url; ?>/assets/img/logo.jpeg" alt="logo"></a>
			      	</div>

			      	<button class="navbar-toggle-btn">
			          	<i class="fa fa-bars"></i>
			        </button>

			    </div>

			    <ul class="navbar-menu nav navbar-nav navbar-right">
					<li><a href="<?php echo site_url(); ?>">Home</a></li>
					<li class="has-dropdown"><a href="#">Tentang Kami</a>
						<ul class="dropdown">
							<?php 
							$programs = $this->handle_data->header('all_about', 'post_model', 'all_about');
							if($programs) {
								foreach($programs as $row) {
									echo '<li><a href="'.site_url($row->type.'/'.$row->slug).'">'.$row->title.'</a></li>';
								}
							}
							?>
						</ul>
					</li>
					<li class="has-dropdown"><a href="#">Program</a>
						<ul class="dropdown">
							<?php 
							$programs = $this->handle_data->header('all_program', 'program_model', 'all_program');
							if($programs) {
								foreach($programs as $row) {
									echo '<li><a href="'.site_url('program/'.$row->slug).'">'.$row->title.'</a></li>';
								}
							}
							?>
						</ul>
					</li>
					<!-- <li><a href="<?php echo site_url('hitung-zakat'); ?>">Hitung Zakat</a></li> -->
					<li class="has-dropdown"><a href="#">Informasi</a>
						<ul class="dropdown">
							<li><a href="<?php echo site_url('berita'); ?>">Berita</a></li>
							<li><a href="<?php echo site_url('artikel'); ?>">Artikel</a></li>
						</ul>
					</li>
					<li><a href="<?php echo site_url('galeri'); ?>">Galeri</a></li>
					<li><a href="<?php echo site_url('kontak-kami'); ?>">Kontak Kami</a></li>
			    </ul>
		  	</div>
		</nav>
