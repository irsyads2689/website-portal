<?php 
$theme_url = get_theme_url(); 
?>
    <div id="page-header">
        <div class="section-bg"></div>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="header-content">
                        <h1><?php echo $gallery->title; ?></h1>
                        <ul class="breadcrumb">
                            <li class="active"><?php echo date_indo($gallery->created_at); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="section">
    <div class="container">
        <div class="row">
            <main id="main" class="col-md-9">
                <?php 
                if($list_gallery) {
                    foreach($list_gallery as $row) {
                        $image = get_image($row->image_folder, $row->image);
                        echo '
                            <div class="article">
                                <div class="article-img">
                                    <img src="'.$image.'">
                                </div>
                            </div>';
                    }
                }
                ?>
            </main>

            <?php $this->load->view('default/sidebar'); ?>
        </div>
    </div>
</div>


