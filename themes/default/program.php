<?php 
$theme_url = get_theme_url(); 
?>
    <div id="page-header">
        <div class="section-bg"></div>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="header-content">
                        <h1><?php echo $program->title; ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="section">
    <div class="container">
        <div class="row">
            <main id="main" class="col-md-9">
                <?php 
                if($list_program) {
                    $x = 0;
                    foreach($list_program as $row) {
                        $x++;
                        $title = $row->title;
                        $image = get_image($row->image_folder, $row->image);
                        $content = $row->content;
                        $_class_col = $x%2==0 ? 'two' : 'first';
                        echo '
                            <div class="article '.$_class_col.'">
                                <div class="article-img">
                                    <img src="'.$image.'" alt="'.$title.'">
                                </div>

                                <div class="article-content">
                                    <h2 class="article-title">'.$title.'</h2>
                                    '.$content.'
                                </div>
                            </div>';
                    }
                }
                ?>
            </main>

            <?php $this->load->view('default/sidebar'); ?>
        </div>
    </div>
</div>


