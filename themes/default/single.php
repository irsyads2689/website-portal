<?php 
$theme_url = get_theme_url();

$post_title   = $post->title;
$post_content = $post->content;
$post_image   = get_image('posts', $post->image);
$post_date    = date('d/m/Y', strtotime($post->created_at));
$category_url = site_url($post->category_slug);
$category     = $post->category;
?>

    <div class="container">
        <ul class="breadcrumbs">
            <li class="breadcrumbs__item">
                <a href="index.html" class="breadcrumbs__url">Home</a>
            </li>
            <li class="breadcrumbs__item">
                <a href="<?php echo $category_url; ?>" class="breadcrumbs__url"><?php echo $category; ?></a>
            </li>
            <li class="breadcrumbs__item breadcrumbs__item--current">
                <?php echo $post_title; ?>
            </li>
        </ul>
    </div>

    <div class="main-container container" id="main-container">
        <div class="row">
            <div class="col-lg-8 blog__content mb-40">
                <div class="content-box">           
                    <article class="entry mb-0">
                        <div class="single-post__entry-header entry__header">
                            <a href="javascript:;" class="entry__meta-category entry__meta-category--label entry__meta-category--green">
                                <?php echo $category; ?>
                            </a>
                            <h1 class="single-post__entry-title">
                                <?php echo $post_title; ?>
                            </h1>

                            <div class="entry__meta-holder">
                                <ul class="entry__meta">
                                    <li class="entry__meta-date">
                                        <?php echo $post_date; ?>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="entry__img-holder">
                            <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>" class="entry__img">
                        </div>

                        <div class="entry__article-wrap">
                            <div class="entry__share">
                                <div class="sticky-col">
                                    <div class="socials socials--rounded socials--large">
                                        <a class="social social-facebook" href="#" title="facebook" target="_blank" aria-label="facebook">
                                            <i class="ui-facebook"></i>
                                        </a>
                                        <a class="social social-twitter" href="#" title="twitter" target="_blank" aria-label="twitter">
                                            <i class="ui-twitter"></i>
                                        </a>
                                        <a class="social social-google-plus" href="#" title="google" target="_blank" aria-label="google">
                                            <i class="ui-google"></i>
                                        </a>
                                        <a class="social social-pinterest" href="#" title="pinterest" target="_blank" aria-label="pinterest">
                                            <i class="ui-pinterest"></i>
                                        </a>
                                    </div>
                                </div>                  
                            </div>

                            <div class="entry__article">
                                <?php echo $post_content; ?>
                            </div>
                        </div> 


                        <nav class="entry-navigation">
                            <div class="clearfix">
                                <?php if($post_prev) { ?>
                                <div class="entry-navigation--left">
                                    <i class="ui-arrow-left"></i>
                                    <span class="entry-navigation__label">Artikel Sebelumnya</span>
                                    <div class="entry-navigation__link">
                                        <a href="<?php echo site_url($post_prev->category_slug.'/'.$post_prev->slug); ?>" rel="next"><?php echo $post_prev->title; ?></a>
                                    </div>
                                </div>
                                <?php } ?>
                                <?php if($post_next) { ?>
                                <div class="entry-navigation--right">
                                    <span class="entry-navigation__label">Artikel Selanjutnya</span>
                                    <i class="ui-arrow-right"></i>
                                    <div class="entry-navigation__link">
                                        <a href="<?php echo site_url($post_next->category_slug.'/'.$post_next->slug); ?>" rel="prev"><?php echo $post_next->title; ?></a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </nav>

                    

                        <section class="section related-posts mt-40 mb-0">
                            <div class="title-wrap title-wrap--line title-wrap--pr">
                                <h3 class="section-title">Artikel Terkait</h3>
                            </div>

                            <div id="owl-posts-3-items" class="owl-carousel owl-theme owl-carousel--arrows-outside">
                                <article class="entry thumb thumb--size-1">
                                  <div class="entry__img-holder thumb__img-holder" style="background-image: url('img/content/carousel/carousel_post_1.jpg');">
                                    <div class="bottom-gradient"></div>
                                    <div class="thumb-text-holder">   
                                      <h2 class="thumb-entry-title">
                                        <a href="single-post.html">9 Things to Consider Before Accepting a New Job</a>
                                      </h2>
                                    </div>
                                    <a href="single-post.html" class="thumb-url"></a>
                                  </div>
                                </article>
                            </div>

                        </section>
                    </article>

                </div>
            </div>
        
            <?php $this->load->view('default/sidebar'); ?>
        </div>
    </div>
