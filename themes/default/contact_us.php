<?php 
$theme_url = get_theme_url(); 
?>
    <div id="page-header">
        <div class="section-bg"></div>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="header-content">
                        <h1>Kontak Kami</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="section">
    <div class="container">
        <div class="row">
            <main id="main" class="col-md-9">
                
            </main>

            <?php $this->load->view('default/sidebar'); ?>
        </div>
    </div>
</div>


