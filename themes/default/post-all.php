<?php 
$theme_url = get_theme_url(); 
?>
    <div id="page-header">
        <div class="section-bg"></div>

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="header-content">
                        <h1><?php echo str_first_upper($title); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="section">
    <div class="container">
        <div class="row">
            <main id="main" class="col-md-9">
                <div class="row">
                <?php 
                if($posts) {
                    foreach($posts as $key => $row) {
                        $title = $row->title;
                        $link = site_url($row->type.'/'.$row->slug);
                        $image = get_image($row->image_folder, $row->image);
                        $date = date_indo($row->created_at);
                        echo '
                            <div class="col-md-6">
                                <div class="article">
                                    <div class="article-img">
                                        <a href="'.$link.'">
                                            <img src="'.$image.'" alt="'.$title.'">
                                        </a>
                                    </div>
                                    <div class="article-content">
                                        <h3 class="article-title"><a href="'.$link.'">'.$title.'</a></h3>
                                        <ul class="article-meta">
                                            <li>'.$date.'</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>';
                        if($key%2!=0) {
                            echo '<div class="clearfix visible-md visible-lg"></div>';
                        }
                    }
                }
                ?>
                    



                    <!-- pagination
                    <div class="col-md-12">
                        <ul class="article-pagination">
                            <li class="active">1</li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li>...</li>
                            <li><a href="#">12</a></li>
                        </ul>
                    </div>
                    <!-- /pagination -->
                </div>
            </main>

            <?php $this->load->view('default/sidebar'); ?>
        </div>
    </div>
</div>


