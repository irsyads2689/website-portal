<?php 
$theme_url = get_theme_url(); 
?>

    <div class="main-container container pt-40" id="main-container">         

      <!-- Content -->
      <div class="row">

        <!-- Posts -->
        <div class="col-lg-8 blog__content mb-40">
          <h1 class="page-title">Hasil pencarian: <?php echo $keywords; ?></h1>
          <div id="article-search" data-kata_kunci="<?php echo $keywords; ?>" data-next_page="<?php echo $next_page; ?>" data-total_page="<?php echo $total_page; ?>" data-try="<?php echo $try; ?>">
          <?php 
          if($keywords) {
              if($articles) {
                  foreach($articles as $row) {
                      $post_url       = site_url($row->category_slug.'/'.$row->slug);
                      $post_title     = $row->title;
                      $post_image     = get_image('posts', $row->image);
                      $category_url   = site_url($row->category_slug);
                      $category_title = $row->category;
                      echo '
                          <article class="entry card post-list">
                              <div class="entry__img-holder post-list__img-holder card__img-holder" style="background-image: url('.$post_image.')">
                                  <a href="'.$post_image.'" class="thumb-url"></a>
                                  <img src="'.$post_image.'" alt="'.$post_image.'" class="entry__img d-none">
                                  <a href="'.$category_url.'" class="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--green">'.$category_title.'</a>
                              </div>

                              <div class="entry__body post-list__body card__body">
                                  <div class="entry__header">
                                      <h2 class="entry__title">
                                          <a href="'.$post_url.'">'.$post_title.'</a>
                                      </h2>
                                  </div>        
                              </div>
                          </article>
                      ';
                  }
              } else {
                  echo 'ups, artikel dengan kata kunci "'.$keywords.'" tidak ditemukan.';
              }
          } else {
              echo 'artkel tidak ditemukan, kata kunci kosong!.';
          }
          ?>
          </div>
          <div style="position: relative; display: none;" id="loader-scroll" class="pt-40 pb-40 hidden">
              <div class="loader" style="display: block"><div></div></div>
          </div>

          </div>

        <?php $this->load->view('default/sidebar'); ?>
  
    </div> <!-- end content -->
</div> <!-- end main container -->

