<?php 
$theme_url = get_theme_url(); 
?>
    <!-- slider  -->
    <div id="home-owl" class="owl-carousel owl-theme">
        <div class="home-item">
            <div class="section-bg" style="background-image: url(<?php echo $theme_url; ?>/assets/img/banner1.jpg);"></div>
        </div>

        <div class="home-item">
            <div class="section-bg" style="background-image: url(<?php echo $theme_url; ?>/assets/img/banner2.JPG);"></div>
        </div>

        <div class="home-item">
            <div class="section-bg" style="background-image: url(<?php echo $theme_url; ?>/assets/img/banner3.jpg);"></div>
        </div>
    </div>
</header>

<!-- profile -->
<div id="about" class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="about-video">
                    <img src="<?php echo $theme_url; ?>/assets/img/about2.jpg" alt="">
                </div>
            </div>
            <div class="col-md-offset-1 col-md-5">
                <div class="section-title">
                    <h2 class="title">Profil</h2>
                    <p class="sub-title"><?php echo get_option('site_name'); ?></p>
                </div>
                <div class="about-content"><?php echo $profile ? excerpt($profile->content, 500, true, '...') : ''; ?></p>
                    <a href="<?php echo site_url($profile->type.'/'.$profile->slug); ?>" class="primary-button">Selengkapnya</a>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- program -->
<div id="numbers" class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="section-title text-center">
                    <h2 class="title">Program Kami</h2>
                </div>
            </div>
            <div class="col-md-12">
                <div id="program-owl" class="owl-carousel owl-theme">
                <?php 
                if($programs) {
                    foreach($programs as $row) {
                        $title = $row->title;
                        $link = site_url('program/'.$row->slug);
                        $image = get_image($row->image_folder, $row->image);
                        echo '
                                <a href="'.$link.'">
                            <div class="program">
                                    <img src="'.$image.'" class="img img-responsive">
                                    <h3>'.$title.'</h3>
                                <div class="overlay"></div>
                                
                            </div></a>';
                    }
                }
                ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- CTA -->
<div id="cta" class="section">
    <div class="section-bg" style="background-image: url(<?php echo $theme_url; ?>/assets/img/paralax.jpg);" data-stellar-background-ratio="0.5"></div>

    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <div class="cta-content text-center">
                    <h1>Wujudkan impian mereka</h1>
                    <p class="lead">Mari bantu mereka untuk mewujudkan impiannya</p>
                    <a href="#" class="primary-button">Donasi Sekarang</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- latest news -->
<div id="blog" class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="section-title text-center">
                    <h2 class="title">Berita</h2>
                </div>
            </div>
            <?php 
            if($news) {
                foreach($news as $row) {
                    $title = $row->title;
                    $link = site_url($row->type.'/'.$row->slug);
                    $image = get_image($row->image_folder, $row->image);
                    $date = date_indo($row->created_at);
                    echo '
                        <div class="col-md-4">
                            <div class="article">
                                <div class="article-img">
                                    <a href="single-blog.html">
                                        <img src="'.$image.'" alt="'.$title.'">
                                    </a>
                                </div>
                                <div class="article-content">
                                    <ul class="article-meta">
                                        <li>'.$date.'</li>
                                    </ul>
                                    <h3 class="article-title"><a href="'.$link.'">'.$title.'</a></h3>
                                </div>
                            </div>
                        </div>';
                }
            }
            ?>
        </div>
    </div>
</div>


